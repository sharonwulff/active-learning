\documentclass[a4paper,12pt]{article}
\usepackage{a4wide}

% Bibstyle
\bibliographystyle{plainnat}
\usepackage{url}

%\usepackage{amsmath, amssymb, natbib, graphicx, url, algorithm2e}

%%% For Math
  \usepackage{amsmath, amssymb, amstext, amsthm}

% For figures
%\usepackage{subfigure} 

% For citations
  \usepackage{natbib}

% For algorithms
 \usepackage{algorithm}
 \usepackage{algorithmic}

%%% theoremstyle
  \theoremstyle{plain}
    \newtheorem{theorem}{Theorem}
    \newtheorem{lemma}[theorem]{Lemma}
    \newtheorem{corollary}[theorem]{Corollary}
    \newtheorem{observation}[theorem]{Observation}

  \theoremstyle{definition}
    \newtheorem{definition}[theorem]{Definition}

% caligraphic letters
  \newcommand{\Acal}{{\mathcal A}}
  \newcommand{\Bcal}{{\mathcal B}}
  \newcommand{\Pcal}{{\mathcal P}}
  \newcommand{\Qcal}{{\mathcal Q}}
  \newcommand{\Dcal}{{\mathcal D}}
  \newcommand{\Ncal}{{\mathcal N}}
  \newcommand{\Ucal}{{\mathcal U}}
  \newcommand{\Scal}{{\mathcal S}}
  \newcommand{\Fcal}{{\mathcal F}}
  \newcommand{\Xcal}{{\mathcal X}}
  \newcommand{\Ycal}{{\mathcal Y}}
  
   \newcommand{\reals}{\mathbb{R}}
   \newcommand{\naturals}{\mathbb{N}}
   \newcommand{\e}{\mathrm{e}}

   \newcommand{\err}{\mathrm{Err}}
   \newcommand{\ex}{\mathbb{E}}
   \newcommand{\Exp}{\mathrm{E}}	% expectation of a random variable
   \newcommand{\Prob}{\mathrm{Prob}}   %Probability
   \newcommand{\eerr}{\mathrm{\widehat{Err}}}
   \newcommand{\opt}{\mathrm{opt}}
   \newcommand{\sign}{\mathrm{sign}}
   \newcommand{\VC}{\mathrm{VCdim}}
   \newcommand{\rc}{\mathrm{Right}}
   \newcommand{\lc}{\mathrm{Left}}
   \newcommand{\rt}{\mathrm{Root}}
   \newcommand{\level}{\mathrm{level}}
   \newcommand{\diam}{\mathrm{diam}}
   \newcommand{\ERM}{\mathrm{ERM}}
   \newcommand{\RLM}{\mathrm{RLM}}
   \newcommand{\NN}{\mathrm{NN}}
   \newcommand{\argmin}{\mathrm{argmin}}
   \newcommand{\PLAL}{\mathrm{PLAL}}
   \newcommand{\act}{\mathrm{act}}
   \newcommand{\STAT}{\mathrm{STAT}}
   
   \newcommand{\iid}{\textsl{i.i.d.\ }} 		% iid
   \newcommand{\eg}{e.g.\ }				% e.g.
   \newcommand{\ie}{i.e.\ }				% e.g.
   
   \newcommand{\pl}[2]{\mathrm{L}^{#1}_{#2}} %ProbLip function?


%Comments:
\newcommand{\shai}[1]{{\em {\bf \color{red} Shai:} #1}}
\newcommand{\ruth}[1]{{\em {\bf \color{red} Ruth:} #1}}
\newcommand{\shar}[1]{{\em {\bf \color{red} Sharon:} #1}}

\usepackage{pspicture}

% For the generation of the plots from csv files (build with: pdflatex -pdf -shell-escape colt2013_plal.tex)
\def\pgftikzinstalled{1} % comment this if you don't have pgf installed
\ifdefined \pgftikzinstalled
    \usepackage{tikz}
    \usetikzlibrary{external}
    \usepackage{pgfplots}
    \usepackage{pgfplotstable}
    \pgfplotsset{compat=newest}
\else
    \usepackage{tikzexternal}
\fi
\tikzexternalize
\tikzsetexternalprefix{include/externalized/}


% TODO: use citep and citet! Put several citations in the same cite*{}!


\title{PLAL: Cluster-based Active Learning}

 % Authors with different addresses:
\author{Ruth Urner\\
School of Computer Science\\
Univerity of Waterloo\\
Canada, ON, N2L 3G1\\
\texttt{rurner@cs.uwaterloo.ca}\\
\and
Sharon Wulff \\
Department of Computer Science\\
ETH, Zurich \\
Switzerland\\
\texttt{sharon.wulff@inf.ethz.ch} \\
\and
Shai Ben-David\\
School of Computer Science\\
Univerity of Waterloo\\
Canada, ON, N2L 3G1\\
\texttt{shai@cs.uwaterloo.ca}\\
}
% \author{\Name{Ruth Urner} \Email{rurner@cs.uwaterloo.ca}\\
%  \addr Address 1
%  \AND
%  \Name{Sharon Wulff} \Email{sharon.wulff@inf.ethz.ch}\\
%  \addr Address 2
%  \Name{Shai Ben-David} \Email{shai@cs.uwaterloo.ca}\\
%  \addr Address 2
%  }

\begin{document}

\maketitle

\begin{abstract}
We investigate the label complexity of active learning under some smoothness assumptions on the data-generating process.
We propose a procedure, PLAL, for ``activising'' passive, sample-based learners. The procedure takes an unlabeled
sample, queries the labels of some of its members, and outputs a full labeling of that sample. Assuming the data satisfies ``Probabilistic Lipschitzness'', a notion of clusterability, we show that for several 
common learning paradigms, applying our procedure as a preprocessing leads to provable label complexity reductions (over any ``passive''
learning algorithm, under the same data assumptions). Our labeling procedure is simple and easy to implement. We complement our theoretical findings with experimental validations.
\end{abstract}


% Recent work on the theoretical analysis of active learning suggests that the clusterability of the data can be 
% used to investigate the expected label reduction of active learning algorithms. This approach especially holds promise 
% in the agnostic learning settings, where shrinkage of the hypothesis space is not guaranteed upon revealing 
% a label, a notion that is essential for most existing label complexity bounds. 
% In this work we investigate the label complexity of active learning under a natural clusterability notion of the 
% data called the ``Probabilistic Lipschitzness'' (PL). We propose PLAL, a general active learning procedure which can be 
% used as a pre-processing step before using one's favorite sample-based learner. 
% We show that this procedure can lead to provable label complexity reduction as a 
% function of the PL of the underlying data, and in comparison to non-active sample bounds under the same PL assumptions. 
% PLAL is easy to implement and aside for the theoretical analysis, the actual procedure does not require the knowledge of the data's PL. 
% We complement our theoretical findings with experimental validations.


% \begin{keywords}
% learning theory, agnostic active learning, label complexity%, probabilistic lipschitzness
% \end{keywords}

 \input{introduction}
% 
 \input{related_work}
% 
 \input{definitions}
% 
 \input{algorithm_new}
% 
 \input{plal_al}

 \input{experiments}


% Acknowledgments---Will not appear in anonymized version
%\acks{We thank a bunch of people.} 


\bibliography{plal_bib}

\appendix

\input{appendix}

\end{document}
