%
\section{The PLAL labeling procedure}\label{s:plal}

The general framework for our algorithm was suggested in \cite{DasguptaH08}. The idea is to use a hierarchical clustering (cluster tree) of the unlabeled data, check the clusters for label homogeneity by starting at the root of the tree (the whole data-set) and working towards the leafs (single data points). The label homogeneity of a cluster is estimated by choosing data points for label queries uniformly at random from the cluster.  If a cluster can be considered label homogeneous with sufficiently high confidence, all remaining unlabeled points in the cluster are labeled with the majority label and no further points from this cluster will be queried. If a cluster is detected to be label heterogeneous, it is split into its children in the cluster tree. Since the cluster tree is fixed before any labels were seen, the algorithm can reuse labels from the parent cluster (the induced subsample can be considered a sample that was chosen uniformly at random from the points in the child-cluster) without 
introducing 
any sampling bias. 
\cite{Dasgupta11} provides nice overview on this labeling strategy.

\cite{DasguptaH08} analyze of this framework %shows that this framework is beneficial 
assuming that there exist a label homogeneous clustering of the data consisting of a relatively small number of tree-node clusters. 
%However, it should not hurt the paradigm too much if single branches of the label-homogeneous final clustering have a high distance from the root as long as the tree does not branch out too much up to this level.
%We provide an analysis of this framework that takes this into account.
In contrast, our analysis depends on the rate in which the diameters of the clusters shrink. Invoking the PL assumption, we can turn such cluster-diameter bounds into error and label query bounds of the procedure. The rates in which cluster diameters shrink have been analyzed for cluster trees that are induced by \emph{spatial trees} in \cite{NakulSS12}. In this work, we thus consider a version of the general framework that employs spatial trees for the  hierarchical clustering. To obtain a concrete algorithm from the general framework, we further need to specify, how many points to query per cluster and in which order to choose the clusters. We describe our version of this labeling procedure in the next subsection.

% For simplicity, we fix the hierarchical clustering to consist of axis alligned rectangles (called \emph{cells}). The root of the tree is the whole space $[0,1]^d$ and each cell is split into the $2^d$ cells obtains by halving all its side-lengths. Choosing the number of points to query per cluster carefully, we prove that PLAL labels at most an $\epsilon$-fraction of the points in the sample incorrectly and we obtain bounds on the number of queries the labeling procedure makes, that are independent of the depth in the cluster tree that the 
% algorithm reaches.

\subsection{The algorithm}\label{ss:algorithm}

A spatial tree is a binary tree $T$, where each node consists of a subset of the space $\Xcal=[0,1]^d$. We refer to these subsets as \emph{cells}. The root $\rt(T)$ of
a spatial tree is the whole space $[0,1]^d$ and for each node (cell) $C$ the children $\lc(C)$ and $\rc(C)$ form a $2$-partition of the node $C$. This implies that for each \emph{level} $k$ (distance from the root), the nodes at this level form a $2^k$-partition of the space. For a sample $S$, a spatial tree induces a hierarchical clustering of $S$ with \emph{clusters} $S\cap C$ for the nodes $C$ in the tree.

Our algorithm works in rounds (see pseudocode in Algorithm \ref{PLAL}). It takes an unlabeled \iid sample $S_\Xcal$ and a spatial tree $T$ as input. At each round, the algorithm maintains a partition of the space $[0,1]^d$ into \emph{active} and \emph{inactive cells}.
Initially, there is only one active cell, which is the root of the tree $T$, \ie the entire unit cube $[0,1]^d$ containing all sample points. 
% In the course of the algorithm, cells are split by halving the their side-length and distributing the contained sample points among the  child-cells accordingly.
Per round (level), the algorithm queries sufficiently many
labels from the $S_\Xcal$ points in each of the active cells, to detect if the
cell is label heterogeneous (the next paragraph gives a detailed explanation for this method $C.query()$). A \emph{label homogenous} cell (all seen labels in the cell are the same) is
declared inactive and all remaining sample points in the cell are assigned that label. For a \emph{label heterogeneous} cell, the children of the cell in $T$ are added to the list of active cells for the next round, if they still contain unlabeled points. 

\begin{algorithm}[tb]
   \caption{PLAL labeling procedure}
   \label{PLAL}
\begin{algorithmic}
   \STATE {\bfseries Input:} unlabeled sample $S_\Xcal=(x_1,\ldots, x_m)$, spatial tree $T$, parameters $\epsilon, \delta$\\
%   \STATE       \hspace{1.2cm} query numbers $q_1, q_2, q_3 \ldots$
%   \STATE $C_1 = [0,1]^d$
   \STATE $\level = 0$
   \STATE $active\_cells[0].append(\rt(T))$
   \WHILE{$active\_cells[\level]$ not empty}
   \STATE $q_{\level}= \frac{\level \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$
      \FOR {{\bf all} $C$ {\bf in} $active\_cells[\level]$}
	  \STATE $C.query(q_{\level})$
	  \IF{all labels seen in $C$ are the same}
	  \STATE label all points in $C\cap S$ with that label \sl (cell $C$ now becomes inactive)
	  \ELSE
	    \IF{there are unqueried points in $C\cap S$}
	      %\STATE split $C$ into $C_1,\ldots,C_{2^{d}}$
	      \STATE $active\_cells[\level + 1].append(\rc(C), \lc(C))$
	      %\STATE \hspace{4cm} $\ldots,C_{2^{d}})$ 
	    \ENDIF
	  \ENDIF
      \ENDFOR
      \STATE $\level = \level+1$
   \ENDWHILE   
   \STATE {\bfseries Return:} labeled sample $S=((x_1,y_1) \ldots, (x_m, y_m))$
\end{algorithmic}
\end{algorithm}

% A label heterogeneous cell is split into smaller cells halving the
% side-length. Thus each label heterogeneous cell is split into $2^d$ active cells
% for the next round.

%\ruth{the next paragraph needs to be modified}
For a cell $C$, method $C.query(q)$ queries the labels of the first $q$ sample points in the cell. For this, it reuses labels of points that were queried in earlier rounds (\ie does not actually query those). 
If the cell contains fewer than $q$ sample points, the labels of all unlabeled points among these are queried and the cell is declared inactive. In this case, it is not important whether the cell is label homogeneous or label heterogeneous, as the algorithm does not infer labels for any of the points and thus all the labels of points in such cells are correct labels. Note that ``declaring a cell inactive'' is implicit in the code of Algorithm \ref{PLAL}: Only for cells that are heterogeneous \emph{and} contain unlabeled points the children are added to the list of active cells for the next round.

At the end of the procedure all sample points in $S_\Xcal$ are labeled. Each point was either queried or obtained an induced label from the homogeneous declared cell it resides in.
%that was declared inactive, by assigning all points in the cell the label of this cell. 
Only in the latter case, a point might possibly have obtained an erroneous label. We show in Subsection \ref{ss:bound_error} below that, by choosing the \emph{query numbers} $q_i= \frac{i \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$, we can bound the number of labeling mistakes this algorithm makes.

% In order to determine the label heterogeneity of a cell the algorithm chooses
% points to query from the sample that lie in this cell uniformly at random. For input parameter $\epsilon$ the algorithm queries $1/\epsilon$ many points from a cell and declares the cell label homogeneous (and thus inactive for the next round) if all of the queried points have the same label.



% \begin{algorithm}[tb]
%    \caption{PLAL labeling procedure}
%    \label{PLAL}
% \begin{algorithmic}
%    \STATE {\bfseries Input:} sample $S$, number $q$
%    \STATE $C_0 = [0,1]^d$
%    \STATE $active\_cluster\_queue.append(C_0)$
%    \REPEAT
%    \STATE $C = active\_cluster\_queue.pop()$
%    \STATE $cluster\_state = query(C, q)$
%    \IF{$cluster\_state == homogeneous$} 
%    \STATE label all points in $C$ with the label in $C$
%    \ELSE
%       \IF{there are unqueried points in $C$}
%       \STATE $active\_cluster\_queue.append(C.split)$ 
%       \ENDIF
%    \ENDIF
%    \UNTIL{$active\_cluster\_queue$ is $empty$}
%    \STATE {\bfseries Return:} labeled sample $S$
% \end{algorithmic}
% \end{algorithm}


% Algorithm \ref{PLAL} provides pseudo code for our labeling procedure. The algorithm maintains a queue with the active clusters. At each step, a cluster $C$ is popped from the queue. The method $C.query$ selects $q$ (or all available if less) points uniformly at random from the cluster and queries their label. If these are all the same ($cluster\_state == homogeneous$) then all remaining unqueried points (if any) are labeled with this label. If the labels queried in $C$ are not all the same and there are unqueried points left in $C$, then the cluster is split and the resulting clusters appended to the queue of active clusters.

\subsection{Error-bound}\label{ss:bound_error}
In this section, we prove that with high probability over the unlabeled input sample, PLAL will label almost all points in the sample correctly. More precisely, we show the following: 
% \ruth{We need to make a precise statement about how much of this subsection already appears in Dusgupta's paper}

\begin{theorem}\label{thm_error_on_sample}
  Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, $l:\Xcal\to \{0,1\}$ a labeling function and $m\in\naturals$. 
  %For $\epsilon >0 $ and $\delta >0$.let $q_k = \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ be the query number for PLAL at level $k$. 
  Then, when given an \iid unlabeled $P_\Xcal$-sample $S_\Xcal$ of size $m$ and parameters $\epsilon$ and $\delta$, with probability at least $(1-\delta)$ (over the choice of the sample $S_\Xcal$), PLAL  labels at least $(1-\epsilon)m$ many points from $S_\Xcal$ correctly.
\end{theorem}

% To prove this theorem we need the following lemma, which bounds the probability of seeing a label-homogeneous sample when the true underlying distribution is label heterogeneous. The proof is a simple standard calculation, but for completeness, we add a proof in the Appendix.
% 
% \begin{lemma}\label{l:homogenous_sample}
%  Let $\Xcal$ be a domain, $P_\Xcal$ a distribution over $\Xcal$, and $l:\Xcal\to \{0,1\}$ a labeling function. For any $\epsilon, \delta >0$, if $l$ assigns  both labels with $P_\Xcal$-probability larger than $\epsilon$, then with probability at least $1-\delta$ in an \iid $P_\Xcal$-sample, labeled by $l$, of size at least 
%  \[
%   \frac{\ln(2/\delta)}{\epsilon}
%  \]
%  both labels will occur.
%  \end{lemma}
%
%With this we can proceed to the proof of the theorem:

\begin{proof}%[Proof of Theorem \ref{thm_error_on_sample}]
Consider a cell that is declared inactive by the PLAL procedure. This cell was either declared homogeneous 
 %and all points in the cell were labeled with the same label, 
or all the points in the cell were actually queried for their label. In the latter case, all points receive the correct label. 
We show that in each cell $C$, that was declared homogeneous, at most an $\epsilon$ fraction of the points are labeled incorrectly.
Note that, at each stage, the set of labeled points in a cell $C$ can be viewed as a superset of a set chosen uniformly at random from the cell: Such a uniformly chosen set may have revealed fewer than $q$ labels, and as $S_\Xcal$ is an \iid sample we can without loss of generality assume that these are the points with smallest indices in the cell.

%  We need to show that,  with high probability, we declare a cell as homogeneous only if at most an $\epsilon$-fraction of the sample points in the cell are of the minority label. 
%  %We need the following result (the proof is a simple standard calculation, but for completeness, we add a proof in the Appendix):
 Standard analysis shows that, for any cell $C$, if $\min \{\Pr[l=1|C], \Pr[l=0|C]\} \geq \epsilon$ then a sample of size $ \frac{\ln(2/\delta)}{\epsilon}$ has probability at most $\delta$ of being label homogeneous.
Therefore, choosing query numbers $\frac{\ln(2/\delta_C)}{\epsilon}$,
for every cell $C$, guarantees that  with probability at least $1-\delta_C$, it will either be declared homogeneous,  resulting in at most an $\epsilon$-fraction of the sample points in the cell being misclassified or the cell will be declared heterogeneous and split further.
 % {\bf Claim 1} If $l$ assigns  both labels with $P_\Xcal|_C$-probability larger than $\epsilon$ for some set $C\subseteq \Xcal$, then with probability at least $1-\delta$ in an \iid $P_\Xcal|_C$-sample, labeled by $l$, of size at least 
%  \[  \frac{\ln(2/\delta)}{\epsilon} \] both labels will occur.
%  We need to control the confidence parameter $\delta_C$ for each of the cells that are declared homogeneous. 
 By choosing $\delta_C=\delta/2^{2k-1}$, where $k$ is the level of the cell $C$, we ensure that the sum over all confidence parameters $\delta_C$ for all cells $C$, that are declared homogeneous, is at most $\delta$ (note this results in our query numbers  $\frac{\ln(2/\delta_C)}{\epsilon}=\frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$)
% For some $\epsilon>0$ and $\delta_C>0$,  Claim 1 guarantees that if the minority label of a cell $C$ occurs in more than an $\epsilon$-fraction of the points of the cell, then a sub-sample of size $\frac{\ln{2/\delta_C}}{\epsilon}=\frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ of the points in the cell will contain both labels and will not be declared homogeneous. 
 Thereby, 
 with probability $1-\delta$ over samples, PLAL labels at least a $1-\epsilon$ fraction of the points correctly. 
\end{proof}

\begin{observation}\label{o:cells_homo}
It is interesting to note that, if the spatial tree was fixed \emph{before} the unalabeled sample $S_\Xcal$ was drawn, then for a given cell $C$ the set of points whose labels were queried can be viewed as a sample from the underlying distribution restricted to this cell. This implies that, when PLAL declares the sample in a cell label homogeneous (after querying the labels of the first $\frac{\ln(2/\delta_C)}{\epsilon}$ sample points in the cell), we can actually conclude that at most an $\epsilon$-fraction (according to the distribution) of all domain points in the cell are of the opposite label. Thus, if we restrict our view to the cells that get declared homogeneous during a run of PLAL, the labeling that labels those cells with the detected label has error at most $\epsilon$ (with high probability).
\end{observation}



\subsection{Bound on the number of queries}\label{ss:bound_queries}

 In this section we provide a bound on the number of queries the algorithm makes when fed with an unlabeled sample of size $m$ under the assumption that the data generating distribution satisfies a Probabilistic Lipschitz condition. Our bounds involve the spread of the sample points at level $k$, called the \emph{data diameter}.
 In order to avoid overloaded notation, we consider the spatial tree $T$ fixed for this section.
 For a set of points $S$, we let $\lambda_k^S$ denote the maximum data-diameter in a cell at level $k$, \ie 
 $
  \lambda_k^S = \max \{\diam(C, S) ~:~ C\ \text{is a cell at level}~ k\},  
 $
 where $\diam(C,S)$ is the \emph{data-diameter} of the sample points in cell $C$, defined as 
 $
  \diam(C,S) = \max_{x,y\in C\cap S} \|x-y\|.
 $
We denote the maximum diameter of a cell at level $k$ by $\lambda_k := \lambda_k^\Xcal$. The diameter of a cell is always an upper bound on its data-diameter.

\begin{theorem}\label{t:query_bound}
  Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, $l:\Xcal\to \{0,1\}$ a labeling function that is $\phi$-Lipschitz for some function $\phi$, let $q_i=\frac{i \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ denote the query numbers of PLAL for level $i$ and let $(\lambda_i)_{i\in\naturals}$ be a decreasing sequence with $\lambda_i\in[0,\sqrt{d}]$. Then the expected number of queries that PLAL makes on an unlabeled \iid sample $S$ from $P_\Xcal$ of size $m$, given that the data diameter of $S$ at level $k$ satisfies $\lambda_k^S\leq\lambda_k$ for all $k$, is bounded by
  $$
   \min_{k\in\naturals}~ (q_{k}2^{k} + \phi(\lambda_k)m).
  $$
\end{theorem}

\begin{proof}
  For each level, the Probabilistic Lipschitzness allows us to bound the number of points that lie in heterogeneous cells at level $k$: For any sample point $x$ that lies in a label heterogeneous cluster at level $k$, there is a sample point $y$ in this cluster, such that the labling function $l$ on $x$ and $y$ violates the (standard) Lipschitz condition for $1/\lambda^S_k$, and thus also for $1/\lambda_k$. The total weight of such points $x$ is bounded by $\phi(\lambda_k)$. Therefore (as $\lambda_k$ was fixed before drawing the sample), the expected number of sample points that lie in heterogeneous clusters at level $k$ is bounded by $\phi(\lambda_k)\cdot m$. Thus, the expected number of points that are still unlabeled at the beginning of round $k+1$ is bounded by $\phi(\lambda_k)\cdot m$. 
%   (Note that our algorithm always detects label homogeneity: If a cell is truly label homogeneous, then the sample is label homogeneous and all sample points in the cell will thus be labeled with the label by the method $C.query()$.)

  %Let $q_k$ be the number of labels that our algorithm requires to be seen per cell on level $k$. 
  Consider the partition of the space PLAL has produced at the beginning of round $k$ (some of the cells in this partition are homogeneous cells from previous rounds and some are the active cells at this level $k$).
  Clearly, $q_k$  is a bound on the number of label-queries the algorithm made so far for each of the cells in this partition, as we reuse labels from previous rounds, and the sequence $(q_i)_{i\in\naturals}$ is non-decreasing. There are at most $2^{k}$ cells in this partition. Thus $q_k 2^{k}$ is an upper bound on the number of queries made up to level $k$.  
  These two bounds together imply that the number of queries is bounded by $q_{k}2^{k} + \phi(\lambda_k)\cdot m$ for any $k$. 
 \end{proof}

%   Now we set the query numbers as in the previous section, \ie $q_k =  \ln\left(\frac{2}{\delta/2^{2k-1}}\right)/\epsilon =\frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$.  

  
  The following corollary will allow us to obtain concrete bounds on the number of queries for various probabilistic Lipschitz functions (see Table \ref{t:active_query_numbers} below). It follows directly from Theorem \ref{t:query_bound}. Note that, provided the sequence $(q_i)_{i\in\naturals}$ of query numbers is non-decreasing, the condition $\phi(\lambda_{k^*})\cdot m \leq q_{k^*} \cdot 2^{k^*d}$ in the corollary is satisfied for sufficiently large $k^*$: $\phi(\lambda)$ is decreasing for $\lambda\to 0$, and $\lambda_k\to 0$ for $k \to \infty$.
  
  \begin{corollary}\label{c:query_bound}
%   Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, $l:\Xcal\to \{0,1\}$ a labeling function that is $\phi$-Lipschitz for some function $\phi$, and $q_k = \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ the sequence of query numbers. 
%   
  Under the conditions of Theorem \ref{t:query_bound}, let $k^*$ be such that $\phi(\lambda_{k^*})\cdot m \leq q_{k^*} \cdot 2^{k^*}$. Then the expected number of queries that PLAL makes on an unlabeled \iid sample from $P_\Xcal$ of size $m$ is bounded by
  $$
  \frac{k^* \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon} \cdot  2^{k^*+1}
  %   2^{k^*d}\cdot \frac{3(\ln(2/\delta)+k^*d\ln(2))}{\epsilon}
  $$
\end{corollary}

% \begin{proof}
% The result follows directly from Theorem \ref{t:query_bound}.%, by setting $q_k = \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$. 
% \end{proof}


\subsection{Bounds for specific trees}\label{ss:tree_bounds}

\paragraph{Dyadic trees} Here we provide concrete bounds on the expected number of queries for \emph{dyadic trees}. 
In a dyadic spatial tree, cells are always partitioned by halving one of the coordinates, cycling through the dimensions. That is, for any $k$, the initial unit cube $[0,1]^d$ (at the root of the tree) is split into $2^{kd}$ cubes of sidelength $1/2^k$ at level $k\cdot d$. The diameter of such a cube at level $kd$ is $\lambda_{kd} = \sqrt{d}/2^k$, which is at the same time an upper bound on the data diameter $\lambda_{kd}^S$ at level $kd$ for any sample $S$.

Table \ref{t:active_query_numbers} provides an overview on the bounds that we get from Corollary \ref{c:query_bound} for the polynomial and the exponential Lipschitz assumption. For each of the considered probabilistic Lipschitz functions, we first calculate a value $k^*$ such that $\phi(\lambda_{k^*})\cdot m \leq q_{k^*} \cdot 2^{k^*}$ and then plug this into the formula of Corollary \ref{c:query_bound} in order to bound the expected number of queries. The calculations can be found in the appendix. The $\tilde{O}$-notation hides $\log$-factors and constants.% (here we consider the dimension of the space $d$ a constant).

\begin{table}[h]
\caption{Dyadic trees}
\label{t:active_query_numbers}
\centering
\begin{tabular}{|r|l|}
\hline
Lipschitzness & Bound on expected number of queries\\
\hline
$\phi(\lambda) = \lambda^n$ & $2\cdot\frac{\log({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}} \ln(2) + \ln(1/\delta)}{\epsilon} \cdot({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}} = \tilde{O}(m^{\frac{d}{n+d}} \left(\frac{1}{\epsilon}\right)^{\frac{n}{n+d}})$\\[12pt]
% %\hline
% $\frac{\lambda^d}{\sqrt{d}^d}$ &  $\frac{\sqrt{m}}{\epsilon^{1/2}}\cdot\left(\log(m\epsilon)^{1/2}\ln(2)+\ln(2/\delta))\right) $\\[12pt]
%\hline
$\phi(\lambda) = \e^{\frac{-1}{\lambda}}$ & $\frac{\sqrt{d}^d\log(\epsilon m)^d}{\epsilon} 2(\log(\log((\epsilon m)^{\sqrt{d}}))d\ln(2) +\ln(2/\delta))=\tilde{O}(\frac{1}{\epsilon})$\\[3pt]
\hline 
\end{tabular}
\end{table}

\paragraph{Other spatial trees}
Often, the \emph{intrinsic dimension} of real data is considerably smaller than the Euclidean dimension of its feature space.
\cite{NakulSS12} show (for several notions of intrinsic dimension) that, for various classes of spatial trees, the expected data diameter decreases as a function of the intrinsic dimension. Thus, we expect that the query bounds of PLAL used with these trees scale well with the intrinsic dimension.

%Thus, our analysis implies that the query bounds of PLAL used with these trees scale well with the intrinsic dimension.

