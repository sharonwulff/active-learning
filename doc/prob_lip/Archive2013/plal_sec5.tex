\section{Using PLAL for active learning}

In the previous section we have shown how,  given any sample, $S=((x_1, y_1), \ldots, (x_m, y_m))$, the PLAL labeling procedure takes its unlabeled projection $(x_1, \ldots, x_m)$ as input, queries some of the labels and outputs 
a labeled sample $S'=((x_1, y'_1), \ldots, (x_m, y'_m))$ such that with high probability,  $|\{i : y_i \neq y'_i\}|$ is bounded (as a function of the Probabilistic Lipschitzness of the data generating distribution and the number of labels PLAL  queried ). In many cases, such a sample, $S'$, suffices for successful learning. Below, we formalize this claim.


\begin{definition} Given a sequence of labeled instances, $S=((x_1, y_1), \ldots, (x_m, y_m))$ and $\epsilon \geq 0$, define the $\epsilon$-neighborhood of $S$ as 
$${\cal S}_{\epsilon} = \{ S'=((x_1, y'_1), \ldots, (x_m, y'_m)): ~ |\{i : y_i \neq y'_i\}|/m \leq \epsilon\}.$$
\end{definition}

\begin{definition} We say that a learning algorithm, ${\cal A}$,  is $(m, \epsilon, \delta, \eta)$- robust with respect to a data distribution $P$, if, 
%with probability $\geq (1-\delta)$over samples of size $m$ drawn i.i.d. by $P$, 

\[\Pr_{S \sim P^m}\left[ \{S : ~ \forall S' \in {\cal S}_{\epsilon}, ~ \err_P({\cal A}(S')) \leq \err_P({\cal A}(S)) + \eta\} \right ] \geq (1-\delta)\]

\end{definition}

\begin{lemma}\label{robust_works}
If  ${\cal A}$,  is a learning algorithm which is  $(m, \epsilon, \delta, \eta)$- robust with respect to a data distribution $P$, then on random training samples of size $m$
generated by $P$, replacing the fully labeled sample with one actively labeled by the PLAL algorithm  with error parameters $\epsilon, \delta$ results in deterioration of the error of ${\cal A}(S)$ by at most $\eta$
(with probability $\geq (1-\delta)$ over the random training samples).
\end{lemma}

Next we show that many common learning algorithm are indeed robust under any data generating distribution, for sufficiently large sample sizes (we will explicitly discuss these sizes).
Applying Lemma \ref{robust_works}, we then conclude that for such algorithms PLAL can be applied as a preliminary procedure, and reduce the label complexity of learning, in cases where the query numbers required by PLAL are sufficiently small (so that it compensates for the $\eta$ loss of accuracy). To continue the discussion we require the following basic notions:

\begin{definition} \label{UC-complexity} 
\begin{enumerate}
\item We say that a labeled sample $S$ is $\epsilon$-representative of $H$ w.r.t. a data-generating distribution, $P$ if for every $h \in H$, $|\err_S(h) -\err_P(h)| \leq \epsilon$.
\item We say that a class $H$ satisfies the uniform convergence property with rate $m^{UC}_H : (0,1) \times (0,1) \to \naturals$ if, for any data generating distribution, $P$ and any $\epsilon, \delta >0$, for every $m \geq m^{UC}_H(\epsilon, \delta)$,
\[\Pr_{S \sim P^m}( \{S : S ~ \mbox{is} ~ \epsilon ~\mbox{representative for $H$ w.r.t.} ~ P \}) \geq 1-\delta\]

\end{enumerate}
\end{definition}
Recall that there exists a constant $C$ such that for any finite VC class, $H$, $m^{UC}_H(\epsilon, \delta)= C \frac{\VC(H) - \log\delta}{\epsilon^2}$.

The following lemma is a direct consequence of the above definitions.
\begin{lemma}\label{robust_algs}
If $m \geq m^{UC}_H(\epsilon, \delta)$ and ${\cal A}$ is either an ERM algorithm over $H$ or a Regularized Loss Minimization algorithm over $H$, then ${\cal A}$,  is $(m, \epsilon, \delta, 3\epsilon)$- robust with respect to any data distribution $P$.

\end{lemma}

%\textbf{STILL NEED TO DEFINE $m^{UC}_H(\epsilon, \delta, P)$ and $m^{UC}_H(\epsilon, \delta, )$and maybe cite the claim that this function exists for any VC class $H$. }

 %%A sample labeled by PLAL labeling procedure can be fed to any learning algorithm that is robust to a small fraction of the training labels being corrupted. 
 
% It is not hard to see that any agnostic learner (see Definition \ref{d:agnostic_learner}) enjoys this robustness. Any agnostic learner has sample complexity at least $\Omega(1/\epsilon^2)$, which corresponds to the sample complexity of the ERM paradigm. Thus, any agnostic learner outputs a classifier that is close to an empirical risk minimizer and thus its behavior can not be changed by much when the empirical error changes by $\epsilon$. 
%
%\begin{lemma}\label{l:agnostic_noise}
% %Let $P$ be a distribution over $X\times\ \{0,1\} $, l
% Let $A$ be an agnostic learner for some hypothesis class $H$ over some domain $\Xcal$ and let $m$ be the sample size required by $A$ to $(\epsilon,\delta)$-learn $H$. Then, with probability at least $(1-\delta)$ over an \iid sample of size $m$ from some distribution $P$ over $\Xcal$  with at most and $\epsilon$-fraction of corrupted labels, $A$ outputs a hypothesis $h$ with $\err_P(h)\leq \opt_H(P)+4\epsilon$.
%\end{lemma}


\subsection{Label savings}

Table \ref{t:active_query_numbers} provides an upper bound on the number of label queries the PLAL procedure makes, given the unlabeled projection of a sample $S$ of size $m$, to generate a sample $S' \in {\cal S}_{\epsilon}$ . 

We now wish to apply these bounds to show provable reductions in the label complexity achieved by applying PLAL to passive learning algorithms.
Since the PLAL query bounds depend on assuming that the data-generating distribution satisfies PL conditions, a fair comparison requires establishing lower bounds
for the sample complexity of the common passive model (of fully labeled random training samples) under the same PL assumptions. 

\begin{theorem}
For $\phi : \reals \to [0,1]$, let ${\cal Q}_{\phi}$ be the family of all data distributions $P=(P_{\Xcal}, l)$ over the unit cube $[0,1]^d$ (for some $d \geq 2$) that satisfy the $\phi$- Lipschitz condition. Then
\begin{enumerate}
\item For every passive learning algorithm ${\cal A}$ and every $\epsilon>0$  there exists a distribution $P \in {\cal Q}_{\phi}$ such that, 
%for any   $\lambda \leq \phi^{-1}( \epsilon)$,  
 $m < \frac{d}{16\epsilon} (\frac{1}{\phi^{-1}( \epsilon)})^{d-1}$ implies that
 the expected error of ${\cal A}(S)$ over $m$-size $P$-generated training samples is $\geq \epsilon$ (where $\phi^{-1}$ is the inverse function of $\phi$).

\item There exists a constant $C$ such that the sample complexity of  the 1-Nearest Neighbor w.r.t the class ${\cal Q}_{\phi}$, is 
$m^{1NN}_{{\cal Q}_{\phi}}(\epsilon, \delta) \leq \frac{2}{\epsilon \delta} \left(\frac{\sqrt{d}}{\phi^{-1}(\epsilon)}\right)^d$.
%suand every $\epsilon >0$, and every $\epsilon >0$, if $m \geq $ then the expected error of the 1-Nearest Neighbor over over $m$-size $P$-generated training samples is $\leq 1/4$.

\end{enumerate}
\end{theorem}

\begin{theorem}
For a concept class $H$ over the unit cube in some Euclidean space $\reals^d$ and $\phi : \reals \to [0,1]$, let ${\cal Q}^H_{\phi}$ be the family of all data distributions $P=(P_{\Xcal}, l)$ over the unit cube $[0,1]^d$ that satisfy the $\phi$- Lipschitz condition and $l \in H$. Then
for every $v \leq \infty$ there is a concept class $H_v$ over the unit cube in some Euclidean dimension, $d$, such that \begin{enumerate}
\item VCdim$(H_v)=v$.
\item There exists constants $C_1, C_2$, such that sample complexity of $H_v$ over the class of distributions satisfies ${\cal Q}^H_{\phi}$ satisfies, for all $\epsilon, \delta >0$
% $\Theta \left(\frac{v}{\epsilon^2}\right)$.
\[ C_1 \frac {v -\log\delta}{\epsilon^2} \leq m^p_H({\cal Q}^H_{\phi})(\epsilon, \delta) \leq  C_2 \frac {v -\log\delta}{\epsilon^2}.\]
\end{enumerate}
\end{theorem}

With these bounds on the label complexity under the passive learning model, we can now apply the analysis of PLAL from section \ref{s:plal}, to show that under some PL conditions the
PLAL procedure improves the label complexity beyond what may be achieved by any passive learner. For concreteness, we will focus on the case of polynomial PL functions $\phi$.

\begin{definition}
\begin{enumerate}
\item Given a passive learning algorithm, ${\cal A}$, let ${\cal A} \circ PLAL$ denote the composition of ${\cal A}$ with the PLAL procedure. Namely, for any data generating distribution, $P=(P_{\Xcal}, l)$, 
rather than dawning a $P$-generated sample of some size, $m$, ${\cal A} \circ PLAL$ considers an unlabeled sample, $S_u$ of the same size, drawn i.i.d. by $P_{\Xcal}$, applies PLAL to $S_u$ and then applies ${\cal A}$ to the resulting labeled sample. 
\item For an active learner ${\cal A}$, let $m_l^a({\cal A}, \Qcal)$ denote the minimal labeled sample complexity of ${\cal A}$ over all unlabeled sample sizes. That is, for every $\epsilon, \delta$,
$m_l^a({\cal A}, \Qcal)(\epsilon, \delta)= \min_{m_u}(m_l({\cal A}, \Qcal, m_u)$.
\item For a concept class, $H$, let $m_H^p(\Qcal^H)$ denote the sample complexity function of learning the class $H$ with respect to distributions in $\Qcal^H$.

\end{enumerate}
\end{definition}

\begin{theorem}
Let $\phi(\lambda)= \lambda^n$ for any $n \in \naturals$.
For every $v \leq \infty$ there exists a concept class $H_v$ over the unit cube $[0,1]^d$, such that $\VC(H_v)=v$ and an ERM learner ${\cal A}$ for $H$ such that the composition of PLAL with ${\cal A}$  results in active learning sample complexity that is below the sample complexity of any passive learner for that class of distributions. Namely
\[m^a_l({\cal A} \circ PLAL, \Qcal^H_{\phi}) = \omega(m^p_H(  \Qcal^H_{\phi})).\]
\end{theorem}

\begin{theorem} Let $\phi(\lambda)= \lambda^n$ for any $n \in \naturals$. Applying PLAL to a 1-NN algorithm results in active sample complexity for learning ${\cal Q}_{\phi}$ that is below the
sample complexity of any passive learning algorithm for that class. Namely,
\[m^a(1NN \circ PLAL, {\cal Q}_{\phi}) = \omega(m^p({\cal Q}_{\phi})).\]

\end{theorem}
 
-----------------------------------------------------------------------------------------------------------------------------------




If the learning task satisfies exponential Lipschitzness ($\phi(\lambda) = \e^{\frac{-1}{\lambda}}$), then expected number of queries is bounded by $1/\epsilon$ times a factor that is logarithmic in the original sample size $m$. Thus, for any algorithm with (fully supervised) sample complexity $m=\Omega((1/\epsilon)^\alpha)$ for some $\alpha >1$, labeling the sample with PLAL reduces the label complexity. 

Similarly, if we have polynomial Lipschitzness ($\phi(\lambda) = \lambda^n$), then the expected number of queries is bounded by $\tilde{O}(m^{\frac{d}{n+d}} \left(\frac{1}{\epsilon}\right)^{\frac{n}{n+d}})$. For an algorithm with (fully supervised) sample complexity $m=\Omega((1/\epsilon)^\alpha)$, this yields $\tilde{O}( \left(\frac{1}{\epsilon}\right)^{\frac{n + \alpha d}{n+d}}))$ as a bound on the expected number of queries. This implies that, again, we labeling with PLAL reduces the label complexity if $\alpha >1$.

The sample complexity of an agnostic learner gets reduced from $\Theta(1/\epsilon^2)$ to $\tilde{O}(1/\epsilon)$ in the case of exponential Lipschitzness and to $\tilde{O}( \left(\frac{1}{\epsilon}\right)^{\frac{ n+ 2 d }{n+d}}))$ in the case of polynomial Lipschitzness. 

