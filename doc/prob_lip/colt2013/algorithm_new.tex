%
\section{The PLAL labeling procedure}\label{s:plal}

The general idea behind our algorithm was suggested in \cite{DasguptaH08}. The idea is to use a hierarchical clustering (cluster tree) of the unlabeled data, check the clusters for label homogeneity by starting at the root of the tree (the whole data-set) and working towards the leafs (single data points). The label homogeneity of a cluster is estimated by choosing data points for label queries uniformly at random from the cluster.  If a cluster can be considered label homogeneous with sufficiently high confidence, all remaining unlabeled points in the cluster are labeled with the majority label and no further points from this cluster will be queried. If a cluster is detected to be label heterogeneous, it is split into its children in the cluster tree. Since the cluster tree is fixed before any labels were seen, the algorithm can reuse labels from the parent cluster (the induced subsample can be considered a sample that was chosen uniformly at random from the points in the child-cluster) without introducing 
any sampling bias. 
A nice overview on this can be found in \cite{Dasgupta11}.

% \cite{DasguptaH08} analyze of this framework %shows that this framework is beneficial 
% assuming that there exist a label homogeneous clustering of the data consisting of a relatively small number of tree-node clusters. 
% %However, it should not hurt the paradigm too much if single branches of the label-homogeneous final clustering have a high distance from the root as long as the tree does not branch out too much up to this level.
% %We provide an analysis of this framework that takes this into account. 
% In contrast, our analysis depends on the rate in which the diameters of the clusters shrink. Invoking the PL assumption, we can then turn such cluster-diameter bounds into error bounds of our procedure. The rates in which cluster diameters shrink for various classes of \emph{spatial trees} have been analyzed for cluster trees that are induced by \emph{spatial trees} in \cite{NakulSS12}. 


% For simplicity, we fix the hierarchical clustering to consist of axis alligned rectangles (called \emph{cells}). The root of the tree is the whole space $[0,1]^d$ and each cell is split into the $2^d$ cells obtains by halving all its side-lengths. Choosing the number of points to query per cluster carefully, we prove that PLAL labels at most an $\epsilon$-fraction of the points in the sample incorrectly and we obtain bounds on the number of queries the labeling procedure makes, that are independent of the depth in the cluster tree that the 
% algorithm reaches.

\subsection{The PLAL labeling procedure}\label{ss:algorithm}

A spatial tree is a binary tree $T$, where each node consists of a subset of the space $\Xcal=[0,1]^d$. We refer to these subsets as \emph{cells}. The root $\rt(T)$ of
a spatial tree is the whole space $[0,1]^d$ and for each node (cell) $C$ the children $\lc(C)$ and $\rc(C)$ form a $2$-partition of the node $C$. This implies that for each \emph{level} $k$ (distance from the root), the nodes at this level from a $2^k$-partition of the space. For a sample $S$, a spatial tree induces a hierarchical clustering of $S$ with clusters $S\cap C$ for the nodes $C$ in the tree.

Our algorithm works in rounds (see pseudocode in Algorithm \ref{PLAL}). It takes an unlabeled \iid sample $S_u$ and a spatial tree $T$ as input.
%query numbers $q_i$ as input. 
At each round, the algorithm maintains a partition of the space $[0,1]^d$ into \emph{active} and \emph{inactive cells},
Initially, there is only one active cell, which is the root if the tree $T$, \ie the entire unit cube $[0,1]^d$ containing all sample points. 
% In the course of the algorithm, cells are split by halving the their side-length and distributing the contained sample points among the  child-cells accordingly.
Per round (level), the algorithm queries sufficiently many
labels from the $S_u$ points in each of the active cells, to detect if the
cell is label heterogeneous (the next paragraph gives a detailed explanation for method $C.query()$). A \emph{label homogenous} cell (all seen labels in the cell are the same) is
declared inactive and all remaining sample points in the cell are assigned that label. For a label heterogeneous cell, the children of the cell in $T$ are added to the list of active cells for the next round.

\begin{algorithm}[tb]
   \caption{PLAL labeling procedure}
   \label{PLAL}
\begin{algorithmic}
   \STATE {\bfseries Input:} unlabeled sample $S_u=(x_1,\ldots, x_m)$, spatial tree $T$, parameters $\epsilon, \delta$\\
%   \STATE       \hspace{1.2cm} query numbers $q_1, q_2, q_3 \ldots$
%   \STATE $C_1 = [0,1]^d$
   \STATE $\level = 0$
   \STATE $active\_cells[0].append(\rt(T))$
   \WHILE{$active\_cells[\level]$ not empty}
   \STATE $q_{\level}= \frac{\level \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$
      \FOR {{\bf all} $C$ {\bf in} $active\_cells[\level]$}
	  \STATE $C.query(q_{\level})$
	  \IF{all labels seen in $C$ are the same}
	  \STATE label all points in $C\cap S$ with that label \sl (cell $C$ now becomes inactive)
	  \ELSE
	    \IF{there are unqueried points in $C\cap S$}
	      %\STATE split $C$ into $C_1,\ldots,C_{2^{d}}$
	      \STATE $active\_cells[\level + 1].append(\rc(C), \lc(C))$
	      %\STATE \hspace{4cm} $\ldots,C_{2^{d}})$ 
	    \ENDIF
	  \ENDIF
      \ENDFOR
      \STATE $\level = \level+1$
   \ENDWHILE   
   \STATE {\bfseries Return:} labeled sample $S$
\end{algorithmic}
\end{algorithm}

% A label heterogeneous cell is split into smaller cells halving the
% side-length. Thus each label heterogeneous cell is split into $2^d$ active cells
% for the next round.

%\ruth{the next paragraph needs to be modified}
For a cell $C$ method $C.query(q)$ chooses the first $q$ sample points in the cell to query their label. For this, it reuses labels of points that were queried in earlier rounds. Note that the set of labeled points can be viewed as a superset of a set chosen uniformly at random from the cell: Such a uniformly chosen set may have revealed less than $q$ labels, and as $S_u$ is an \iid sample we can without loss of generality assume that these are the points with smallest indices in the cell.
% 
% uniformly at random from $C\cap S$ for label querying, such that $q$ labels will be known in the cell after the call of this method. 
If the cell, contains less than $q$ sample points, the labels of all these points are queried and the cell is declared inactive. In this case, it is not important whether the cell is label homogeneous or label heterogeneous, as the algorithm does not infer labels for any of the points and thus all the labels of points in such cells are correct labels. Note that ``declaring a cell inactive'' is implicit in the code of Algorithm \ref{PLAL}: Only for cells that are heterogeneous \emph{and} contain unlabeled points the children are added to the list of active cells for the next round.

At the end of the procedure all sample points in $S$ are labeled. Each point was either queried or obtained an induced label from the homogeneously declared cell it resides in.
%that was declared inactive, by assigning all points in the cell the label of this cell. 
Only in the latter case, a point might possibly have obtained an erroneous label. We show in Subsection \ref{ss:bound_error} below that, by choosing the \emph{sequence of query numbers $q_i$} carefully, we can bound the number of labeling mistakes this algorithm makes.

% In order to determine the label heterogeneity of a cell the algorithm chooses
% points to query from the sample that lie in this cell uniformly at random. For input parameter $\epsilon$ the algorithm queries $1/\epsilon$ many points from a cell and declares the cell label homogeneous (and thus inactive for the next round) if all of the queried points have the same label.



% \begin{algorithm}[tb]
%    \caption{PLAL labeling procedure}
%    \label{PLAL}
% \begin{algorithmic}
%    \STATE {\bfseries Input:} sample $S$, number $q$
%    \STATE $C_0 = [0,1]^d$
%    \STATE $active\_cluster\_queue.append(C_0)$
%    \REPEAT
%    \STATE $C = active\_cluster\_queue.pop()$
%    \STATE $cluster\_state = query(C, q)$
%    \IF{$cluster\_state == homogeneous$} 
%    \STATE label all points in $C$ with the label in $C$
%    \ELSE
%       \IF{there are unqueried points in $C$}
%       \STATE $active\_cluster\_queue.append(C.split)$ 
%       \ENDIF
%    \ENDIF
%    \UNTIL{$active\_cluster\_queue$ is $empty$}
%    \STATE {\bfseries Return:} labeled sample $S$
% \end{algorithmic}
% \end{algorithm}


% Algorithm \ref{PLAL} provides pseudo code for our labeling procedure. The algorithm maintains a queue with the active clusters. At each step, a cluster $C$ is popped from the queue. The method $C.query$ selects $q$ (or all available if less) points uniformly at random from the cluster and queries their label. If these are all the same ($cluster\_state == homogeneous$) then all remaining unqueried points (if any) are labeled with this label. If the labels queried in $C$ are not all the same and there are unqueried points left in $C$, then the cluster is split and the resulting clusters appended to the queue of active clusters.

\subsection{Error-bound}\label{ss:bound_error}

In this section we prove that with high probability over the unlabeled input sample, PLAL will label almost all points in the sample correctly. More precisely, we prove the following: 
% \ruth{We need to make a precise statement about how much of this subsection already appears in Dusgupta's paper}

\begin{theorem}\label{thm_error_on_sample}
  Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, and $l:\Xcal\to \{0,1\}$ a labeling function. 
  %For $\epsilon >0 $ and $\delta >0$.let $q_k = \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ be the query number for PLAL at level $k$. 
  Then, when given an \iid unlabeled $P_\Xcal$-sample $S_u$ of size $m$ and parameters $\epsilon$ and $\delta$, with probability at least $(1-\delta)$ (over the choice of the sample $S_u$), PLAL  labels at least $(1-\epsilon)m$ many points from $S_u$ correctly.
\end{theorem}

% To prove this theorem we need the following lemma, which bounds the probability of seeing a label-homogeneous sample when the true underlying distribution is label heterogeneous. The proof is a simple standard calculation, but for completeness, we add a proof in the Appendix.
% 
% \begin{lemma}\label{l:homogenous_sample}
%  Let $\Xcal$ be a domain, $P_\Xcal$ a distribution over $\Xcal$, and $l:\Xcal\to \{0,1\}$ a labeling function. For any $\epsilon, \delta >0$, if $l$ assigns  both labels with $P_\Xcal$-probability larger than $\epsilon$, then with probability at least $1-\delta$ in an \iid $P_\Xcal$-sample, labeled by $l$, of size at least 
%  \[
%   \frac{\ln(2/\delta)}{\epsilon}
%  \]
%  both labels will occur.
%  \end{lemma}
%
%With this we can proceed to the proof of the theorem:

\begin{proof}%[Proof of Theorem \ref{thm_error_on_sample}]
 Consider a cell that is declared inactive by the PLAL procedure. This cell was either declared homogeneous 
 %and all points in the cell were labeled with the same label, 
 or all the points in the cell were actually queried for their label. In the latter case, all points receive the correct label. 
%  We need to show that,  with high probability, we declare a cell as homogeneous only if at most an $\epsilon$-fraction of the sample points in the cell are of the minority label. 
%  %We need the following result (the proof is a simple standard calculation, but for completeness, we add a proof in the Appendix):
 Standard analysis shows that, for any cell $C$, if $\min \{\Pr[l=1|C], \Pr[l=0|C]\} \geq \epsilon$ then a sample of size $ \frac{\ln(2/\delta)}{\epsilon}$ has probability of at most $\delta$ of being label homogeneous.
% {\bf Claim 1} If $l$ assigns  both labels with $P_\Xcal|_C$-probability larger than $\epsilon$ for some set $C\subseteq \Xcal$, then with probability at least $1-\delta$ in an \iid $P_\Xcal|_C$-sample, labeled by $l$, of size at least 
%  \[  \frac{\ln(2/\delta)}{\epsilon} \] both labels will occur.
%  We need to control the confidence parameter $\delta_C$ for each of the cells that are declared homogeneous. 
 By choosing $\delta_C=\delta/2^{2k-1}$, where $k$ is the level of the cell $C$, we ensure that the sum over all confidence parameters $\delta_C$ for all cells $C$, that are declared homogeneous, is at most $\delta$. 
% For some $\epsilon>0$ and $\delta_C>0$,  Claim 1 guarantees that if the minority label of a cell $C$ occurs in more than an $\epsilon$-fraction of the points of the cell, then a sub-sample of size $\frac{\ln{2/\delta_C}}{\epsilon}=\frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ of the points in the cell will contain both labels and will not be declared homogeneous. 
Therefore, query numbers  $\frac{\ln{2/\delta_C}}{\epsilon}$ establish the claim.
% for every cell $C$,  with probability $\geq 1-\delta_C$, it will either be declared homogeneous,  resulting in at most an $\epsilon$-fraction of the sample points in the cell being misclassified or the cell will be declared heterogeneous and split further.
%  Thereby, 
%  with probability $1-\delta$ over samples, PLAL labels at least a $1-\epsilon$ fraction of the points correctly. 
\end{proof}

\vspace{-1cm}

\subsection{Bound on the number of queries}\label{ss:bound_queries}

 In this section we provide a bound on the number of queries the algorithm makes when fed with an unlabeled sample of size $m$ under the assumption that the data generating distribution satisfies a probabilistic Lipschitz condition. Our bounds involve the spread of the sample points at level $k$, called the \emph{data diameter}.
 For a set of points $S$, we let $\lambda_k^S$ denote the maximum data-diameter in a cell at level $k$, \ie 
 $
  \lambda_k^S = \max \{\diam(C, S) ~:~ C\ \text{is a cell at level}~ k\},  
 $
 where $\diam(C,S)$ is the \emph{data-diameter} of the sample points in cell $C$, defined as 
 $
  \diam(C,S) = \max_{x,y\in C\cap S} \|x-y\|.
 $
We denote the maximum diameter of the cell at level $k$ by $\lambda_k := \lambda_k^\Xcal$. The diameter of a cell is always an upper bound on the data-diameter.

\begin{theorem}\label{t:query_bound}
  Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, $l:\Xcal\to \{0,1\}$ a labeling function that is $\phi$-Lipschitz for some function $\phi$, and $(q_i)_{i\in\naturals}$ a non-decreasing sequence of query numbers. For any $k>0$, the expected number of queries that PLAL makes on an unlabeled \iid sample $S$ from $P_\Xcal$ of size $m$ is bounded by
  $
   \min_{k\in\naturals}~ (q_{k}2^{k} + \phi(\lambda_k^S)m).
  $
\end{theorem}

\begin{proof}
  For each level, the probabilistic Lipschitzness allows us to bound the number of points that lie in heterogeneous cells at level $k$: Any data point that lies in a label heterogeneous cell at level $k$ violates the Lipschitz condition for $\lambda^S_k$. The total weight of points that violate the $\lambda^S_k$ Lipschitz condition is bounded by $\phi(\lambda_k^S)$. Therefore, the expected number of sample points that lie in heterogeneous cells at level $k$ is bounded by $\phi(\lambda_k^S)\cdot m$. Thus, the expected number of points that are still unlabeled at the beginning of round $k+1$ is bounded by $\phi(\lambda_k^S)\cdot m$. 
%   (Note that our algorithm always detects label homogeneity: If a cell is truly label homogeneous, then the sample is label homogeneous and all sample points in the cell will thus be labeled with the label by the method $C.query()$.)

  %Let $q_k$ be the number of labels that our algorithm requires to be seen per cell on level $k$. 
  Consider the partition of the space at the beginning of round $k$.
  Clearly, $q_k$  is a bound on the number of label-queries the algorithm makes for each of the cells in this partition, as we reuse labels from previous rounds, and the sequence $(q_i)_{i\in\naturals}$ is non-decreasing. There are at most $2^{k}$ cells in this partition. Thus $q_k2^{k}$ is an upper bound on the number of queries made up to level $k$.  
  These two bounds together imply that the number of queries is bounded by $q_{k}2^{k} + \phi(\lambda_k^S)\cdot m$ for any $k$. 
 \end{proof}

%   Now we set the query numbers as in the previous section, \ie $q_k =  \ln\left(\frac{2}{\delta/2^{2k-1}}\right)/\epsilon =\frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$.  

\vspace{-0.5cm}
  
  The following corollary will allow us obtain concrete bounds on the number of queries for various probabilistic Lipschitz functions (see Table \ref{t:active_query_numbers} below). 
  
  \begin{corollary}\label{c:query_bound}
  Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, $l:\Xcal\to \{0,1\}$ a labeling function that is $\phi$-Lipschitz for some function $\phi$, and $q_k = \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ the sequence of query numbers. Let $k^*$ be such that $\phi(\lambda_{k^*}^S)\cdot m \leq q_{k^*} \cdot 2^{k^*}$. Then the expected number of queries that PLAL makes on an unlabeled \iid sample from $P_\Xcal$ of size $m$ is bounded by
  $
  \frac{k^* \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon} \cdot  2^{k^*+1}
  %   2^{k^*d}\cdot \frac{3(\ln(2/\delta)+k^*d\ln(2))}{\epsilon}
  $
\end{corollary}

\begin{proof}
The result follows directly from Lemma \ref{t:query_bound}, by setting $q_k = \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$. 
\end{proof}

\vspace{-1cm}

\subsection{Bounds for specific trees}\label{ss:tree_bounds}

\paragraph{Dyadic trees} Here we provide concrete bounds on the number of expected queries for \emph{dyadic trees}. In a dyadic spatial tree, cells are always partitioned by halving one of the coordinates, cycling through the dimensions. That is, for any $k$, the initial unit cube $[0,1]^d$ (at the root of the tree) is split into $2^{kd}$ cubes of sidelength $1/2^k$ at level $k\cdot d$. The diameter of such a cube at level $kd$ is $\lambda_{kd} = \sqrt{d}/2^k$, which is at the same time an upper bound on the data diameter $\lambda_{kd}^S$ at level $kd$ for any sample $S$.

Table \ref{t:active_query_numbers} provides an overview on the bounds that we get from Corollary \ref{c:query_bound} for the polynomial and the exponential Lipschitz assumption. For each of the considered probabilistic Lipschitz functions, we first calculate a value $k^*$ such that $\phi(\lambda_k^*)\cdot m \leq q_{k^*} \cdot 2^{k^*}$ and then plug this into the formula of Corollary \ref{c:query_bound} in order to bound the expected number of queries. The calculations can be found in the appendix. The $\tilde{O}$-notation hides $\log$-factors and constants (here we consider the dimension of the space $d$ a constant).

\vspace{-0.5cm}
\begin{table}[h]
\caption{Dyadic trees}
\label{t:active_query_numbers}
\centering
\begin{tabular}{|r|l|}
\hline
Lipschitzness & Bound on expected number of queries\\
\hline
$\phi(\lambda) = \lambda^n$ & $2\cdot\frac{\log({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}} \ln(2) + \ln(1/\delta)}{\epsilon} \cdot({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}} = \tilde{O}(m^{\frac{d}{n+d}} \left(\frac{1}{\epsilon}\right)^{\frac{n}{n+d}})$\\[12pt]
% %\hline
% $\frac{\lambda^d}{\sqrt{d}^d}$ &  $\frac{\sqrt{m}}{\epsilon^{1/2}}\cdot\left(\log(m\epsilon)^{1/2}\ln(2)+\ln(2/\delta))\right) $\\[12pt]
%\hline
$\phi(\lambda) = \e^{\frac{-1}{\lambda}}$ & $\frac{\sqrt{d}^d\log(\epsilon m)^d}{\epsilon} 2(\log(\log((\epsilon m)^{\sqrt{d}}))d\ln(2) +\ln(2/\delta))=\tilde{O}(\frac{1}{\epsilon})$\\[3pt]
\hline 
\end{tabular}
\end{table}

\paragraph{Other spatial trees}
Often, the \emph{intrinsic dimension} of real data is considerably smaller than its Euclidean dimension of its feature space.
\cite{NakulSS12} show that, for various classes of spatial trees, the data diameter decreases as a function of the intrinsic dimension (for several notions of intrinsic dimension). Thus, our analysis implies that the query bounds of PLAL used with these trees scale well with the intrinsic dimension.

% \section{Using PLAL for active learning}
% 
% We saw that given any sample, $S=((x_1, y_1), \ldots, (x_m, y_m))$, the PLAL algorithm takes its unlabeled projection $(x_1, \ldots, x_m)$ as input, queries some of the labels and outputs 
% a labels sample $S'=((x_1, y'_1), \ldots, (x_m, y'_m))$ such that with high probability,  $|\{i : y_i \neq y'_i\}|/m$ is bounded (as a function of the Probabilistic Lipschitzness of the data generating distribution and the number of label queries). In many cases, such a sample suffices for successful learning. Below, we formalize this claim.
% 
% 
% \begin{definition} Given a sequence of labeled instances, $S=((x_1, y_1), \ldots, (x_m, y_m))$ and $\epsilon \geq 0$, define the $\epsilon$-neighborhood of $S$ as 
% $${\cal S}_{\epsilon} = \{ S'=((x_1, y'_1), \ldots, (x_m, y'_m)): ~ |\{i : y_i \neq y'_i\}|/m \leq \epsilon\}.$$
% \end{definition}
% 
% \begin{definition} We say that a learning algorithm, ${\cal A}$,  is $(m, \epsilon, \delta, \eta)$- robust with respect to a data distribution $P$ and a loss function $L$, if, 
% %with probability $\geq (1-\delta)$over samples of size $m$ drawn i.i.d. by $P$, 
% 
% \[P^m\left[ \{S : ~ \forall S' \in {\cal S}_{\epsilon}, ~ L_P({\cal A}(S')) \leq L_P({\cal A}(S)) + \eta\} \right ] \geq (1-\delta)\]
% 
% \end{definition}
% 
% \begin{lemma}\label{robust_works}
% If  ${\cal A}$,  is a learning algorithm which is  $(m, \epsilon, \delta, \eta)$- robust with respect to a data distribution $P$ and a loss function $L$, then on random training samples of size $m$
% generated by $P$, replacing the fully labeled sample with one actively labeled by the PLAL algorithm  with error parameter $\epsilon$ results in deterioration of the loss of ${\cal A}(S)$ by at most $\eta$
% (with probability $\geq (1-\delta)$over the random training samples).
% \end{lemma}
% 
% \begin{lemma}\label{robust_algs}
% If $m \geq m^{UC}_H(\epsilon, \delta, P)$ and ${\cal A}(S)$ is either an ERM algorithm over $H$ or a Regularized Loss Minimization algorithm over $H$, then ${\cal A}$,  is $(m, \epsilon, \delta, 2\epsilon)$- robust with respect to the data distribution $P$ and the loss $L$.
% 
% \end{lemma}
% 
% \textbf{STILL NEED TO DEFINE $m^{UC}_H(\epsilon, \delta, P)$ and $m^{UC}_H(\epsilon, \delta, )$and maybe cite the claim that this function exists for any VC class $H$. }
% 
%  A sample labeled by PLAL labeling procedure can be fed to any learning algorithm that is robust to a small fraction of the training labels being corrupted. 
%  
% % It is not hard to see that any agnostic learner (see Definition \ref{d:agnostic_learner}) enjoys this robustness. Any agnostic learner has sample complexity at least $\Omega(1/\epsilon^2)$, which corresponds to the sample complexity of the ERM paradigm. Thus, any agnostic learner outputs a classifier that is close to an empirical risk minimizer and thus its behavior can not be changed by much when the empirical error changes by $\epsilon$. 
% %
% %\begin{lemma}\label{l:agnostic_noise}
% % %Let $P$ be a distribution over $X\times\ \{0,1\} $, l
% % Let $A$ be an agnostic learner for some hypothesis class $H$ over some domain $\Xcal$ and let $m$ be the sample size required by $A$ to $(\epsilon,\delta)$-learn $H$. Then, with probability at least $(1-\delta)$ over an \iid sample of size $m$ from some distribution $P$ over $\Xcal$  with at most and $\epsilon$-fraction of corrupted labels, $A$ outputs a hypothesis $h$ with $\err_P(h)\leq \opt_H(P)+4\epsilon$.
% %\end{lemma}
% 
% 
% \paragraph{Label savings}
% 
% Table \ref{t:active_query_numbers} provides an upper bound on the number of label queries the PLAL procedure makes given a sample of size $m$. If the learning task satisfies exponentioal Lipschitzness ($\phi(\lambda) = \e^{\frac{-1}{\lambda}}$), then expected number of queries is bounded by $1/\epsilon$ times a factor that is logarithmic in the original sample size $m$. Thus, for any algorithm with (fully supervised) sample complexity $m=\Omega((1/\epsilon)^\alpha)$ for some $\alpha >1$, labeling the sample with PLAL reduces the label complexity. 
% 
% Similarly, if we have polynomial Lipschitzness ($\phi(\lambda) = \lambda^n$), then the expected number of queries is bounded by $\tilde{O}(m^{\frac{d}{n+d}} \left(\frac{1}{\epsilon}\right)^{\frac{n}{n+d}})$. For an algorithm with (fully supervised) sample complexity $m=\Omega((1/\epsilon)^\alpha)$, this yields $\tilde{O}( \left(\frac{1}{\epsilon}\right)^{\frac{n + \alpha d}{n+d}}))$ as a bound on the expected number of queries. This implies that, again, we labeling with PLAL reduces the label complexity if $\alpha >1$.
% 
% The sample complexity of an agnostic learner gets reduced from $\Theta(1/\epsilon^2)$ to $\tilde{O}(1/\epsilon)$ in the case of exponential Lipschitzness and to $\tilde{O}( \left(\frac{1}{\epsilon}\right)^{\frac{ n+ 2 d }{n+d}}))$ in the case of polynomial Lipschitzness. 
% 

% \section{Agnostic PAC-learning with PLAL}
% 
% Here, we show how to use the PLAL labeling method to get an agnostic active learner for any hypothesis class of finite VC-dimension. We'll refer to this method as \emph{Two-Stage-PLAL}. 
% 
% We use Lemma \ref{l:agnostic} (from Section \ref{ssec:generallearning}), which formalizes the robustnes of agnostic learners to input samples with errors. The Lemma tell us that if we feed an agnostic learner with a sample that was labeled by a function of error at most $\epsilon$, it will output a classifier of error at most  $\opt_H(P)+3\epsilon$ (instead of $\opt_H(P)+\epsilon$).
% 
% Ideally, we would like to directly apply this lemma to a sample that is labeled by our PLAL labeling procedure. We showed in the previous section that at most an $\epsilon$-fraction of the points are getting errounous labels. However, 
% %PLAL can not be viewed as a function from $\Xcal\to\{0,1\}$ of error at most $\epsilon$. The 
% the labeling behaviour of PLAL depends on the sample, whereas the Lemma \ref{l:agnostic} requires the labeling function $f$ to be fixed \emph{before} drawing the sample.
% 
% We can get around this, by drawing an unlabeled sample of double size (or dividing the available unlabeled data uniformly at random into two equal size parts), running PLAL on the first half $S_1$ and then labeleling the second half $S_2$ as follows: We label a point in $S_2$, that lies in a cell that was declared homogeneous, with the label of this cell. For points that lie in cells, that were declared inactive because all points in them were queried, we query the label. This will, on expectation, at most double the number of queries.
% 
% Now, $S_2$ is in fact labeled by a function that is independent of the sample $S_2$ and has small error. The function is constant on the homogeneous cells, and identcal to the true labeling function on all other cells. For a sample $S$ from some distribution and a run of PLAL on $S$, we denote this function by $f_S$ and we show that it has error at most $\epsilon$ with respect to the underlying distribution:
% 
% \begin{lemma}\label{l:f_S}
%  Let $P$ be a distribution over $[0,1] \times\ \{0,1\} $, $S$ an unlabeled \iid sample drawn from $P$'s marginal, and let $f_S$ be the labeling function that is induced by running PLAL with query numbers $q_k = \frac{k\cdot d \ln(2) + \ln(2/\delta)}{\epsilon}$ on $S$.
% %  as follows: On all homogeneous (in the run of PLAL on $S$) cells, $f_S$ assigns the label seen in this cell, on all other cells $f_S$ is identical to the labeling function $l$ of $P$. 
%  Then with probability at least $1-\delta$, over the choice of $S$ and the run of PLAL, $f_S$ has error at most $\epsilon$.
% \end{lemma}
% 
% \begin{proof}
%  We need to show that $f_S$ restricted to a homogeneous cell $C$ has error at most $\epsilon$ with respect to the distribution $P_C$, the restriction of $P$ to this cell $C$. Note that $C\cap S$ can be considered a \iid sample from this distribution. PLAL chooses a subsample to label uniformly at random, thus the subsample can also be considered an \iid sample from $P_C$. Now the claim follows from Lemma \ref{l:homogenous_sample}, noting that the choice of query numbers of PLAL ensures that this holds simultaneously for all homogenous cells with probability at least $1-\delta$.
% \end{proof}
% 
% We can thus feed the labeled sample $S_2$ to an agnostic learner for a class $H$ and, with high probability, it will ouput a classifier of small error. The following theorem provides a precise formulation of this result. We use the notation $F(\phi, m, d, \epsilon, \delta)$ to denote the bound on the number of queries of PLAL in the setting of Corollary \ref{c:query_bound}.
% 
% \begin{theorem}\label{t:agnostic_learning}
%  Let $H$ be a hypothesis class over the domain $\Xcal =[0,1]^d$ and $A$ an agnostic learner for $H$. Let $m$ be such that $A$ $(\epsilon,\delta)$-learns $H$ when fed with a labeled sample of size $m$. Then, for evey distribution $P$ over $\Xcal \times \{0,1\}$ with marginal $P_\Xcal$, our Two-Stage-PLAL method with query numbers $q_k = \frac{k\cdot d \ln(2) + \ln(2/\delta)}{\epsilon}$, when fed with an unlabeled \iid sample from $P_\Xcal$ of size $2m$ outputs a classifier of $P$-error at most $\opt_H + 3\epsilon$ with probability at least $1-\delta$. If 
%  \[
%  m \geq 2\frac{16}{\epsilon^2}\left(d \ln(\frac{16d}{\epsilon^2}) + \ln(\frac{8}{\delta})\right),
%  \], 
%  the expected number of label queries of Two-Stage-PLAL is bounded by $2\cdot F(\phi, m, d, \epsilon, \delta)$.
% \end{theorem}
% 
% \begin{proof}
%  The guarantee on the error follows directly from Lemmas \ref{l:agnostic} and \ref{l:f_S}.  
%  If $m \geq  2 \frac{16}{\epsilon^2}\left(d \ln(\frac{16d}{\epsilon^2}) + \ln(\frac{8}{\delta})\right)$, then with probability $1-2\delta$ both $S_1$ and $S_2$ are $\epsilon$-approximations of the class of axis alligned rectangles, see \cite{HausslerWelzl86}. Thus, for each cell $C$, we have 
%  \[
%   ||S_i\cap C| - D(C)| \leq \epsilon m,
%  \]
%  for $i\in\{1,2\}$, which implies
%  \[
%   ||S_1\cap C| - |S_2\cap C|| \leq 2 \epsilon m,
%  \]
%  We can bound the number of queries for the second half of the sample with the exact same arguments used for bounding the number of queries of PLAL on the first half of the sample, see proof of Theorem \ref{t:query_bound}. For each level $k$, the expected number of points of $S_2$ that lie in heterogeneous cells at this level is bounded by $\phi(\lambda_k)$. 
%  
%  Consider the partition of the space into cells that was produced by the run of PLAL on $S_1$. We can assign a level to each of the cells in the partition (a cell of side-lengh $1/2^k$ is at level $k$).
% \end{proof}
% 
% \begin{corollary}
%  If for some the agnostic learner $A$ in Theorem \ref{t:agnostic_learning} the Lipschitz function $\phi$, $\epsilon$, $\delta$, and $d$ are such that $m_A(\epsilon,\delta) > 2\cdot F(\phi, 2m_A(\epsilon/3,\delta), d, \epsilon,\delta)$, then Two-Stage-PLAL saves labels.
% \end{corollary}


% \section{Static active learning}
% 
% \subsection{The algorithm}
% 
% %\ruth{This algorithm, in principle needs the Lipschitzness (or the level $k$) as
% %input. Is there any way around this? Can we have some experiments on this too? Do we want this Static active learning at all???}
% Our algorithm for SAL gets an unlabeled \iid sample and an integer $k$ input
% and then partitions the space into $2^{kd}$ axis-alligned rectangles of
% sidelength $1/2^k$. Given an unlabeled sample $S$, the  algorithm
% queries the label of one point in $S$ in each cell. Then every point is labeled with the label
% of that labeled point in its cell.
% 
% \subsection{Error bound}
% Note that the diameter of an axis-alligned rectangles of
% sidelength $2^k$ is $\frac{\sqrt{d}}{2^{k}}$. 
% For a given $\epsilon$ and probabilistic Lipschitz function $\phi$, let $k^*$ be such that
% $\phi(\frac{\sqrt{d}}{2^{k^*}})\leq \epsilon$. Every point $x$ in a cell that contains more than one label violates the $\frac{\sqrt{d}}{2^{k}}$-Lipschitz condition with a point $y$ from that same box but the opposing label.
% Thus, by the definition of the probabilistic Lipschitzness, the sum of the weight of
% the cells of sidelength $1/2^{k^*}$ that contain points of both labels is
% smaller than $\epsilon$. Thus, our algorithm produces a classifier of error at
% most $\epsilon$. 
% 
% \subsection{Bound on Number of queries}
% The following table summarizes the query compplexity of our algorithm under various Lipschitz assumptions:
% \begin{table}[h]
% \caption{Number of queries for achieving error $\leq\epsilon$}
% \label{t:static_query_numbers}
% \centering
% \begin{tabular}{|r|l|}
% \hline
% Lipschitzness $\phi$ & Bound on number of queries\\
% \hline
% $\phi(\lambda) = \lambda$ & $\log(\frac{\sqrt{d}}{\epsilon})^d$\\
% \hline
% $\phi(\lambda) = (\frac{\lambda}{\sqrt{d}})^d$ & $\frac{\sqrt{d}^d}{\epsilon^{\frac{d}
% {n}}}$  \\
% \hline
% $\phi(\lambda) = \e^-1/\lambda$ &  $(\ln(1/\epsilon)\sqrt{d})^d$\\
% \hline 
% \end{tabular}
% \end{table}

