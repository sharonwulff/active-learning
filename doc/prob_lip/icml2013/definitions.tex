\section{Definitions}

\subsection{General Learning} \label{ssec:generallearning}

We fix some domain set $\Xcal$ and some label set $\Ycal$, (which, for concreteness, will be the unit cube $[0,1]^d$
and the binary set $\{0,1\}$, respectively).
A \emph{hypothesis} or label predictor, is a function $h:\Xcal\to \Ycal$, and a hypothesis class is a set of hypotheses.
We assume that the data for a learning problem is generated by some  \emph{target distribution} $P$ over
$\Xcal\times \{0,1\}$.  We denote the marginal distribution of $P$ over $\Xcal$ by $D$ % and its density by $d$.
and let $l:\Xcal\rightarrow\{0,1\}$  denote the labeling function of this distribution. For some function  $h:X\to\{0,1\}$ we define the
\emph{error} of $h$ with respect to $P$ as $\err_P(h)=\Pr_{(x,y)\sim P}(y\neq h(x))$.
For a class $H$ of hypotheses on $\Xcal$,
let the smallest error of a hypothesis $h\in H$ with respect to $P$ be denoted by $\opt_H(P):=\min_{h\in H}\err_P(h)$.

A \emph{learner} $A$ takes a sample $S=((x_1,y_1),\dots,(x_n,y_n))$ generated \iid from $P$ and ouputs a hypothesis $h:X\to\{0,1\}$. Formally
\[
\textstyle
A : \bigcup_{m = 1}^\infty (\Xcal \times \{0,1\})^m \to \{0,1\}^\Xcal \; .
\]

We use the folowing common definition of learnability:
\begin{definition}
 An algorithm $A$ is an \emph{agnostic PAC learner} for some hypothesis class $H$ over $\Xcal$ if for all $\epsilon>0$ and $\delta>0$ there exists a sample size $m=m(\epsilon,\delta)$ such that, for all distributions $P$ over $\Xcal\times\{0,1\}$, when given an \iid sample of size $m$ from $P$, then with probability at least $1-\delta$ over the sample, $A$ outputs a classifier $h: X\to\{0,1\}$ with error at most $\opt_H(P)+\epsilon$. In this case, we also say that the learner $(\epsilon,\delta)$-learns $H$.
\end{definition}

 
\subsection{Active Learning} \label{ssec:activelearning}
An \emph{active learner} receives an unlabeled sample $S=(x_1,\dots,x_m)$ generated \iid by $D$. The learner can then sequentially query labels for points in $S$, \ie the learner chooses indices $i_1,\dots, i_n \in\{1,\dots m\}$ and receives the labels $y_{i_1},\dots y_{i_n}$ generated by $l(x_{i_1}),\dots l(x_{i_n})$ repectively. At each step, the choice of each $i_j$ can depend on the set $S$ and the labels $y_{i_1},\dots y_{i_{j-1}}$ seen so far. An active learner is evaluated as to whether it achieves the same performace as a standard learner (in terms of the error of the classifier it ouputs) while requiring less labels.

One can also consider an active learning regime, where the choice of label queries is allowed to only depend on the unlabeled sample, but not on the previously seen labels, \ie the learner needs to request all points to label at once. We call this setting \emph{Static Active Learning (SAL)}. As opposed to this we call the setting where the choice of points to query is allowed to depend on previous queries, \emph{Adaptive Active Learning (AAL)}.

\subsection{Probabilistic Lipschitzness}

We first recall that a function $f:\Xcal\to \reals$ satisfies the (standard) $\lambda$-Lipschitz property 
%with Lipschitz constant $\lambda$ 
(with respect to the underlying metric $\mu$), if
$
|f(x)-f(y)| \le \lambda \, \|x-y\|
$
holds for all $x,y\in \Xcal$.
This condition can be readily applied to probabilistic labeling rules $l : X \to [0,1]$.
However, 
%if the labeling function is deterministic, namely if $l(x)\in\{0,1\}$ for all $x \in \Xcal$, 
this requirement forces a $1/\lambda$ gap between differently labeled points, and will thus fail whenever $l$ is non-constant on some connected subregion of the distributions support. 
A natural relaxation is to require that the inequality will hold only with some high probability. Namely, 
%with a probability of at least $1-\phi(\lambda)$ over $x$. That is,
\begin{definition}[Probabilistic Lipschitzness] \label{def:newProbLip}
Let $\phi : \reals \to [0,1]$.
 We say that $f : \Xcal \to \reals$ is $\phi$-Lipschitz w.r.t. a distribution $D$ over $\Xcal$ if, for all $\lambda > 0$:
 \[
 \Pr_{x \sim D} [ \exists y :  |f(x)-f(y)| > \lambda\,\|x-y\| ] \le \phi(\lambda)
 \]
\end{definition}
This definition generalizes the standard definition since, given any $\lambda>0$, setting $\phi(a) = 1$ for $a < \lambda$ and $\phi(a) =0$ for $a \ge \lambda$ results in the standard $\lambda$--Lipschitzness condition.

This probabilistic Lipschitzness condition can be viewed as a way of formalizing the \emph{cluster assummption} of the data, an assumption that is often made to account for the success of  semi-supervised learning. It implies that the data can be divided into clusters that are almost label-homogeneous and are separated by low-density regions. See \cite{ourSSLpaper} for such an application
of a similar notion.


\subsubsection{Examples}

\begin{description}
 \item[Linear Lipschitzness]Let $D$ be the uniform distribution over $\Xcal=[0,1]^d$. If $l$ is  a linear separator, then $\phi(\lambda)=C\cdot \lambda$, for some constant $C$.
% \sim \lambda$.
 \item[Polynomial Lipschitzness]
 If $l$ gives a discrete set of points one label and the other label to
the rest of the domain, then, for some threshold $t_{l}$, as long as $\lambda
> t_{l}$
$\phi(\lambda)$ is some constant and below that level it behaves like
$\lambda^d$ (once $\lambda$ is small enough so that the $\lambda$ balls around
the one-labeled points are disjoint).
\ruth{give another example with non-uniform marginal}
 \item[Exponential Lipschitzness]
 \begin{figure}\caption{Exponential Lipschitznes}\label{ExpLip}
 \includegraphics{ExpLip.jpg}
\end{figure}
 We let the domain be the unit interval $[0,1]$ and let the labeling rule be $0$ for  $\lambda\leq 1/2$ and $1$ for $\lambda>1/2$. Now we let the density $d$ of the distribution form clusters by setting $d(\lambda)=c\e^{-1/\lambda}$ for $0\leq \lambda \leq 1/4$, $d(\lambda)=c\e^{-1/|1/2-\lambda|}$ for $1/4\leq \lambda \leq 3/4$
and $d(\lambda)=c\e^{-1/|1-\lambda|}$ for $3/4\leq \lambda \leq 1$ with $c=(2\e^{-1/4})^{-1}$. This distribution is not $\lambda$-Lipschitz for any constant $\lambda$, since there exist arbitrarily close points with opposing labels. But, we have
\begin{align*}
      & \Pr_{\lambda\sim D} \big[ \exists y \big|~ f(\lambda)\neq f(y) \land ||\lambda-y|| \le \lambda \big] \\
 \leq & \int_{1/2-\lambda}^{1/2-\lambda} c\e^{-1/|1/2-\lambda|}d\lambda \leq  \e^{-1/\lambda}\\
\end{align*}
See Figure \ref{ExpLip} for illustration.
\end{description}




% Let us consider some concrete cases (of data distributions) to get an intuition
% of how the ProbLip function $D_h(\lambda)$ behaves:
% \begin{enumerate}
% \item Let $D$ be the uniform distribution over $\Xcal=[0,1]^d$. 
% \begin{itemize}
% \item If $l$ is  a linear separator, then (it looks like) $\phi(\lambda)
% \sim \lambda$.
% \item If $l$ gives a discrete set of points one label and the other label to
% the rest of the domain, then, for some threshold $t_{l}$, as long as $\lambda
% > t_{l}$
% $\phi(\lambda)$ is some constant and below that level it behaves like
% $\lambda^d$ (once $\lambda$ is small enough so that the $\lambda$ balls around
% the one-labeled points are disjoint).
% \end{itemize}
% \item If $D$ is a mixture of separated Gaussians (say their variances are
% smaller than the distances between their means),
% \begin{itemize}
% \item If the labeling function $h$ is such that each Gaussian is 
% label-homogenous, then $\phi(\lambda)$ goes down exponentially fast with
% $\lambda$.
% Namely, $\phi(\lambda) \sim e^{-\sigma/\lambda}$ (where $\sigma$ is the
% variance of the Gaussians).
% \item If the labeling function has mixed labels over a Gaussian (or several
% Gaussians) or total weight $p$, then $\phi(\lambda)$ is essentially constant
% $p$.
% \end{itemize}
%\end{enumerate}