\section{Experiments}
\label{sec:experiments}
We designed experiments on synthetic data to empirically evaluate two aspects of the PLAL labeling framework.
The first is how well the PLAL algorithm performs in terms of maintaining low prediction 
error while reducing the number of required labels. We compare the prediction error induced by a supervised 
classifier trained on the subset of the data requested by the PLAL, to the error induced by a 
classifier trained on randomly sampled subset of equal size. 
% A similar experiment on real world data 
% was conducted in~\cite{DasguptaH08}, where Hierarchical clustering with Ward's linkage criterion served 
% as the clustering method. Here we demonstrate that a fixed partition of the space 
The second aspect and perhaps the more interesting one, is how the reduction in labeled sample size relates to the (empirical)
probabilistic Lipschitzness of the data. To address this question without assuming access to 
the true data generating distribution, we use an estimator which adheres to a PL definition
in which the probability of finding a $\lambda$-close point with different label, is bounded (but is not necessarily zero).  

\subsection{Synthetic Data Description}
\label{sec:syndata}
We generated $3$ datasets, each consisting of $2000$ samples from a mixture of multivariate Gaussian distributions.
The distributions included $4$ dense Gaussian, as well as $4$ sparse Gaussian, with the same parameters governing the density
of each group, but each dataset having different sets of values. We used a different label for the samples associated with each 
Gaussian, resulting in a multi-label classification task with $8$ labels. 
While the covariance matrix parameters (``dense'' and ``sparse''), 
of the each dataset were fixed, we varied the dimensionality of the generated data in the different experiments. 
We always sampled the dense Gaussian means close to the ``corners'' of the space, whereas the means of the sparse ones 
we sampled uniformly at random. This procedure essentially allowed us to create datasets exhibiting different empirical 
PL behaviors, by varying the covariances of the dense and sparse Gaussian of each dataset. We used diagonal covariance matrices to
avoid extra noise and sampled $~80\%$ of the points from the dense Gaussian, and the remaining $20\%$ from the sparse ones. 
The datasets are denoted by $A$,$B$, and $C$ corresponding to: $A$-$0.1$ dense variance and $1$ sparse variance, 
$B$-$.01$ dense variance and $.1$ sparse variance, and $C$-$.001$ dense variance and $.1$ sparse variance. 
With this choice of parameters the datasets can be intuitively casted as the most clusterable being $C$ to the least clusterable, 
or least separable, being $A$.


\subsection{Empirical Probabilistic Lipschitzness}
We plotted the empirical PL of the datasets $A$,$B$, and $C$ with dimensions $5$, $15$ and $25$. 
The lambda values range between $0-10$, for each $\lambda$ value we calculated the empirical $\phi(\lambda)$ as the 
percentage of data points having at least $\lambda$ close neighbor with a different label.   
The results are shown in Figure~\ref{fig:emp_pl}.

\begin{figure}[htb]
    \centering
    \def\experimentprefix{include/data/}

    \begin{minipage}[c]{0.33\textwidth}
        \def\dim{5}
        \def\showylabel{1}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \caption{The empirical $\phi(\lambda)$ as a function of $\lambda$ in the range $0-10$ for datasets 
    $A$,$B$, and $C$ described in~\ref{sec:syndata}}    
    \label{fig:emp_pl}
\end{figure}

\subsection{Classification}

\begin{figure}[h]

    \centering
    \def\ymin{0}
    \def\experimentprefix{include/data/}

    \def\dataset{A}
    \def\ymax{80}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
   % \vspace{-0.4cm}
    
    \def\dataset{B}
    %\def\ymax{60}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    %\vspace{-0.4cm}
    
    \def\dataset{C}
    %\def\ymax{25}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \caption{The average number of queries requested by PLAL on datasets $A$,$B$, and $C$ for different values of $\epsilon$
	      is denoted as $\%-queries$. The average prediction error of the nearest neighbor classifier with PLAL queries 
	      is denoted as \% NN-PLAL-error and the prediction error
	      of the nearest neighbor classifier with random queries is denoted as \% NN-random-error}    
    \label{fig:classification}
\end{figure}

In this experiment we varied $\epsilon$ in the range $(0.01,0.05,0.1,0.15,0.2,0.25,0.3)$, and for each $\epsilon$ value
we computed the PLAL queries. We sampled uniformly at random an equal number of points to serve as a benchmark. We used a $K$ 
nearest neighbor classifier to compute predictions on a test set using the PLAL queries as well as the randomly sampled ones. 
We used $K$ values in the range $(1,3,5,10)$, and chose the best $K$ for every run. We repeated this procedure $5$ times 
and we report the average values for each configuration. We computed the prediction error as the percentage of labels which 
differ between the predictions and the true ones. 
The results on the datasets $A$,$B$, and $C$ with dimensions $5$, $15$ and $25$ are shown in Figure~\ref{fig:classification}. 

The average number of queries requested by PLAL is plotted in red and is denoted as $\%-queries$. The prediction error 
of the nearest neighbor classifier with PLAL queries is denoted as \% NN-PLAL-error whereas the prediction error
of the nearest neighbor classifier with random queries is denoted as \% NN-random-error.
The plots strengthen the intuition that the PLAL labeling framework will save the most labels on datasets which are
more clusterable. The empirical PL behavior of the datasets matches the clusterability classification. Dataset $A$ 
for all choices of dimensions, exhibits the fastest increase of $\phi(\lambda)$ whereas dataset $C$ is the slowest.
It is evident in the plots that the PLAL algorithm is more sensitive than random sampling to very small sample sizes. 
On such instances the overall error is close to the bound.
