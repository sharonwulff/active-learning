\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{colt2013}[2013/01/09 v1.0 COLT 2013 Proceedings]

\@ifundefined{ifanonsubmission}{
  \newif\ifanonsubmission
  \anonsubmissionfalse
}{}

\@ifundefined{iffinalsubmission}{
  \newif\iffinalsubmission
  \finalsubmissionfalse
}{}

\DeclareOption{anon}{\anonsubmissiontrue}
\DeclareOption{final}{\finalsubmissiontrue}
\ProcessOptions\relax

\LoadClass[wcp]{jmlr}

\jmlrvolume{vol 30}
\jmlryear{2013}

\ifanonsubmission
 \newcommand{\coltauthor}[1]{}
 \author{\vspace{-0.5cm}author names withheld}
 \editor{Under Review for COLT 2013}
 \jmlrworkshop{26th Annual Conference on Learning Theory}
 \renewcommand{\acks}[1]{}
\else
 \newcommand{\coltauthor}[1]{\author{#1}}
 \iffinalsubmission
  \editor{Shai Shalev-Shwartz, Ingo Steinwart}
  \jmlrworkshop{26th Annual Conference on Learning Theory}
 \fi
\fi



