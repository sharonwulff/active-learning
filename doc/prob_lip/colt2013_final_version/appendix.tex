\section*{Appendix}

\subsection*{Experiments}
\label{sec:experiments}

We designed experiments on synthetic data to empirically evaluate two aspects of the PLAL labeling framework.
The first is how well the PLAL algorithm performs in terms of maintaining low prediction 
error while reducing the number of required labels. We compare the prediction error induced by a supervised 
classifier trained on the subset of the data requested by the PLAL, to the error induced by a 
classifier trained on randomly sampled subset of equal size. 
% A similar experiment on real world data 
% was conducted in~\cite{DasguptaH08}, where Hierarchical clustering with Ward's linkage criterion served 
% as the clustering method. Here we demonstrate that a fixed partition of the space 
The second aspect and perhaps the more interesting one, is how the reduction in labeled sample size relates to the (empirical)
probabilistic Lipschitzness of the data. To address this question without assuming access to 
the true data generating distribution, we use an estimator which adheres to a PL definition
in which the probability of finding a $\lambda$-close point with different label, is bounded (but is not necessarily zero).  

\subsubsection*{Synthetic Data Description}
\label{sec:syndata}
We generated $3$ datasets, each consisting of $2000$ samples from a mixture of multivariate Gaussian distributions.
The distributions included $4$ dense Gaussian, as well as $4$ sparse Gaussian, with the same parameters governing the density
of each group, but each dataset having different sets of values. We used a different label for the samples associated with each 
Gaussian, resulting in a multi-label classification task with $8$ labels. 
While the covariance matrix parameters (``dense'' and ``sparse''), 
of the each dataset were fixed, we varied the dimensionality of the generated data in the different experiments. 
We always sampled the dense Gaussian means close to the ``corners'' of the space, whereas the means of the sparse ones 
we sampled uniformly at random. This procedure essentially allowed us to create datasets exhibiting different empirical 
PL behaviors, by varying the covariances of the dense and sparse Gaussian of each dataset. We used diagonal covariance matrices to
avoid extra noise and sampled $~80\%$ of the points from the dense Gaussian, and the remaining $20\%$ from the sparse ones. 
The datasets are denoted by $A$,$B$, and $C$ corresponding to: $A$-$0.1$ dense variance and $1$ sparse variance, 
$B$-$.01$ dense variance and $.1$ sparse variance, and $C$-$.001$ dense variance and $.1$ sparse variance. 
With this choice of parameters the datasets can be intuitively casted as the most clusterable being $C$ to the least clusterable, 
or least separable, being $A$.


\subsubsection*{Empirical Probabilistic Lipschitzness}
We plotted the empirical PL of the datasets $A$,$B$, and $C$ with dimensions $5$, $15$ and $25$. 
The lambda values range between $0-10$, for each $\lambda$ value we calculated the empirical $\phi(\lambda)$ as the 
percentage of data points having at least $\lambda$ close neighbor with a different label.   
The results are shown in Figure~\ref{fig:emp_pl}.

\begin{figure}[htb]
    \centering
    \def\experimentprefix{include/data/}

    \begin{minipage}[c]{0.33\textwidth}
        \def\dim{5}
        \def\showylabel{1}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \caption{The empirical $\phi(\lambda)$ as a function of $\lambda$ in the range $0-10$ for datasets 
    $A$,$B$, and $C$ described in~\ref{sec:syndata}}    
    \label{fig:emp_pl}
\end{figure}

\subsubsection*{Classification}
In this experiment we varied $\epsilon$ in the range $(0.01,0.05,0.1,0.15,0.2,0.25,0.3)$, and for each $\epsilon$ value
we computed the PLAL queries. We sampled uniformly at random an equal number of points to serve as a benchmark. We used a $K$ 
Nearest Neighbor classifier to compute predictions on a test set using the PLAL queries as well as the randomly sampled ones. 
We used $K$ values in the range $(1,3,5,10)$, and chose the best $K$ for every run. We repeated this procedure $5$ times 
and we report the average values for each configuration. We computed the prediction error as the percentage of labels which 
differ between the predictions and the true ones. 
The results on the datasets $A$,$B$, and $C$ with dimensions $5$, $15$ and $25$ are shown in Figure~\ref{fig:classification}. 
\begin{figure}[htb]

    \centering
    \def\ymin{0}
    \def\experimentprefix{include/data/}

    \def\dataset{A}
    \def\ymax{80}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
   % \vspace{-0.4cm}
    
    \def\dataset{B}
    %\def\ymax{60}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    %\vspace{-0.4cm}
    
    \def\dataset{C}
    %\def\ymax{25}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \caption{The average number of queries requested by PLAL on datasets $A$,$B$, and $C$ for different values of $\epsilon$
	      is denoted as $\%-queries$. The average prediction error of the Nearest Neighbor classifier with PLAL queries 
	      is denoted as \% NN-PLAL-error and the prediction error
	      of the Nearest Neighbor classifier with random queries is denoted as \% NN-random-error}    
    \label{fig:classification}
\end{figure}

The average number of queries requested by PLAL is plotted in red and is denoted as $\%-queries$. The prediction error 
of the Nearest Neighbor classifier with PLAL queries is denoted as \% NN-PLAL-error whereas the prediction error
of the Nearest Neighbor classifier with random queries is denoted as \% NN-random-error.
The plots confirm the intuition that the PLAL labeling framework will save the most labels on datasets which are
more clusterable. The empirical PL behavior of the datasets matches the clusterability classification. Dataset $A$ 
for all choices of dimensions, exhibits the fastest increase of $\phi(\lambda)$ whereas dataset $C$ is the slowest.
It is evident in the plots that the PLAL algorithm is more sensitive than random sampling to very small sample sizes. 
On such instances the overall error is close to the bound.

\subsection*{Proofs for Table \ref{t:active_query_numbers}}
\begin{description}
 \item[Polynomial Lipschitzness] Assmume $\phi(\lambda)=\lambda^n$. 
  We need to find a $k$ such that 
  \[
   \phi(\lambda_k)\cdot m \leq q_k 2^{k},
  \]
  where $q_k=  \frac{k\cdot 2\ln(2) + \ln(1/\delta)}{\epsilon}$.
  Note that, if this inequality holds for some some value $k = k^*$ it will also hold for all $k\geq k^*$.
  We have $\lambda_{kd} = \frac{\sqrt{d}}{2^k}$.
  We show that for
  \[
   k = \log({\sqrt{d}^n}m \epsilon)^{\frac{1}{n+d}}
  \]
  we have
  \[
   \phi(\lambda_{kd})\cdot m \leq q_{kd} 2^{kd},
  \]
  With the above value for $k$ we get
  \[
   k = \frac{\log({\sqrt{d}^n}m \epsilon)}{n+d},
  \]
  thus
  \[
   2^{k(n+d)} = {\sqrt{d}^n} m \epsilon,
  \]
  thus
  \[
   2^{kd} \frac{1}{\epsilon} \geq \frac{\sqrt{d}^n}{2^{kn}} m,
  \]
   thus
  \[
   2^{kd} \frac{kd\cdot 2 \ln(2) + \ln(1/\delta)}{\epsilon} \geq \frac{\sqrt{d}^n}{2^{kn}} m,
  \]
   which is what we needed to show. Thus we can set $k^* = kd = d\log({\sqrt{d}^n}m \epsilon)^{\frac{1}{n+d}}$.
   According to Corrolary \ref{c:query_bound} the number of queries is now bounded by
  \begin{align*}
      & 2\cdot\frac{k^* \cdot 2\ln(2) + \ln(1/\delta)}{\epsilon} \cdot  2^{k^*}\\
   =~ & 2\cdot\frac{kd \cdot 2 \ln(2) + \ln(1/\delta)}{\epsilon} \cdot  2^{kd}\\
   =~ & 2\cdot\frac{\log({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}} \ln(2) + \ln(1/\delta)}{\epsilon} \cdot({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}}
         =\tilde{O}(m^{\frac{d}{n+d}} \left({1}/{\epsilon}\right)^{\frac{n}{n+d}})\\
   \end{align*}
%    This bound gets better, the larger $n$ is. We next consider a special case where $n=d$.
%  \item[Polynomial Lipschitzness] Assmume $\phi(\lambda)=\frac{\lambda}{\sqrt{d}}^d$.   
%   In this case we can show (with the same calculation as above) that the inequality
%  \[
%    \phi(\lambda_k)\cdot m \leq q_k 2^{kd},
%   \]
%   holds for
%   \[
%   k^* = \log(m\epsilon)^{\frac{1}{2d}}.
%   \]
%   Subsituting this into the bound of Theorem \ref{t:query_bound} yields
%   \begin{align*}
%       & \frac{2(k^* d \ln(2) + \ln(2/\delta))}{\epsilon} \cdot  2^{k^* d}\\
%    =~ & \frac{2(\log(m\epsilon)^{\frac{1}{2d}} d \ln(2) + \ln(2/\delta))}{\epsilon} \cdot (m\epsilon)^{\frac{1}{2}} \\
%    =~ & 2(\log(m\epsilon)^{\frac{1}{2d}} d \ln(2) + \ln(2/\delta)) \cdot m^{\frac{1}{2}} \epsilon^{-\frac{1}{2}} \\
%    \end{align*}
 \item[Exponential Lipschitzness] Assume $\phi(\lambda)=\e^{-\frac{1}{\lambda}}$. 
  We need to find a $k$ such that 
  \[
   \phi(\lambda_k)\cdot m \leq q_k 2^{kd},
  \]
  where $q_k=  \frac{k\cdot 2\ln(2) + \ln(1/\delta)}{\epsilon}$.
  Note that, if this inequality holds for some some value $k = k^*$ it will also hold for all $k\geq k^*$.
  We have $\lambda_{kd} = \frac{\sqrt{d}}{2^k}$.
  We show that for
  \[
   k = \log(\log(\epsilon m )^{\sqrt{d}})
  \]
  we have
  \[
   \phi(\lambda_{kd})\cdot m \leq q_{kd} 2^{kd},
  \]
  With the above value for $k$ we get
  \[
   \frac{2^k}{\sqrt{d}} =\log(\epsilon m ),
  \]
  thus
  \[
   kd + \frac{2^k}{\sqrt{d}}\log(e) \geq \log(\epsilon m ),
  \]
   thus
  \[
   2^{kd} \e^{\frac{2^k}{\sqrt{d}}} \geq \epsilon m,
  \]
   thus
  \[
   (kd 2\ln(2) + \ln(1/\delta)) \cdot 2^{kd} \e^{\frac{2^k}{\sqrt{d}}} \geq \epsilon m,
  \]
   thus
  \[
   \frac{(kd 2\ln(2) + \ln(1/\delta))}{\epsilon} \cdot 2^{kd}  \geq  \e^{\frac{2^k}{\sqrt{d}}} m,
  \]
   which is what we needed to show. Thus we can set $k^* = kd = d\log(\log(\epsilon m )^{\sqrt{d}})$.
   According to Corrolary \ref{c:query_bound} the number of queries is now bounded by
  \begin{align*}
      & 2\cdot\frac{k^* \cdot 2\ln(2) + \ln(1/\delta)}{\epsilon} \cdot  2^{k^*}\\
   =~ & 2\cdot\frac{kd \cdot 2\ln(2) + \ln(1/\delta)}{\epsilon} \cdot  2^{kd}\\
   =~ &  \frac{2(\log(\log(\epsilon m )^{\sqrt{d}}) d \ln(2) + \ln(1/\delta))}{\epsilon} \cdot  (\log(\epsilon m )^{\sqrt{d}})^d\\
   =~ &  \frac{\sqrt{d}^d\log(\epsilon m)^d}{\epsilon} 2(\log(\log((\epsilon m)^{\sqrt{d}}))d\ln(2) + \ln(1/\delta))\\
  \end{align*}
\end{description}


\subsection*{Proof of Lemma \ref{robust_algs}}

\paragraph{ERM algorithms} For some sample $S$, we let $h_S$ denote the empirical risk minimizer in $H$ with respect to $S$, \ie $h_S=\argmin_{h\in H}\err_S(h)$. By Definition \ref{d:robust} we need to show that 
$$
\Pr_{S \sim P^m}\left[~ \forall S' \in {\Ncal}_{\epsilon}(S), ~ \err_P(h_{S'})) \leq \err_P(h_S) + 4\epsilon  \right ] \geq (1-\delta).
$$
By the Definition \ref{UC-complexity} (uniform convergence property) we know that a sample of size at least $m\geq m_H^{UC}(\epsilon,\delta)$ is $\epsilon$-representative for $H$ with probability at least $1-\delta$. Thus, we now assume that the sample $S$ is $\epsilon$-representative and it remains to show that we have for all $S'\in\Ncal(S)$:
\[
 \err_P(h_{S'})\leq \err_P(h_S) +4\epsilon.
\]
We have 
\begin{align*}
 \err_P(h_{S'}) & ~\leq~ \err_S(h_{S'}) + \epsilon & \text{as $S$ is $\epsilon$-representative}\\
                & ~\leq~ \err_{S'}(h_{S'}) + 2\epsilon & \text{as $S'\in \Ncal(S)$}\\
                & ~\leq~ \err_{S'}(h_{S})  + 2\epsilon & \text{by definition of $h_{S'}$}\\
                & ~\leq~ \err_{S}(h_{S})  + 3\epsilon & \text{as $S'\in \Ncal(S)$}\\
                & ~\leq~ \err_{P}(h_{S})  + 4\epsilon & \text{as $S$ is $\epsilon$-representative}\\
\end{align*}
\paragraph{RLM algorithms} Now, for some sample $S$, we let $h_S$ denote the regularized risk minimizer in $H$ with respect to $S$, \ie $h_S=\argmin_{h\in H}(\err_S(h)+\varphi(h))$. Again, we assume that the sample $S$ is $\epsilon$-representative and now need to show that we have for all $S'\in\Ncal(S)$:
\[
 \err_P(h_{S'})\leq \err_P(h_S) +6\epsilon
\]
We start by proving that 
$$\varphi(h_S)-\varphi(h_{S'})\leq 2\epsilon  \hspace{2cm} (*)$$ 
By way of contradiction, let us assume that, on the contrary, $\varphi(h_S) > \varphi(h_{S'}) +2\epsilon$. Then we get
\begin{align*}
 \err_S(h_{S}) + \varphi (h_S) & ~>~ \err_S(h_{S}) + \varphi (h_{S'}) +2\epsilon & \\
                               & ~\geq~ \err_{S'}(h_{S}) + \varphi (h_{S'}) +\epsilon & \text{as $S'\in \Ncal(S)$}\\
                               & ~\geq~ \err_{S'}(h_{S'}) + \varphi (h_{S'}) +\epsilon & \text{by definition of $h_{S'}$}\\
                               & ~\geq~ \err_{S}(h_{S'}) + \varphi (h_{S'})  & \text{as $S'\in \Ncal(S)$}\\
\end{align*}
This contradicts the definition of $h_S$. With this, we conclude:
\begin{align*}
 \err_P(h_{S'}) & ~\leq~ \err_S(h_{S'}) + \epsilon & \text{as $S$ is $\epsilon$-representative}\\
                & ~\leq~ \err_{S'}(h_{S'}) + 2\epsilon & \text{as $S'\in \Ncal(S)$}\\              
                & ~\leq~ \err_{S'}(h_{S})+ (\varphi(h_S)-\varphi(h_{S'})) + 2\epsilon & \text{by definition of $h_{S'}$}\\
                & ~\leq~ \err_{S'}(h_{S})+ 4\epsilon & \text{by (*)}\\
                & ~\leq~ \err_{S}(h_{S})  + 5\epsilon & \text{as $S'\in \Ncal(S)$}\\
                & ~\leq~ \err_{P}(h_{S})  + 6\epsilon & \text{as $S$ is $\epsilon$-representative}\\
\end{align*}

\subsection*{Proof of Theorem \ref{t:NN_plal}}
We adapt a proof from to \cite{ourSSLpaper} (see \cite{SSLSupplement}) for the success of a Nearest Neighbnor algorithm under Lipschitzness to the modified version of $1$-NN with PLAL. We will show that for this algorithm, a sample size of 
\[
 m \geq \left(\frac{1}{\epsilon}\right)^{\frac{d}{n}+1} \frac{(2\sqrt{d})^d}{\delta \e}
\]
for the unlabeled sample $S_u$ suffices to output a classifier of error at most $2\epsilon$ with probability at least $1-\delta$.
Table \ref{t:active_query_numbers} implies that on a sample of size $\tilde{O}(\left(\frac{1}{\epsilon}\right)^{\frac{d}{n}+1})$, PLAL queries at most $\tilde{O}(\left(\frac{1}{\epsilon}\right)^{\frac{d^2}{n(n+d)}+1})$ labels and this is strictly less than the lower bound $\Omega(\left(\frac{1}{\epsilon}\right)^{\frac{d-1}{n}+1})$ for passive learning from Theorem \ref{t:NN_upper_lower}, provided that $d,n\geq 2$. Thus, using PLAL provably reduces the label complexity (as a function of $\left(\frac{1}{\epsilon}\right)$.)

We now prove success of the modified $1$-NN algorithm for the sample size stated above. We need the following result that also appears in \cite{SSLSupplement}:
\begin{lemma}\label{ShaisLemma}
  Let $C_1, C_2, \ldots , C_r$ be a sequence of subsets of some domain set $\Xcal$ and let $S$ be a set of points of size $m$, sampled \iid according to some distribution $P$ over $\Xcal$. Then we have
 \[
  \ex_{S\sim P^m}\left[\sum_{i:C_i\cap S=\emptyset}P[C_i]\right] \leq \frac{r}{m \e}
 \]
\end{lemma}

Let $\lambda=\sqrt{d}/2^k$ for the smallest $k$ such that $\sqrt{d}/2^k\leq \phi^{-1}(\epsilon)$. This implies $\phi^{-1}(\epsilon)\geq\lambda\geq \phi^{-1}(\epsilon)/2$.
We can cover $\Xcal=[0,1]^d$ with $r=\left(\sqrt{d}/\lambda \right)^d$ boxes $C_1, C_2,\ldots C_r$ of side-length $\lambda/\sqrt{d}=1/2^k$.
Note that any two points inside such a box are at distance at most $\lambda$.

Using Markov's inequality, Lemma \ref{ShaisLemma}
% \ref{l:bound_not_hit}
 implies that for any $\epsilon >0$ and $m$ we have
\[\Prob_{S \sim P^m} \left[\left[\sum_{i:C_i\cap S=\emptyset}P[C_i]\right] > \epsilon \right] \leq \frac{r}{\epsilon m \e}\]

It follows that in this setting, for any $\epsilon, \delta >0$, a sample of size
\[
m 
% ~>~ \left( \frac{2\sqrt{d}}{\epsilon^{1/n}} \right)^d\frac{1}{\epsilon \delta \e} 
% ~=~ \left( \frac{2\sqrt{d}}{\phi^{-1}(\epsilon)} \right)^d\frac{1}{\epsilon \delta \e} 
~\geq~ \left( \frac{\sqrt{d}}{\lambda} \right)^d\frac{1}{\epsilon \delta \e} 
~=~ \frac{r}{\epsilon \delta \e}\]
suffices to guarantee that with probability exceding $(1-\delta)$, at most an $\epsilon$-fraction of domain points are in boxes that are not hit by the sample. By noting that $\phi^{-1}(\epsilon) = \epsilon^{1/n}$ (for the polynomial Lipschitzness function $\phi(\lambda)=\lambda^n$) and recalling that $\lambda\geq \phi^{-1}(\epsilon)/2$, we obtain that
\[
m 
~>~ \left( \frac{2\sqrt{d}}{\epsilon^{1/n}} \right)^d\frac{1}{\epsilon \delta \e} 
~=~ \left( \frac{2\sqrt{d}}{\phi^{-1}(\epsilon)} \right)^d\frac{1}{\epsilon \delta \e} 
~\geq~ \left( \frac{\sqrt{d}}{\lambda} \right)^d\frac{1}{\epsilon \delta \e} 
~=~ \frac{r}{\epsilon \delta \e}
\]
suffices for this, which is the sample size stated above.


Now consider the modified $1$-NN labeling rule, where every point $x$ gets the label of its Nearest Neighbor within the cell that a run of PLAL produced on a samle $S_u$. We denote that sample $S_u$ with the labels from PLAL by $S$.  We refer to the elements of the partition that PLAL produced as \emph{cells} and to the elements of the partition in the argument above as \emph{boxes}. All these elements are axis-alligned rectangles that have powers of $1/2$ as sidelengths.
For a point $x$, we denote the box that contains $x$ by $b(x)$ and the cell that contains $x$ by $c(x)$. As the sidelengths of both boxes and cells are powers of $1/2$, we have $b(x)\subset c(x)$ or $c(x)\subset b(x)$ or $b(x)=c(x)$ for all $x$.

We bound the probability that a test point $x$ receives the wrong label:

{\bf Case 1:} $c(x)$ was declared homogeneous by PLAL.\\
Then $x$ will receive the label of $c(x)$. By Observation \ref{o:cells_homo}, this results in error at most $\epsilon$.

{\bf Case 2:}  $c(x)$ was not declared homogeneous by PLAL and $b(x)\subseteq c(x)$.\\
% Then the Nearest Neighbor inside the cell will also be inside the box that $x$ lies (as we chose a power of $1/2$ for the side-length of the boxes), unless the box that $x$ lies in was not hit by the sample $S$. 
We chose the sample size $S$ so that (with probability at least $1-\delta$) at most an $\epsilon$-fraction of points lie in boxes that are not hit by $S$, thus the probability that $S\cap b(x)=\emptyset$ is bounded by $\epsilon$. If $S\cap b(x)\neq \emptyset$, then the Nearest Neighbor of $x$ inside $c(x)$ has distance at most $\lambda$ from $x$ (recall that the diameter of $b(x)$ is $\lambda$). As $\phi^{-1}(\epsilon)\geq\lambda$, at most an $\epsilon$ fraction of points $x$ are at distance less than $\lambda$ from some point of oppostite label. Thus, the error of our labeling rule in this case is at most $2\epsilon$.

{\bf Case 3:}  $c(x)$ was not declared homogeneous by PLAL, $c(x)\subset b(x)$ and $c(x)\cap S \neq \emptyset$.\\
We can bound the probability that $x$ receives a wrong label by $2\epsilon$ in the same way as in Case 2. (The probability that $b(x)\cap S = \emptyset$ is bounded by $\epsilon$ and otherwise $x$ receives the label of a point that is at distance at most $\lambda$.)

{\bf Case 4:}  $c(x)$ was not declared homogeneous by PLAL, $c(x)\subset b(x)$ and $c(x)\cap S = \emptyset$.\\
In this case $x$ receives the label of its Nearest Neighbor in the parent cell of $c(x)$. We denote this cell by $p(c(x))$. The cell $c(x)$ was produced when PLAL decided to split $p(c(x))$. Thus the parent cell $p(c(x))$ contains points from $S$. Note that $c(x)\subset b(x)$ implies $p(c(x))\subseteq b(x)$. This implies that the Nearest Neighbor of $x$ in $p(c(x))$ is at distance at most $\lambda$ from $x$ and as under Case 2 we bound the probability that this neighbor has a different label than $x$ by $\epsilon$.


