from pylab import *
from numpy import *

def gen_generalized_clusters(c):
    
    granularity = 1000
    xaxis = arange(granularity)*1.0/granularity
    y = zeros(granularity)
    
    for i in range(1,granularity/4): 
        x = i/float(granularity)
        y[i] = c*exp(-1.0/float(x))
    for i in range(granularity/4, 3*granularity/4): 
        x = i/float(granularity)
        if(x == 0.5): x = 0.50000000001
        y[i] = c*exp(-1.0/abs(0.5-x))
    for i in range(3*granularity/4, granularity): 
        x = i/float(granularity)
        y[i] = c*exp(-1.0/abs(1-x))
    
    #print xaxis
    #print y
    plot(xaxis,y)
    show()

if __name__ == '__main__':
    
    c = 1.0/(2.0*exp(-0.25))
    c = 3    
    gen_generalized_clusters(c)