%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beamer Presentation
% LaTeX Template
\documentclass[11pt]{beamer}

%% General document %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{bm}
\usepackage{fancybox}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For algorithms
 \usepackage{algorithm}
 \usepackage{algorithmic}

%our stuff
\usepackage{notation}

\mode<presentation> {
\setbeamerfont{structure}{}
\usetheme{default}
\usecolortheme{beaver}



\newenvironment<>{varblock}[2][\textwidth]{%
  \setlength{\textwidth}{#1}
  \begin{actionenv}#3%
    \def\insertblocktitle{#2}%
    \par%
    \mode<presentation>{%
        \setbeamercolor{block title}{bg=lightgray, fg=darkred}
       \setbeamercolor{block body}{bg=lightgray,fg=black}
     }%
    \usebeamertemplate{block begin}}
  {\par%
    \usebeamertemplate{block end}%
  \end{actionenv}}
\addtobeamertemplate{varblock begin}{}{\setlength{\parskip}{35pt plus 1pt minus 1pt}}

\setbeamercolor{block title}{bg=lightgray, fg=red}%bg=background, fg= foreground
\setbeamercolor{block body}{bg=lightgray,fg=black}%bg=background, fg= foreground

%\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
\setbeamertemplate{footline}[page number] % To replace the footer line in all slides with a simple slide count uncomment this line

\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}

\usepackage{graphicx}
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables

\newcommand\fonttext{\fontsize{10}{8}\selectfont}
\newcommand\fontdef{\fontsize{9}{7.2}\selectfont}
\newcommand\highlight[1]{\begin{Large} \textbf{\color{darkred}#1}\end{Large}}

%set the spacing between items
% \newlength{\wideitemsep}
% \setlength{\wideitemsep}{\itemsep}
% \addtolength{\wideitemsep}{5pt}
% \let\olditem\item
% \renewcommand{\item}{\setlength{\itemsep}{\wideitemsep}\olditem}

\newenvironment{wideitemize}{
\begin{itemize}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{10pt}
  \setlength{\parsep}{1pt}
}{\end{itemize}}
  
  
\def\pgftikzinstalled{1} % comment this if you don't have pgf installed
\ifdefined \pgftikzinstalled
    \usepackage{tikz}
    \usetikzlibrary{external}
    \usepackage{pgfplots}
    \usepackage{pgfplotstable}
    \pgfplotsset{compat=newest}
\else
    \usepackage{tikzexternal}
\fi
\tikzexternalize
\tikzsetexternalprefix{include/externalized/}
  
%\logo{\includegraphics[width=1.5cm]{billeder/logo}}
\title{PLAL: Cluster based active learning}
\author{Sharon Wulff \\\vspace{0.5cm} Joint work with \\ Ruth Urner \& Shai Ben David \\\vspace{0.75cm} * Inspired by our conversations with Andreas Krause}
\institute{Machine learning seminar}
\date{April 8 2013}
\begin{document}


\begin{frame}
\titlepage 
\end{frame}

\begin{frame}
\frametitle{Overview}
\tableofcontents 
\end{frame}


%------------------------------------------------
\section{Background and related work} 
%------------------------------------------------

\begin{frame}
\frametitle{Active learning in a nut shell}
\fonttext

\begin{wideitemize}
 \item Typical scenario: Unlabeled data easily assembled, labels are costly
 \item Active learning (AL): Choose which labels to obtain based on the instances and (perhaps) previously seen labels
 \item Goal: Good prediction performance while minimizing number of labeled instances
 \item Evaluation: Comparison to fully supervised settings %with lower sample complexity
\end{wideitemize}

\pause
\begin{center}
\begin{varblock}{(Pool based) AL settings}
\begin{enumerate} 
 \item Receive unlabeled sample generated \iid %by some probability distribution.
 \item At each step choose one instance and query its label
 \item Output a hypothesis
\end{enumerate}
\end{varblock} 
\end{center}


\end{frame}

\begin{frame}
\frametitle{Notation and definitions}
\fonttext

\begin{wideitemize}
 \item $\Xcal$ some domain set, $\Ycal$ label set (for concreteness, $\Xcal = [0,1]^d$, $\Ycal = [0,1]$).
 \item Hypothesis $h:\Xcal\to \Ycal$, hypothesis class $H$
 \item $P$ over $\Xcal\times \Ycal$ the data generating distribution, sample, $S=((x_1,y_1),\dots,(x_n,y_n))$ generated \iid from $P$
 \item For given $h$, the error of $h$ w.r.t $P$: $$\err_P(h)=\Pr_{(x,y)\sim P}(y\neq h(x))$$
 \item For given $H$: $\err_P(H):=\inf_{h\in H}\err_P(h)$ %the smallest error of a hypothesis $h\in H$ with respect to $P$
 \item Learner $\Acal$ gets $S$ %=((x_1,y_1),\dots,(x_n,y_n))$ 
 and outputs a hypothesis $h$. The empirical error of $h$\footnote{We assume deterministic labeling, if $l(x)=P(y=1|x)$ then $l(x) \in \Ycal$}: 
 $$\err_S(h):=|\{(x,y)\in S ~:~ y\neq h(x)\}|/|S|$$
\end{wideitemize}

%%%%%%%%%%%%%%%%
% comment on the realizable case which is when there exists h \in H s.t. \err_P(h)= 0 (and therefore also its error on the sample)
%
%%%%%%%%%%%%%%%%
\end{frame}

\begin{frame}
\frametitle{Notation and definitions}
\fonttext

\begin{columns}
\column{0.42\textwidth}
\highlight{General learning}\\ \vspace{0.5cm}
Learning algorithm $\Acal$

\column{0.42\textwidth} 
\highlight{Active learning}\\ \vspace{0.5cm}
Active learning algorithm $\Acal$
\end{columns}

\begin{center}
$(\epsilon, \delta)$-learns class $H$ with respect to set of distribution $\Qcal$, if there exists
\end{center}

\begin{columns}
\column{0.5\textwidth}
\begin{center}
function $m: (0,1) \times (0,1) \to \naturals$ \\
for all $\epsilon, \delta \in (0,1)$, 
for all $P\in \Qcal$, \\
s.t. given \iid sample $|S| \geq m(\epsilon,\delta)$ 
\end{center}

\column{0.5\textwidth} 
\begin{center}
$m_l: (0,1) \times (0,1) \to \naturals$, $m_u: (0,1) \times (0,1) \to \naturals$ \\
for all $\epsilon, \delta \in (0,1)$, 
for all $P\in \Qcal$, \\
s.t. given \iid unlabeled sample $|S| \geq m_u(\epsilon, \delta)$, 
$\Acal$ queries $\leq m_l(\epsilon, \delta)$ of $S$ and  
\end{center}

\end{columns}

\begin{center} 
with probability $\geq 1-\delta$ $\Acal$ outputs $h$ s.t $\err_P(h) \leq \err_P(H)+\epsilon$.
\end{center}

\begin{columns}
\column{0.5\textwidth}
\begin{center}
Sample complexity: smallest $m$ satisfying the above
\end{center}

\column{0.5\textwidth} 
\begin{center}
Sample complexity: Given $m_u$, the smallest $m_l$ such that $(m_u, m_l)$ satisfy the above
\end{center}

\end{columns}

\end{frame}

\begin{frame}
\frametitle{Active learning unrelated work}

``Efficiently searching through a hypothesis space'' paradigm:\\
 \ \\
 \fbox{%
\begin{Bflushleft}[b]
Let $S \subset \Xcal$ be the unlabeled data\\
Pick few points from $S$ and get their label\\
 \textbf{Repeat}:\\
\hspace{3mm} Fit $h \in H$ to the labels seen so far \\
\hspace{3mm} Query $\xv \in S$ s.t. $\xv$ is: closest to the boundary/\\
\hspace{10mm} most uncertain/likely to decrease overall cost..
\end{Bflushleft}}\\
\ \\
\pause
\vspace{0.2cm}
Not very related AL research:
\begin{itemize}
 \item Theoretical analysis for the realizable case
 \item AL in more applied settings 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Active learning - Sampling bias}
Intuitively:
\begin{itemize}
 \item Initially $S$ is a good representation of $P$
 \item In the greedy process points are sampled based on their informativeness assessments $\rightarrow$ $S$ is less and less a representative sample
 \item Classifier trained on $S$ but tested on $P$
\end{itemize}
\vspace{0.5cm}
Example from \cite{p1}: 
\begin{center}
\includegraphics[width=0.9\linewidth]{include/samplig_bias_dh.jpg} 
\end{center}
Where $h_w(\xv) =  \begin{cases} +1, & \mbox{if } \xv \geq w \\ -1, & \mbox{if } \xv < w \end{cases}$
%%%%%%%%%%%%%%%%
% Many of the ``strong'' AL schemes suffer from this. This is why the online settings is easier to analyze (does not suffer from sampling bias) 
%
%%%%%%%%%%%%%%%%
\end{frame}

\begin{frame}
\frametitle{Active learning related work}
We focus on the agnostic settings\\
\vspace{0.25cm}
\begin{wideitemize}
 \item Lower bounds of $\Omega(1/\epsilon^2)$ by  K{\"a}{\"a}ri{\"a}inen (2006) and Beygelzimer et al. (2009) 
 $\rightarrow$ improvements in label complexity for learning a hypothesis class are not possible in general
\end{wideitemize}
 % $\rightarrow$ Need to identify parameters that characterize 
% learning tasks where active learning is beneficial
\pause
\begin{wideitemize}
 \item The disagreement coefficient by Hanneke (2007) was used to analyze recent works (mostly online AL). 
 Bounds depend on the class approximation error. %become irrelevant when it is large.. 
 \item First cluster based AL by Dasgupta and Hsu (2008). The bound is given in terms of the depth of the 
 effective tree. %Our work builds on ideas from this paper, the algorithm is very similar but the analysis is different and relies on a more natural parameter
\end{wideitemize}
% \begin{enumerate}
%  \item Online AL with proven reduction in label complexity in terms of the disagreement coefficient
%  \item Exploiting cluster structure: Imagine cluster the data and then query 1 label per cluster
%  \begin{enumerate}
%   \item DH obviously (discuss again later)
%   \item Zhu Lafferty and Gharamani exploit neighborhood propagation in a way that still suffers from the sampling bias
%  \end{enumerate}
\end{frame}



\begin{frame}
\frametitle{Probabilistic Lipschitzness (PL)\footnote{Introduced in \cite{p2}}} %for analyzing the merits of unlabeled data in the context of SSL}}
\begin{definition}[] 
\label{def:prob_lip}
\begin{small}
Let $\phi : \reals \to [0,1]$. We say that $f : \Xcal \to \reals$ is $\phi$-Lipschitz w.r.t. a distribution 
$P_\Xcal$ over $\Xcal$ if, for all $\lambda > 0$:
 \[
%  \Pr_{x \sim D} [ \exists y :  |f(x)-f(y)| > \lambda\,\|x-y\| ] \le \phi(\lambda)
   \Pr_{x \sim P_\Xcal}~ \left[~ \Pr_{y\sim P_\Xcal}~ \left[~  |f(x)-f(y)| ~>~ 1/\lambda\,\|x-y\| ~\right] ~>~ 0 ~\right] ~\le~ \phi(\lambda)
 \]
 If $P=(P_\Xcal, l)$, $l$ is $\phi$-Lipschitz, then $P$ satisfies the $\phi$-Probabilistic Lipschitzness. 
\end{small}
\end{definition}

\begin{itemize}
  \item PL formalizes the clusterability of the data assumption
  \item Intuitively: if $P=(P_\Xcal, l)$ is $\phi$-Lipschitz $\rightarrow$ weight of $\xv$ with positive mass of points of 
  opposite label in an $\lambda$-ball around them, is bounded by $\phi(\lambda)$.
%This definition generalizes the standard definition of Lipschitzness. Note that for points $x$ and $y$ at distance smaller than $\lambda$ with opposite labels, 
%the standard Lipschitz condition for Lipschitz constant $1/\lambda$ is violated as $|l(x)-l(y)| = 1 > 1/\lambda \|x-y\|$. Thus, if the labeling function $l$ of 
%a distribution is $L$-Lipschitz (on the support of the distribution) then it satisfies Probabilistic Lipschitzness with the function $\phi(\lambda)=1$ if $\lambda\geq 1/L$ and $\phi(\lambda)=0$ if $\lambda < 1/L$.  
 \end{itemize}

% disadvantages of the definition with exists, is that it doesn't capture well the case of well seperated gaussians with 
% little noise (everything around the noise contributes to the mass of points violating the l-ness). But i don't think it 
% affects much the algorithm, just the analysis is not tight in this case. 
\end{frame}

\begin{frame}
\frametitle{PL examples}
\begin{itemize}
 \item  Let $P_\Xcal$ be the uniform distribution over $\Xcal = [0,1]^d$.\\
\end{itemize}
\vspace{0.25cm}
\begin{columns}
  \begin{column}{0.49\textwidth}
    $l$ is a linear separator $\rightarrow$ $\phi(\lambda) = C \lambda$ for some constant $C$.\\
  \end{column}
  \begin{column}{0.5\textwidth}
     $l$ assigns a discrete set of points a different label $\rightarrow$ $\phi(\lambda) \sim \lambda^d$ \\ ($<$ some threshold).\\
  \end{column}
\end{columns}
\vspace{0.25cm}
\begin{columns}
  \begin{column}{0.45\textwidth}
    \begin{center}
    \includegraphics[width=0.7\linewidth]{include/pl_linear.pdf} 
    \end{center}
  \end{column}

  \begin{column}{0.45\textwidth}
    \begin{center}
    \includegraphics[width=0.7\linewidth]{include/pl_discrete.pdf} 
    \end{center}
  \end{column}
\end{columns}

\end{frame}


%TODO: ask Shai and Ruth about it. If we set \lambda = 1/2 we get that the probability is smaller than exp(-1/2)
\begin{frame}
\frametitle{PL example (non-uniform distribution)}
\begin{itemize}
 \item  Let the domain be the unit interval $\Xcal = [0,1]$ and 
 $l(x) =  \begin{cases} +1, & \mbox{if } x \geq \frac{1}{2} \\ -1, & \mbox{if } x < \frac{1}{2} \end{cases}$.
\end{itemize}
\vspace{0.5cm}
Define the density as follows:\\
$P_\Xcal(x) =  \begin{cases} 
c\e^{-1/x}, & \mbox{for } 0\leq x \leq \frac{1}{4} \\ 
c\e^{-1/|1/2-x|}, & \mbox{for } \frac{1}{4}\leq x \leq \frac{3}{4} \\
=c\e^{-1/|1-x|}, & \mbox{for } \frac{3}{4}\leq x \leq 1 \\
\end{cases}$
with $c=(2\e^{-\frac{1}{4}})^{-1}$. \\
\vspace{0.5cm}
\pause
$\rightarrow$ 
$P_\Xcal(x)$ is not $\lambda$-Lipschitz for any constant $\lambda$ %since there exist arbitrarily close points with opposing labels
but
 \begin{eqnarray*}
\phi(\lambda) & ~\leq~ & \Pr_{x\sim D} \big[ \exists y \big|~ f(x)\neq f(y) \& ||x-y|| \le \lambda \big]\\
 &  ~\leq~ & \int_{1/2-\lambda}^{1/2+\lambda} c\e^{-1/|1/2-x|}dx \leq  \e^{-1/\lambda} 
\end{eqnarray*}

\end{frame}

%------------------------------------------------
\section{The algorithm} 
%------------------------------------------------

\begin{frame}
\frametitle{DH  algorithm ~\cite{p3}}
%TODO: change the algorithm 
\fbox{%
\begin{Bflushleft}[b]
Given a hierarchical clustering of $S$, $\epsilon$, $\delta$\\
Start with root cluster \\
 \textbf{Until the budget runs out..}:\\
\hspace{0.5cm} Select sub-tree $c$, and sample \iid sample from it \\
\hspace{0.5cm} Update the error bounds w.r.t the labels\\ 
Determine leaf labels by their sub-tree majority\\
\end{Bflushleft}}
\vspace{0.3cm}
\\ Still unspecified:
\begin{itemize}
\item Generation of the tree
 \item The select procedure
\end{itemize}
% Hierarchical sampling for active learning
% The idea is to use a hierarchical clustering (cluster tree) of the unlabeled data, 
% check the clusters for label homogeneity by starting at the root of the tree (the whole data-set) 
% and working towards the leafs (single data points). The label homogeneity of a cluster is estimated 
% by choosing data points for label queries uniformly at random from the cluster.  If a cluster can be
% considered label homogeneous with sufficiently high confidence, all remaining unlabeled points in the
% cluster are labeled with the majority label and no further points from this cluster will be queried.
% If a cluster is detected to be label heterogeneous, it is split into its children in the cluster tree. 
% Since the cluster tree is fixed before any labels were seen, the algorithm can reuse labels from the 
% parent cluster (the induced subsample can be considered a sample that was chosen uniformly at random 
% from the points in the child-cluster) without 
% introducing 
% any sampling bias. 
% A nice overview on this can be found in \cite{Dasgupta11}.
\vspace{0.25cm}
* Avoiding sampling bias by determining the cluster split independent of the labels
\end{frame}

\begin{frame}
\frametitle{DH  algorithm ~\cite{p3}}
\begin{center}
 \includegraphics[width=0.9\linewidth]{include/dh_algorithm.jpg}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Spatial trees}

\begin{wideitemize}
 \item Spatial tree is a binary tree where each node consists of a subset of the space $\Xcal = [0,1]^d$.
 \item At level $k$ (distance from the root) the nodes (cells) form a $2^k$ partition of the space.
 \item The \emph{data diameter} is the spread of the points in a cell ($\diam(C,S) = \max_{\xv,\xv' \in C\cap S} \|\xv-\xv'\|$). 
 Spatial trees allows us to link between the data diameter and the cell diameter.
 \end{wideitemize}

 \end{frame}

\begin{frame}
\frametitle{PLAL algorithm}

\begin{algorithmic}
\begin{small}

   \STATE {\bfseries Input:} $S_u=(\xv_1,\ldots, \xv_m)$, spatial tree $T$, $\epsilon, \delta$\\
%   \STATE       \hspace{1.2cm} query numbers $q_1, q_2, q_3 \ldots$
%   \STATE $C_1 = [0,1]^d$
   \STATE $k = 0$
   \STATE $active\_cells[0].append(\rt(T))$
   \WHILE{$active\_cells[k]$ not empty}
   \STATE $q_{k}= \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$
      \FOR {{\bf all} $C$ {\bf in} $active\_cells[k]$}
	  \STATE $C.query(q_{k})$
	  \IF{all labels seen in $C$ are the same}
	  \STATE label $C\cap S$ accordingly \sl (cell $C$ now becomes inactive)
	  \ELSE
	    \IF{there are not-queried points in $C\cap S$}
	      %\STATE split $C$ into $C_1,\ldots,C_{2^{d}}$
	      \STATE $active\_cells[k + 1].append(\rc(C), \lc(C))$
	      %\STATE \hspace{4cm} $\ldots,C_{2^{d}})$ 
	    \ENDIF
	  \ENDIF
      \ENDFOR
      \STATE $k = k+1$
   \ENDWHILE   
   \STATE {\bfseries Return:} labeled sample $S=((x_1,y_1) \ldots, (x_m, y_m))$
  \end{small}
\end{algorithmic}
%%%%%%%%%%%%%%%%%%%%%%%%
% If the cell contains less than q_k points then we query all of them
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{frame}

%------------------------------------------------
\section{Error bound} 
%------------------------------------------------

\begin{frame}
\frametitle{Error bound}
\begin{theorem}\label{thm_error_on_sample}
  Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, and $l:\Xcal\to \{0,1\}$ a labeling function. 
  Then, when given an \iid unlabeled $P_\Xcal$-sample $S_u$ of size $m$ and parameters $\epsilon$ and $\delta$, with probability 
  at least $(1-\delta)$ (over the choice of the sample $S_u$), PLAL  labels at least $(1-\epsilon)m$ many points from $S_u$ correctly.
\end{theorem}

\end{frame}


\begin{frame}
\frametitle{Error bound}
Proof idea:
\begin{wideitemize}
 \item Show that for (declared) homogeneous cells at most $\epsilon$ of the points are different
 \pause
 \item For any cell $C$, if $\min \{\Pr[l=1|C], \Pr[l=0|C]\} \geq \epsilon$ $\rightarrow$ $S$ of size $\frac{\ln(2/\delta_C)}{\epsilon}$ has probability $< \delta_C$ of 
 being label homogeneous.
 \item Setting $\delta_C = \delta/2^{2k-1}$,  $\rightarrow$ sum over all $\delta_C$ is at most $\delta$ 
 \item Finally $\frac{\ln(2/\delta_C)}{\epsilon}=\frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$
\end{wideitemize}
%%%%%%%%%%%%%%%%%%
% $k$ is the level of the cell $C$,
%
%%%%%%%%%%%%%%%%%%%
\end{frame}

%------------------------------------------------
\section{Number of queries bound} 
%------------------------------------------------

\begin{frame}
\frametitle{General bound on the number of queries}
Consider a fixed spatial tree $T$, and sample $S$
\begin{varblock}{Notation}
\begin{itemize}
\item $\lambda_k^S$ - maximum data-diameter in a cell at level $k$
%  \ie 
%  $
%   \lambda_k^S = \max \{\diam(C, S): C\ \text{cell at level}~ k\}
%  $
\item $\lambda_k := \lambda_k^\Xcal$ - maximum diameter of a cell at level $k$
\end{itemize}
* The cell diameter is an upper bound on its data-diameter.
\end{varblock}
\pause
\begin{theorem}\label{t:query_bound}
  Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, $l:\Xcal\to \{0,1\}$ a 
  labeling function that is $\phi$-Lipschitz for some function $\phi$, and let $q_i=\frac{i \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ 
  denote the query numbers of PLAL for level $i$. 
  Then the expected number of queries that PLAL makes on an unlabeled \iid sample $S$ from $P_\Xcal$ of size $m$ is bounded by
  $$
   \min_{k\in\naturals}~ (q_{k}2^{k} + \phi(\lambda_k^S)m).
  $$
\end{theorem}
\end{frame}


\begin{frame}
\frametitle{General bound on the number of queries}
Proof idea: \\
Recall that PLAL either splits heterogeneous cells, or queries all it's points.\\
\vspace{0.25cm}
\pause
For any $k$:
\begin{wideitemize}
 \item $\phi(\lambda_k^S)m)$ - bound the expected number of points that lie in heterogeneous cells.
 \item $q_{k}$ - bound on the number of labels queried in each cell in the partition (re-use is allowed!)
 \item $2^k$ - bound on the number of cells
\end{wideitemize}
\end{frame}


\begin{frame}
\frametitle{General bound on the number of queries}
Let $k^*$ be such that $\phi(\lambda_{k^*}^S)\cdot m \leq q_{k^*} \cdot 2^{k^*}$\\
\vspace{0.25cm}
\begin{corollary}\label{c:query_bound}
%   Let $\Xcal=[0,1]^d$ be the domain, $P_\Xcal$ a distribution over $\Xcal$, $l:\Xcal\to \{0,1\}$ a labeling 
%   function that is $\phi$-Lipschitz for some function $\phi$, and $q_k = \frac{k \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon}$ 
%   the sequence of query numbers. Let $k^*$ be such that $\phi(\lambda_{k^*}^S)\cdot m \leq q_{k^*} \cdot 2^{k^*}$. 
  Then the expected number of queries that PLAL makes on an unlabeled \iid sample from $P_\Xcal$ of size $m$ is bounded by
  $$
  \frac{k^* \cdot 2 \cdot \ln(2) + \ln(1/\delta)}{\epsilon} \cdot  2^{k^*+1}
  %   2^{k^*d}\cdot \frac{3(\ln(2/\delta)+k^*d\ln(2))}{\epsilon}
  $$
\end{corollary}
\end{frame}

\begin{frame}
\frametitle{Dyadic (spatial) trees}
For $\Xcal=[0,1]^d$ 
\begin{itemize}
 \item In \emph{dyadic trees} cells are partitioned by halving one of the coordinates, 
 cycling through the dimensions. 
 \item For any $k$, the root cell $[0,1]^d$ is split into $2^{kd}$ cells of side-length $1/2^k$ at level $k\cdot d$. 
 \item The diameter at level $kd$ is $\lambda_{kd} = \sqrt{d}/2^k$
\end{itemize}
\begin{center}
\includegraphics[width=0.6\linewidth]{include/dyadic_tree.jpg} 
\end{center}

\end{frame}

\begin{frame}
\frametitle{Bound for dyadic trees}
\begin{itemize}
 \item Polynomial and exponential Lipschitz assumption
 \item Calculate $k^*$ s.t. $\phi(\lambda_{k^*})\cdot m \leq q_{k^*} \cdot 2^{k^*}$ ($\lambda_k = \sqrt{d}/2^k$)
 \item Use the bound in previous Corollary
\end{itemize}

\begin{table}[h]
\centering
\begin{tabular}{|r|l|}
\hline
Lipschitzness & Bound on expected number of queries\\
\hline
$\phi(\lambda) = \lambda^n$ & $2\cdot\frac{\log({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}} \ln(2) + \ln(1/\delta)}{\epsilon} \cdot({\sqrt{d}^n}m \epsilon)^{\frac{d}{n+d}}$ \\

& $= \tilde{O}(m^{\frac{d}{n+d}} \left(\frac{1}{\epsilon}\right)^{\frac{n}{n+d}})$\\[12pt]
$\phi(\lambda) = \e^{\frac{-1}{\lambda}}$ & $\frac{\sqrt{d}^d\log(\epsilon m)^d}{\epsilon} 2(\log(\log((\epsilon m)^{\sqrt{d}}))d\ln(2) +\ln(2/\delta))$ \\
& $=\tilde{O}(\frac{1}{\epsilon})$\\[3pt]
\hline 
\end{tabular}
\end{table}
 * The $\tilde{O}$-notation hides $\log$-factors and constants
\end{frame}

\begin{frame}
\frametitle{Is it any good?}
For an algorithm with sample complexity $m=\Omega((1/\epsilon)^\alpha)$, if $\alpha >1$:
\begin{wideitemize}
 \item Exponential Lipschitzness $\phi(\lambda) = \e^{-\frac{1}{\lambda}}$, our bound is $1/\epsilon$ 
 times a factor which is logarithmic in $m$ 
 \item Polynomial Lipschitzness $\phi(\lambda) = \lambda^n$, plugging $m$ our bound becomes 
 $\tilde{O}( \left(\frac{1}{\epsilon}\right)^{\frac{n + \alpha d}{n+d}}))$
\end{wideitemize}
\vspace{0.35cm}
Compare to:
\begin{enumerate}
 \item In the general case of all $\phi$-Lipschitz distributions, a finite vc-dim class 
 has $\Theta(1/\epsilon^2)$ complexity
 \item Restricted to the polynomial Lipschitzness, we get bound improvement of 
 $\tilde{O}((\frac{1}{\epsilon})^{1+\frac{d^2}{n(n+d)}})$ from $\Omega((\frac{1}{\epsilon})^{1+\frac{d-1}{n}})$
 %(note that $\frac{d^2}{n(n+d)}\leq \frac{d-1}{n}$ if $d,n\geq 2$)
\end{enumerate}

\end{frame}


\begin{frame}
\frametitle{Comparison to the DH algorithm}
\begin{enumerate}
 \item Complexity bound - PL. vs. \# nodes in a tree with all-label-homogeneous leafs vs. . 
 \begin{wideitemize}
 \item under which circumstances such trees exist?.
  \item The PL assumption seems more intuitive and likely to hold
  \item The PL is only used in the analysis, vs. a tree is an input
 \end{wideitemize}
\item Our work provides a lower bounds for passive learning under our assumptions
\item Analysis of the robustness of algorithms to an $\epsilon$ labeling error 

\end{enumerate}


\end{frame}

%------------------------------------------------
\section{Experiments} 
%------------------------------------------------

\begin{frame}
 \frametitle{Experiments with synthetic data}
 Mixture of Gaussian datasets with the following characteristics
 \begin{itemize}
  \item $~80\%$ of the points from 4 dense Gaussian, ``centered in the corners`` of the space
  \item $~20\%$ of the points from 4 sparse Gaussian centered at random points
  \item 8 classes - each Gaussian gets a different label
 \end{itemize}
 \vspace{0.3cm}
Vary the variance of the Gaussian $\rightarrow$ 3 different datasets  
\begin{description}
 \item [$A$] $.1$ dense variance and $1$ sparse variance
 \item [$B$] $.01$ dense variance and $.1$ sparse variance
 \item [$C$] $.001$ dense variance and $.1$ sparse variance
\end{description}
\vspace{0.3cm}
Intuition: \\
$C$ most cluster able - $A$ least cluster able
\end{frame}

\begin{frame}
 \frametitle{Prediction error vs. queries}

 \begin{varblock}{Experiment settings:}
We let $\epsilon$ range in $(0.01,0.05,0.1,0.15,0.2,0.25,0.3)$. For each $\epsilon$ we compute the PLAL queries and 
predictions, and compare with a $k$-NN prediction on a random sample of the same size (best $k$ in the range $(1,3,5,10)$).\\
For each dataset we generate 10 instantiation for each dimension $d = (5,15,25)$.\\  
 \end{varblock}

\vspace{0.3cm}
 \begin{wideitemize}
  \item Prediction using a $k$-NN rule on PLAL queries yields better accuracy than with the PLAL itself.
  \item The splitting of the cells is done on one dimension per round. The results seem rather invariant to the order 
 \end{wideitemize}
\end{frame}

\begin{frame}
 %\frametitle{Prediction error vs. queries}
 
\begin{figure}[htb]

    \centering
    \def\ymin{0}
    \def\experimentprefix{include/data/}

    \def\dataset{A}
    \def\ymax{80}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
   % \vspace{-0.4cm}
    
    \def\dataset{B}
    %\def\ymax{60}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    %\vspace{-0.4cm}
    
    \def\dataset{C}
    %\def\ymax{25}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_notnormalized.tikz.tex}
    \end{minipage}
    \caption{The average number of queries requested by PLAL on datasets $A$,$B$, and $C$ for different values of $\epsilon$
	      is denoted as $\%-queries$. The average prediction error of the nearest neighbor classifier with PLAL queries 
	      is denoted as \% NN-PLAL-error and the prediction error
	      of the nearest neighbor classifier with random queries is denoted as \% NN-random-error}    
    \label{fig:classification}
\end{figure}

\end{frame}

\begin{frame}
 \frametitle{Empirical PL}
\begin{varblock}{Settings:}
The empirical $\phi(\lambda)$ is calculated as the 
percentage of data points having at least 1 $\lambda$-close neighbor with a different label.\\
%We plot the empirical $\phi(\lambda)$ for $\lambda \in [0,10]$ 
\end{varblock}
\vspace{0.25cm}
\begin{figure}[htb]
    \centering
    \def\experimentprefix{include/data/}

    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{5}
        \def\showylabel{1}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{15}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
    \begin{minipage}[c]{0.3\textwidth}
        \def\dim{25}
        \def\showlegend{1}
        \input{include/plot_plcomparison.tikz.tex}
    \end{minipage}
%     \caption{The empirical $\phi(\lambda)$ as a function of $\lambda$ in the range $0-10$ for datasets 
%     $A$,$B$, and $C$ described in~\ref{sec:syndata}}    
%     \label{fig:emp_pl}
\end{figure}

 
\end{frame}

\begin{frame}
\frametitle{Future work}

Analysis side:
\begin{itemize}
\item Tighten the analysis of the complexity by counting active cells only
 \item Extend the algorithm's analysis to the PL without the exists statement
 \item Other parameters useful for AL? 
 %\item Compare the sample complexity of PLAL with DH (for specific PL, compute the exp. number of nodes)
\end{itemize}
\vspace{0.3cm}

Experiments:
\begin{itemize}
 \item Find a real dataset for which PLAL works better than random 
\end{itemize}


\end{frame}




%------------------------------------------------

\begin{frame}
\frametitle{References}
\footnotesize{
\begin{thebibliography}{1} % Beamer does not support BibTeX so references must be inserted manually as below

\bibitem[Dasgupta et al. 2008]{p3} Sanjoy Dasgupta and Daniel Hsu (2008)
\newblock Hierarchical sampling for active learning
\newblock \emph{ICML} 2008.

\bibitem[Urner et al., 2011]{p2} Ruth Urner and Shai Ben-David and Shai Shalev-Shwartz (2011)
\newblock Unlabeled data can Speed up Prediction Time.
\newblock \emph{ICML} 2011.

\bibitem[Dasgupta, 2011]{p1} Sanjoy Dasgupta (2011)
\newblock Two faces of active learning
\newblock \emph{Theor. Comput. Sci.} 412(19), 1767 -- 1781.


\end{thebibliography}
}
\end{frame}


\end{document} 