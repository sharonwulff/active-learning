# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 14:53:36 2012

@author: ruth
"""
from cluster import Cluster, MinSplittingCluster
from numpy import zeros, arange, array, ceil
from numpy.random import seed, randint
from Queue import PriorityQueue

from time import clock, time

EPSILON = 1e-8
SPLITTING_ALL = 0
SPLITTING_RANDOM = 1
SPLITTING_MAX = 2


def pl_algorithm(points, labels, epsilon, splitting = SPLITTING_ALL, zero_one_bounding_box = False, use_seed = None):
    
    dim, num_pts = points.shape
    labels = array(labels)
    
    #compute initial boundaries: list of d tuples
    initial_boundaries = None
    
    if zero_one_bounding_box: 
        initial_boundaries = [(0, 1) for d in dim]
    else:
        initial_boundaries = compute_data_boundaries(points)
    
    if(splitting == SPLITTING_ALL):
        initial_cluster = Cluster(initial_boundaries, points, labels, external_indices=arange(num_pts), use_seed = use_seed)
    else:
        initial_cluster = MinSplittingCluster(initial_boundaries, points, labels, splitting, external_indices=arange(num_pts))
        
    active_clusters = [initial_cluster]
    finished_clusters = []
    
    estimated_labels = zeros(num_pts)    
    num_points_queried = 0
    queried_indices = [] 
   
    #it seems that the get indices part takes the most and then the splitting
    while len(active_clusters) > 0:
        
        #print "active clusters %d queried points %d out of %d"%(len(active_clusters), num_points_queried, num_pts)
        curr_cluster = active_clusters.pop()
        #print "calling query"        
        
        #start_query = time()     
        queries_made = curr_cluster.query(epsilon)
        #elapsed_query = (time() - start_query)        
        #print "newly queried ", queries_made
        
        #start_should_split = time() 
        should_split = curr_cluster.should_split()     
        #elapsed_should_split = (time() - start_should_split)
        
        #print "timing: elapsed_query, elapsed_should_split ", elapsed_query, elapsed_should_split
        
        if(should_split):

            #start_split = time() 
            cluster_list = curr_cluster.split()
            #elapsed_split = (time() - start_split)
            #print "timing: elapsed_split ", elapsed_split
            #print "splitting cluster into ",len(cluster_list)
            for c in cluster_list: active_clusters.insert(0,c)
        else:
           
            #start_get_labels = time() 
           curr_labels = curr_cluster.get_labels()
           #elapsed_get_labels = (time() - start_get_labels)           
           
           #print "cluster is finished, used %d out of %d "%(curr_cluster.num_labels_used(), len(curr_cluster.points[1]))
           num_points_queried += curr_cluster.num_labels_used()
                      
           #internal indices
           curr_queried_indices = curr_cluster.label_pts_indicies
           #start_get_indices = time()
          
           
           #with this code:
           curr_external_indices = curr_cluster.get_external_indices()
           # set the labels found by the algorithm           
           estimated_labels[curr_external_indices] = curr_labels
           queried_indices.extend(array(curr_external_indices)[curr_queried_indices])
           
          
           #elapsed_get_indices = (time() - start_get_indices)
    
           #print "timing: elapsed_get_labels, elapsed_get_indices ", elapsed_get_labels, elapsed_get_indices
           finished_clusters.append(curr_cluster)
           #print "num finished clusters ", len(finished_clusters)
           
           
    queried_indices = list(set(queried_indices))
    assert(num_points_queried == len(queried_indices))
    
    return estimated_labels, num_points_queried, queried_indices


def pl_budget_algorithm(points, labels, epsilon, budget, zero_one_bounding_box = False, use_seed = None, use_all_budget = True):
    
    dim, num_pts = points.shape
    labels = array(labels)
       
    estimated_labels = zeros(num_pts)    
    num_points_queried = 0
    queried_indices = [] 
    active_clusters = None
    
    num_rounds = 0
    MAX_NUM_ROUNDS = 20
    
    while(num_points_queried < budget and num_rounds < MAX_NUM_ROUNDS):
        
        
        #compute initial boundaries: list of d tuples
        initial_boundaries = None
        
        if zero_one_bounding_box: initial_boundaries = [(0, 1) for d in dim]
        else: initial_boundaries = compute_data_boundaries(points)
        
        initial_cluster = MinSplittingCluster(initial_boundaries, points, labels, SPLITTING_MAX, external_indices=arange(num_pts), use_seed = use_seed)
        if(num_rounds > 0):
            initial_cluster = MinSplittingCluster(initial_boundaries, points, labels, SPLITTING_MAX, external_indices=arange(num_pts), use_seed = use_seed, labeled_indicies = queried_indices)
            
        active_clusters = PriorityQueue()
        active_clusters.put((-1*len(initial_cluster.labels) ,initial_cluster))
        finished_clusters = []
        
        max_queries_per_cluster = ceil(1.0/epsilon) + num_rounds
        print "round %d max queries for cluster is %d queried so far %d"%(num_rounds, max_queries_per_cluster, num_points_queried)
    

        while (num_points_queried < budget and active_clusters.qsize() > 0):
            
            #print "%d active clusters "%(active_clusters.qsize())
            curr_cluster = active_clusters.get()[1]
            curr_clusters_num_queries = len(curr_cluster.label_pts_indicies)
            #print "current active cluster has %d points and %d queries"%(len(curr_cluster.labels), curr_clusters_num_queries)
            
            if(curr_clusters_num_queries < max_queries_per_cluster):
                
                cluster_still_needs = min((max_queries_per_cluster - curr_clusters_num_queries), (len(curr_cluster.labels) - curr_clusters_num_queries))
                #print "this cluster still needs %d pts "%cluster_still_needs
                num_new_queries_made, new_queries = curr_cluster.query_budget(min(cluster_still_needs, budget - num_points_queried))
                num_points_queried += num_new_queries_made
                
                if(num_new_queries_made > 0):
                    # append the new queries to the list of queries
                    curr_external_indices = curr_cluster.get_external_indices()
                    queried_indices.extend(array(curr_external_indices)[new_queries])
                            
                    #print "queried points %d out of %d"%(num_points_queried, budget)   
                    #print "new queried points: ", array(curr_external_indices)[new_queries]
                    #print "all points queried so far: ",queried_indices
            
            #check if the cluster has seen at least 1/epsilon points, otherwise we treat it as a none finished cluster
            if(len(curr_cluster.label_pts_indicies) == len(curr_cluster.labels) or len(curr_cluster.label_pts_indicies) >= max_queries_per_cluster):
                
                #print "cluster has enough queries to either split or be finished"
                should_split = curr_cluster.should_split()     
                if(should_split):
                    #print "splitting "
                    cluster_list = curr_cluster.split()
                    for c in cluster_list: 
                        active_clusters.put((-1*len(c.labels), c))
                    #print "after split %d active clusters "%(active_clusters.qsize())
                else:
                   
                   curr_labels = curr_cluster.get_labels()
                   #print "cluster is finished, used %d out of %d "%(curr_cluster.num_labels_used(), len(curr_cluster.labels))
                   
                   curr_external_indices = curr_cluster.get_external_indices()
                   # set the labels found by the algorithm           
                   estimated_labels[curr_external_indices] = curr_labels
                   
                   #print "timing: elapsed_get_labels, elapsed_get_indices ", elapsed_get_labels, elapsed_get_indices
                   finished_clusters.append(curr_cluster)
                   #print "num finished clusters ", len(finished_clusters)
            else:
                #print "putting back cluster, it has only %d pts queried"%len(curr_cluster.label_pts_indicies)
                active_clusters.put((-1*len(curr_cluster.labels), curr_cluster))
        
        num_rounds += 1
    
    assert(num_points_queried == len(queried_indices))
    assert(num_points_queried == budget or num_rounds == MAX_NUM_ROUNDS)
    
    #print "budget is finished, go over the active clusters"
    #go over the active clusters and get their points and labels
    while (active_clusters.qsize() > 0):
        
        curr_cluster = active_clusters.get()[1]
        
        curr_labels = curr_cluster.get_labels()
        curr_external_indices = curr_cluster.get_external_indices()
        # set the labels found by the algorithm           
        estimated_labels[curr_external_indices] = curr_labels
        #queried_indices.extend(array(curr_external_indices)[curr_queried_indices])
       
        #print "timing: elapsed_get_labels, elapsed_get_indices ", elapsed_get_labels, elapsed_get_indices
        finished_clusters.append(curr_cluster)
    
    
    if(len(queried_indices) < budget):
        print "no active clusters, but haven't used all of the budget yet.. only %d out of %d"%(len(queried_indices), budget)
        if(use_seed): seed(use_seed)
        while(len(queried_indices) < budget):
            idx = randint(num_pts)
            if(idx not in queried_indices): queried_indices.append(idx)
            
    #just to be on the safe side
    estimated_labels[queried_indices] = labels[queried_indices]
    return estimated_labels, len(queried_indices), queried_indices

def find_point_indices(points, point):
    
     dim, num_pts = points.shape
     ret = []
     for idx in range(num_pts):
         if((point == points[:,idx]).all()): ret.append(idx)
     return ret
            
def compute_data_boundaries(points):

    dim, num_pts = points.shape
#    print dim, num_pts
    boundaries=[(points[i,0], points[i,0]) for i in range(dim)]
#    print(boundaries)
    for k in range(num_pts):
#        print "in outer loop", k
        for j in range(dim):
#            print "in inner loop", j
            boundaries[j] = (min(boundaries[j][0], points[j,k]) - EPSILON, 
                             max(boundaries[j][1], points[j,k]) + EPSILON)
    return boundaries