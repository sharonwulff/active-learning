# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 17:12:14 2012

@author: Ruth and Sharon    
"""
from numpy.random import rand, seed, randint
from numpy import ones, ceil, arange, mean, std, argsort, array, argmax
from numpy.linalg import norm

SPLITTING_RANDOM = 1
SPLITTING_MAX = 2
MIN_BOUNDRY = 1e-14

def is_point_in_cluster(point, boundaries):

    assert(len(point)==len(boundaries))    

    for i, coord in enumerate(point):
        if coord < boundaries[i][0] or coord >= boundaries[i][1]:
            return False
    return True


class Cluster(object):
    """
    boundary is an array of tuples, each with a starting and ending coordinate
    external indices is the indices of the points and labels in some external 
    data structure. This is useful in case we want to keep track of the labeled 
    points in some external settings. E.g. if we want in the end to return the
    indices of the labels we queried
    """
    def __init__(self, boundaries, points, labels, labeled_indicies = None, external_indices = None, use_seed = None, parent_cluster = None):
        
        if(use_seed != None): seed(use_seed)
        self.use_seed = use_seed
        self.parent_cluster = parent_cluster
        self.boundaries = boundaries
        self.dim = len(self.boundaries)
        self.points = points
        self.labels = labels
        self.external_indices = external_indices
        self.label_pts_indicies = []
        self.unlabeled_pts_indicies = list(arange(len(labels)))
        if labeled_indicies != None: 
            self.label_pts_indicies.extend(labeled_indicies) 
            for i in self.label_pts_indicies: self.unlabeled_pts_indicies.remove(i)
        dim, self.num_points = points.shape
        assert(dim == self.dim)
        
        
    def __repr__(self):
        
        s = "boundaries:\n"
        s += str(self.boundaries)
        s += "\n points with labels: \n"
        for idx in range(self.num_points):
            s += str(self.points[:,idx]) 
            s += ("," + str(self.labels[idx]) + "\n")
        return s
        
    def _split_recursion(self, clusters, dim):
        
        if(dim == self.dim): 
            return clusters
            
        (start, end) = self.boundaries[dim]
        half_dist = abs(end-start)/2.0
        first_sec = (start, start + half_dist)
        second_sec = (start + half_dist, end)
        
        new_clusters = []
        if(dim == 0): new_clusters = [[first_sec],[second_sec]]
        else:
            for c in clusters:
                c1 = list(c)
                c2 = list(c)
                c1.append(first_sec)
                c2.append(second_sec)
                new_clusters.append(c1)
                new_clusters.append(c2)
        
        return self._split_recursion(new_clusters, dim+1)
        
    def split(self):
        
        #print "before ==>", self.boundaries
        boundaries_list = self._split_recursion(None, 0)
        cluster_list = []
        #creating a cluster
        for b_idx, b in enumerate(boundaries_list):
            
            curr_pts_indices = []
            curr_labeled_indicies = []
            curr_external_indicies = None
            if(self.external_indices != None): curr_external_indicies = []
                
            for pts_idx in range(self.num_points):
                
                if (is_point_in_cluster(self.points[:,pts_idx], b)):
                    #here we use the len of the new poits as an index, dont change the order of the lines!
                    if (pts_idx in self.label_pts_indicies): curr_labeled_indicies.append(len(curr_pts_indices))
                    curr_pts_indices.append(pts_idx)
                    #if we keep track of the external indices, pass on this information
                    if(curr_external_indicies != None): curr_external_indicies.append(self.external_indices[pts_idx])
                    
            #print "sub cluster %d has %d points"%(b_idx, len(curr_pts_indices)) 
            if len(curr_pts_indices) > 0:
                c = Cluster(b, self.points[:,curr_pts_indices], self.labels[curr_pts_indices], 
                            labeled_indicies = curr_labeled_indicies, external_indices=curr_external_indicies, use_seed=self.use_seed, parent_cluster=self) 
                cluster_list.append(c)
                
        return cluster_list
    
#    def is_homogenous(self):
#                
#        return True 
    def num_labels_used(self):
        
        return len(self.label_pts_indicies)
    
    def get_external_indices(self):
        
        return self.external_indices

    def query(self, epsilon):
        
        num_required_labels = ceil(1.0/epsilon)
        if len(self.label_pts_indicies) >= num_required_labels: return 0, []
        
        if(self.use_seed != None): seed(self.use_seed)
        new_queries_made = 0       
        new_queries = []
        
        while (len(self.label_pts_indicies) < num_required_labels and
            len(self.label_pts_indicies) < self.num_points):
                
            query_pt_idx = self.unlabeled_pts_indicies[randint(len(self.unlabeled_pts_indicies))]
            self.label_pts_indicies.append(query_pt_idx)
            self.unlabeled_pts_indicies.remove(query_pt_idx)
            new_queries_made += 1
            new_queries.append(query_pt_idx)
        
        return new_queries_made, new_queries
        
    def should_split(self):
        
        assert(len(self.label_pts_indicies)>0)
        
        #we queried all the points
        if len(self.label_pts_indicies) == self.num_points:
            return False       
        
        if (not self.can_split()):
            print "cant split further!"
            return False
        # label homo test
        (is_homo, label) = self._is_homo_label()
        return (not is_homo)
        
    def can_split(self):
        
        return (sum([abs(self.boundaries[i][0]-self.boundaries[i][1]) >= MIN_BOUNDRY \
        for i in range(len(self.boundaries))]) == len(self.boundaries))
        
    def get_labels(self):
        
        if(self.should_split()):
            print "Can't output labels! split further"
            return None
        
        #should be homogenoues or cant split further
        if(len(self.label_pts_indicies) < self.num_points):
            
            is_homo, label = self._is_homo_label()
            if(is_homo):  return label*ones(self.num_points)
            
            #it is not homo, this means we stopped because we cant split further
            print "query all of it!"
            self.label_pts_indicies = arange(self.num_points)
            
            
        #here we should have all labels of the original points 
        return self.labels
        
    def _is_homo_label(self):
        
        if(len(self.label_pts_indicies) == 0):
            return False, None
            
        label = self.labels[self.label_pts_indicies[0]]
        for idx in self.label_pts_indicies:
           if(label != self.labels[idx]): return False, None
        
        return True, label

    def homogeneity(self):
        
        if(len(self.label_pts_indicies) == 0): return 0, None
        all_labels = list(set(self.labels[self.label_pts_indicies]))
        fraction_pts_each_label = array([sum(self.labels[self.label_pts_indicies] == label) for label in all_labels])/float(len(self.label_pts_indicies))
        homogeneity = max(fraction_pts_each_label)
        return homogeneity, all_labels[argmax(fraction_pts_each_label)]

"""
In this version the cluster upon split, will computes the boundries list which 
is the result of splitting in each dimension, but will choose one of them to do 
the split. The child clusters, only 2 at each round, will inheret the list of boundries 
to chose from, minus the one which was used in their split-up. Once the list is exhausted, 
each cluster is computing the split into d again. 
"""     

class MinSplittingCluster(Cluster):
    """
    Child of class Cluster    
    """
    def __init__(self, boundaries, points, labels, splitting_strategy, labeled_indicies = None, external_indices = None, use_seed = None, parent_cluster = None):
        
        super(MinSplittingCluster, self).__init__(boundaries, points, labels, labeled_indicies = labeled_indicies, external_indices = external_indices, use_seed=use_seed, parent_cluster=parent_cluster)
        
        self.splitting_strategy = splitting_strategy

    def _split_1_d(self, dim):
        
        assert(dim >= 0 and dim <= self.dim)
        (start, end) = self.boundaries[dim]
        half_dist = abs(end-start)/2.0
        first_sec = (start, start + half_dist)
        second_sec = (start + half_dist, end)
        
        boundries_cluster1 = []
        boundries_cluster2 = []
        
        for d, boundries_in_d in enumerate(self.boundaries):
            if(d == dim):
                boundries_cluster1.append(first_sec)
                boundries_cluster2.append(second_sec)
            else:
                boundries_cluster1.append(boundries_in_d)
                boundries_cluster2.append(boundries_in_d)
        
        return boundries_cluster1, boundries_cluster2
    
    def _choose_d_for_splitting(self):
        #for now we implement iterative splitting for debugging 
        #and the random splitting. Later we can think of more criteria
        if(self.splitting_strategy == SPLITTING_RANDOM): return self.next_dimensions_to_be_splitted[randint(len(self.next_dimensions_to_be_splitted))]
        
        if(self.splitting_strategy == SPLITTING_MAX):
            
            balace_pos = 1
            sum_labels_pos = 2
            std_pos = 3
            def _my_cmp(i1,i2):
                #every additional label in sum labels after 2 gets you -5 points
                #every 0.1 in the balance pos gives you 1 point, so balance ration of 1 gives you 10 points. ration below 0.2 gets -3 below 0.1 -5
                # 3pts goes to std, -3*stds
                score1 = 5*(2-i1[sum_labels_pos]) + max(i1[balace_pos] - 0.2, 0)*10 +30*min(i1[balace_pos] - 0.2, 0) -3*i1[std_pos]
                score2 = 5*(2-i2[sum_labels_pos]) + max(i2[balace_pos] - 0.2, 0)*10 +30*min(i2[balace_pos] - 0.2, 0) -3*i2[std_pos]
                return 2*int(score1 < score2) -1 
            
            dim_scores = []
            for dim in range(self.dim):
                #print "check dim %d"%dim
                (start, end) = self.boundaries[dim]
                half_dist = abs(end-start)/2.0
                #first_sec = (start, start + half_dist)
                #second_sec = (start + half_dist, end)
                points_in_1st_cluster = arange(self.num_points)[self.points[dim, :] <= half_dist]        
                points_in_2nd_cluster = arange(self.num_points)[self.points[dim, :] > half_dist]        
                balance_ratio = float(min(len(points_in_1st_cluster), len(points_in_2nd_cluster)))/float(max(len(points_in_1st_cluster), len(points_in_2nd_cluster)))
                
                if(balance_ratio < 0.01): continue #dont want very unbalanced clusters
            
                num_labels_1st = len(set(self.labels[list(set(self.label_pts_indicies).intersection(set(points_in_1st_cluster)))]))
                num_labels_2nd = len(set(self.labels[list(set(self.label_pts_indicies).intersection(set(points_in_2nd_cluster)))]))
                sum_labels = num_labels_1st + num_labels_2nd
                #mean_dist = norm(mean(self.points[:, points_in_1st_cluster]) - mean(self.points[:, points_in_2nd_cluster]))
                sum_stds = std(self.points[:, points_in_1st_cluster])/len(points_in_1st_cluster) + std(self.points[:, points_in_2nd_cluster])/len(points_in_2nd_cluster)
                dim_scores.append((dim, balance_ratio, sum_labels, sum_stds))
                
                
            if(len(dim_scores) == 0): return randint(self.dim)
            dim_scores.sort(cmp = _my_cmp)
            #print dim_scores
            return dim_scores[0][0]
            
            
        #for now the default option is iterative
        return self.next_dimensions_to_be_splitted[0]        
        #if(iterative): return self.next_dimensions_to_be_splitted[0]
        
    def split(self):
        
        split_on_dim = self._choose_d_for_splitting()
        #print "splitting on dimension %d"%split_on_dim
        
        boundaries_list = self._split_1_d(split_on_dim)
        
        cluster_list = []
        #creating a cluster for each new boundry list
        for b_idx, b in enumerate(boundaries_list):
            
            curr_pts_indices = []
            curr_labeled_indicies = []
            curr_external_indicies = None
            if(self.external_indices != None): curr_external_indicies = []
                
            for pts_idx in range(self.num_points):
                
                if (is_point_in_cluster(self.points[:,pts_idx], b)):
                    #here we use the len of the new poits as an index, dont change the order of the lines!
                    if (pts_idx in self.label_pts_indicies): curr_labeled_indicies.append(len(curr_pts_indices))
                    curr_pts_indices.append(pts_idx)
                    #if we keep track of the external indices, pass on this information
                    if(curr_external_indicies != None): curr_external_indicies.append(self.external_indices[pts_idx])
                    
            #print "sub cluster %d has %d points"%(b_idx, len(curr_pts_indices)) 
            if len(curr_pts_indices) > 0:
              
                c = MinSplittingCluster(b, self.points[:,curr_pts_indices], self.labels[curr_pts_indices], self.splitting_strategy,
                            labeled_indicies = curr_labeled_indicies, external_indices=curr_external_indicies, use_seed=self.use_seed, parent_cluster = self)
                cluster_list.append(c)
                
        
        return cluster_list
        
    def query_budget(self, budget):
        
        if(self.use_seed != None): 
            seed(self.use_seed)
        new_queries_made = 0       
        
        new_queries = []
        while (new_queries_made < budget and len(self.unlabeled_pts_indicies) > 0):
                
            query_pt_idx = self.unlabeled_pts_indicies[randint(len(self.unlabeled_pts_indicies))]
            self.label_pts_indicies.append(query_pt_idx)
            self.unlabeled_pts_indicies.remove(query_pt_idx)
            new_queries.append(query_pt_idx)
            new_queries_made += 1
        
        return new_queries_made, new_queries
     
    def get_labels(self):
        
        if(len(self.label_pts_indicies) == 0):
            if (self.parent_cluster != None):
                (parent_homogeneity, max_parnet_label) = self.parent_cluster.homogeneity()
                if(parent_homogeneity > 0): 
                    predicted_labels = max_parnet_label*ones(len(self.labels))
                    return predicted_labels
            print "Cant label the points, have zero information"
            return None
        
        homogeneity, max_label = self.homogeneity()
        predicted_labels = max_label*ones(len(self.labels))
        predicted_labels[self.label_pts_indicies] = self.labels[self.label_pts_indicies]
        
        return predicted_labels
        
#From this point on we have uni-tests for the cluster object     
if __name__ == '__main__':
    
    dim = 3
    num_pts = 10
    boundaries = []
    for i in range(dim): boundaries.append((0,1))
    pts = rand(dim, num_pts)
    labels = ones(num_pts)
    c = Cluster(boundaries, pts, labels)
    cluster_list = c.split()
    print len(cluster_list), cluster_list
    
    

           
