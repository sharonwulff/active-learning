# -*- coding: utf-8 -*-
"""

"""
from numpy import dot, min, max, outer, zeros, arange, array, floor, tile, empty, ones, mean, median, argsort
from numpy.linalg import norm



def pl_properties(data, labels, granularity = 100, pl_def = "conditional-probability", max_dist = None, min_dist = None, truncate_lambda = False, normalize_dist = False):
    
    dim, num_pts = data.shape
    #compute the distance matrix, this can take time..     
    distance_matrix = zeros((num_pts, num_pts))
    for i in range(num_pts):
        if(num_pts > 5000): print "row %d"%i
        for j in range(i):
            n = norm(data[:,i] - data[:,j])
            distance_matrix[i,j] = n
            distance_matrix[j,i] = n
           
    
    print "finished computing the distance matrix!"
    
    if(normalize_dist == True):
        distance_matrix /= max(distance_matrix)
        distance_matrix *= 100.0
    
    if(min_dist == None): min_dist = min(distance_matrix)
    if(max_dist == None): max_dist = max(distance_matrix)    
    
    print "min and max dist : ",min(distance_matrix), max(distance_matrix)
    
    num_lambda_values = granularity
    lambda_range = min_dist+ (arange(num_lambda_values))*((max_dist - min_dist)/float(num_lambda_values))
    pl_lambda = zeros(num_lambda_values)    
            
    #pl(lambda) = P(l(x) != l(y) | dist(x,y) < lambda)
    if(pl_def == "conditional-probability"):
        
        agreement_mat = outer(labels, labels)
        
        for lambda_idx, lambda_val in enumerate(lambda_range):
            matching_entries = agreement_mat[(distance_matrix < lambda_val)]
            if(len(matching_entries) == 0): continue
            pl_lambda[lambda_idx] = 100.0*(sum(matching_entries == -1)/float(len(matching_entries)))

    #pl(lambda) = P(exists y in the sample: dist(x,y) < lambda and l(x) != l(y))
    elif(pl_def == "truncated"):
        
        #matrix with True/False value depending on whether the respective pair of points have same label (works for multi-label as well)
        disagreement_mat = (tile(labels,(num_pts, 1)) != tile(labels,(num_pts, 1)).T)
  
        for lambda_idx, lambda_val in enumerate(lambda_range):
            
            temp = distance_matrix < lambda_val
            #first sum the number of violating examples that each examples sees. Then sum over the ones that see more than 0 such points
            num_violating_examples = sum(sum(temp*disagreement_mat) > 0)
            pl_lambda[lambda_idx] = 100.0*(num_violating_examples/float(num_pts))
            if(num_violating_examples == num_pts):                
                if truncate_lambda:
                    pl_lambda = pl_lambda[:lambda_idx+2]
                    lambda_range = lambda_range[:lambda_idx+2]
                else:
                    pl_lambda[lambda_idx:] = 100.0
                break
            #print lambda_idx, lambda_val, pl_lambda[lambda_idx]
    
    return lambda_range, pl_lambda


def neighbors_distance(data, labels, num_neighbors):
    
    dim, num_pts = data.shape
    #compute the distance matrix, this can take time..     
    distance_matrix = empty((num_pts, num_pts))
    for i in range(num_pts):
        if(num_pts > 5000): print "row %d"%i
        for j in range(i):
            n = norm(data[:,i] - data[:,j])
            distance_matrix[i,j] = n
            distance_matrix[j,i] = n
    
    print "finished computing the distance matrix!"
    
    neighbors_dist = {k:[] for k in num_neighbors}
    for idx in range(num_pts):
        s = argsort(distance_matrix[idx,:])
        for k in num_neighbors:
            neighbors_dist[k].append(mean(distance_matrix[idx,s[1:k+1]])) #the first one is always the element itself
    
    for k in num_neighbors:
        neighbors_dist[k] = mean(neighbors_dist[k])
    
    return neighbors_dist

def unittest_nd():
    
    data = arange(16).reshape(4,4)
    labels = ones(16)
    num_n = [1,2,3]
    neighbors_distance(data, labels, num_n)
    
def unittest_truncated():
    
    data = ones((3,5))
    data[:,0] = -2*data[:,0]
    data[:,1] = -1*data[:,1]
    data[:,2] = 0*data[:,2]
    #data[:,3] = -1*data[:,3]
    data[:,4] = 2*data[:,4]
    labels = array([0,0,1,2,2])
    
    lambda_range, pl_lambda = pl_properties(data, labels, granularity = 100, pl_def = "truncated")
    print lambda_range
    print pl_lambda
    
if __name__ == '__main__':
    
    #unittest_truncated()
    unittest_nd()
    
    #test2    