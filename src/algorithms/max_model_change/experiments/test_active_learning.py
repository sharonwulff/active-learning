# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""
epsilon = 1e-10
seed_num = 1

from utils.gen_data import gen_2_class_guassians
from utils.utils import *
from max_model_change_active_learning import *
from numpy.random import random, randint
from numpy import sign, array, matrix, ones, zeros, nonzero, dot
from pylab import *
import pickle 
import os


def exp_2_results_file_name(dictionary):
    
    s = ""
    keys = dictionary.keys()
    keys.sort()
    for k in keys: s+= "%s_%s_"%(k,str(dictionary[k]))  
    return s[:len(s)-1]

def find_sl_bayes_error_budget(train_data, labels, test_data, test_labels, pts_budget, C, gen_plot = False):
    
    dim, num_points = train_data.shape
    pos_idx = permutation(nonzero(labels > 0.0)[0])[0]
    neg_idx = permutation(nonzero(labels < 0.0)[0])[0]
    acc_test_sl = []
    
    for budget in range(3, pts_budget):
        
        sl_pts = [pos_idx, neg_idx]
        while(len(sl_pts) < budget):
            idx = randint(num_points)
            if(idx not in sl_pts): sl_pts.append(idx)
            
        svm_sl = train(train_data[:, sl_pts], labels[sl_pts], C)
        test_labels_sl = predict(train_data[:, sl_pts], test_data, svm_sl)     
        acc_test_sl.append(100.0*sum(sign(test_labels_sl) != sign(test_labels))/float(len(test_labels)))
        #print acc_test_sl[-1]
        
    if(gen_plot):
        plot_error(acc_test_sl, 'sl: test error')
        show()

def parameters_2_filename(C, seed_num, num_pts, dim, flip, additional=None):
    
    param_str = "al"
    if(C!= None): param_str += '_C_%s'%str(C)
    if(seed_num!= None): param_str += '_seed_%d'%seed_num
    if(num_pts!= None): param_str += '_numpts_%d'%num_pts
    if(dim!= None): param_str += '_dim_%d'%dim
    if(flip!= None): param_str += '_flip_%2.3f'%flip
    if additional != None: 
        for param_name, value in additional.iteritems(): param_str += '_%s_%s'%(param_name, str(value))
    return param_str

def filename_2_parameters(filename):
    
    params_arr = filename[:filename.rindex('.')].split('_')
    seed_num = int(params_arr[2])
    num_pts = int(params_arr[4])
    dim = int(params_arr[6])
    C = float(params_arr[8])
    flip = float(params_arr[10])
    
    if(len(params_arr) > 11):
        additional = {}
        for idx in range(11, len(params_arr), 2): additional[params_arr[idx]] = [params_arr[idx+1]]
        return C, seed_num, num_pts, dim, flip, additional
        
    return C, seed_num, num_pts, dim, flip

def write_results_2_file(filename, data = None, labels = None, al_pts = None):
    
    outputfile = open('%s.pkl'%filename, 'wb')
    d = {'data':data, 'labels':labels, 'al_pts':al_pts}
    pickle.dump(d, outputfile)
    return '%s.pkl'%filename
    
def results_from_file(filename):
    
    if('.pkl' not in filename): filename = '%s.pkl'%filename,
    if(not os.path.exists(filename)):return None
    pkl_file = open(filename, 'rb')
    d = pickle.load(pkl_file)
    return d['data'], d['labels'],d['al_pts']


def get_predictions(train_data, train_labels, test_data, C):
    
    svm = train_with_svm(train_data, train_labels, C)
    predictions = predict(train_data, test_data, svm)
    return predictions

def compare_al_with_sl(train_data, labels, test_data, test_labels, C, budget, al_chosen_pts, num_repetitions = 1, gen_plot = False):
    
    print "compare_al_with_sl"    
    
    #seed(seed_num)
    acc_train_sl = zeros(budget)
    acc_train_al = zeros(budget)
    acc_remaining_train_sl = zeros(budget)       
    acc_remaining_train_al = zeros(budget)
    acc_test_sl = zeros(budget)
    acc_test_al = zeros(budget)
        
    for curr_budget in range(2,budget):
        print "budget: %d"%budget 
        
        for curr_repetition in range(num_repetitions):                
            
            dim, num_points = train_data[curr_repetition].shape
             
            #choose points in random for sl, to be fare we start with one from each class given to the al
            sl_pts = [al_chosen_pts[curr_repetition][0], al_chosen_pts[curr_repetition][1]]
            while(len(sl_pts) < curr_budget):
                idx = randint(num_points)
                if(idx not in sl_pts): sl_pts.append(idx)
         
            svm_sl = train(train_data[curr_repetition][:, sl_pts], labels[curr_repetition][sl_pts], C)
            train_labels_sl = predict(train_data[curr_repetition][:, sl_pts], train_data[curr_repetition][:, sl_pts], svm_sl)
            wsl, bsl = get_hyperplane_from_svm(svm_sl, train_data[curr_repetition][:, sl_pts])
            remaining_train_labels_sl = predict(train_data[curr_repetition][:, sl_pts], train_data[curr_repetition], svm_sl)                
            #test_labels_sl = predict(train_data[curr_repetition][:, sl_pts], test_data[curr_repetition], svm_sl)        
           
            al_pts = al_chosen_pts[curr_repetition][:curr_budget]
            svm_al = train(train_data[curr_repetition][:, al_pts], labels[curr_repetition][al_pts], C)
            train_labels_al = predict(train_data[curr_repetition][:, al_pts], train_data[curr_repetition][:, al_pts], svm_al)    
            wal, bal = get_hyperplane_from_svm(svm_al, train_data[curr_repetition][:, al_pts])        
            remaining_train_labels_al = predict(train_data[curr_repetition][:, al_pts], train_data[curr_repetition], svm_al)            
            #test_labels_al = predict(train_data[curr_repetition][:, al_pts], test_data[curr_repetition], svm_al) 
            
            #measure the error on the train
            acc_train_sl[curr_budget-2] = 100.0*sum(sign(train_labels_sl) != sign(labels[curr_repetition][sl_pts]))/float(num_repetitions* len(labels[curr_repetition][sl_pts]))
            acc_train_al[curr_budget-2] = 100.0*sum(sign(train_labels_al) != sign(labels[curr_repetition][al_pts]))/float(num_repetitions* len(labels[curr_repetition][al_pts]))        
            #measure the error on the remaining train points                
            acc_remaining_train_sl[curr_budget-2] = 100.0*sum(sign(remaining_train_labels_sl) != sign(labels[curr_repetition]))/float(num_repetitions* len(labels[curr_repetition]))        
            acc_remaining_train_al[curr_budget-2] = 100.0*sum(sign(remaining_train_labels_al) != sign(labels[curr_repetition]))/float(num_repetitions* len(labels[curr_repetition]))
            #measure the error on the test data 
            #acc_test_sl[curr_budget-2] = 100.0*sum(sign(test_labels_sl) != sign(test_labels[curr_repetition]))/float(num_repetitions* len(test_labels[curr_repetition]))
            #acc_test_al[curr_budget-2] = 100.0*sum(sign(test_labels_al) != sign(test_labels[curr_repetition]))/float(num_repetitions* len(test_labels[curr_repetition]))
                
    if(gen_plot):
        
        plot_error(acc_remaining_train_sl, 'sl: remaining train error')
        plot_error(acc_remaining_train_al, 'al: remaining train error')
        #plot_error(acc_test_sl, 'sl: test error')
        #plot_error(acc_test_al, 'al: test error')
        show()
     
    return  acc_train_sl, acc_train_al, acc_remaining_train_sl, acc_remaining_train_al, acc_test_sl, acc_test_al
     
if __name__ == '__main__':
    
    seed(seed_num)    
    num_pts = 75
    num_test = 200
    budget = 50
    
    #Cs = [1000,1]
    Cs = [1]
    dims = [10]
    flip_percs = [0.05,0.1,0.2]
    flip_percs = [0.2]    
    models = []
    for flip in flip_percs:
        for C in Cs:
            for d in dims: models.append((flip, C, d))    
    
    results_dir = 'results_small'
    action = 'compute'
    action = 'compare'
    
    if(action == 'compare'):
        
        for f in os.listdir(results_dir):
            
            if('.pkl' not in f): continue
            print "extracting results of ",f
            C, seed_num, num_pts, dim, flip = filename_2_parameters(f)
            data, labels, al_chosen_pts = results_from_file(results_dir+'/'+f)
            test_data, test_labels = gen_2_class_guassians(dim, num_test, num_test, flip_perc = flip)
            compare_al_with_sl(data, labels, test_data, test_labels, C, al_chosen_pts, gen_plot = True)
        
    elif(action == 'compute'):
        
        for model_idx, (flip, C, dim) in enumerate(models):
            
            print "%d computing al for "%model_idx, (flip, C, dim)
            param_str = parameters_2_filename(C, seed_num, num_pts, dim, flip)
            data, labels = gen_2_class_guassians(dim, num_pts, num_pts, flip_perc = flip)
            
            log_file_name = "%s.log"%param_str
            outputfile = open(log_file_name, 'w')
            wal, bal, chosen_pts = max_model_change_active_learning(data, labels, budget, C, log_file = outputfile)
            outputfile.close()
            
            write_results_2_file(param_str, data = data, labels = labels, al_pts = chosen_pts)
            