# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 18:54:55 2012

@author: sharon
"""


from utils.utils import *
from numpy.random import random, randint
from numpy import sign, array, matrix, ones, zeros, nonzero, dot, abs, argmin
from pylab import *

def boundry_active_learning(data, labels, budget, C, information_score = None,
                                     log_file = None, plot_progress = False):
    
    #seed(seed_num)
    dim, num_points = data.shape    
    #get the first two point for free, because the shogun SVM cant train unless 
    #it gets representation of both classes in the training data
    pos_idx = permutation(nonzero(labels > 0.0)[0])[0]
    neg_idx = permutation(nonzero(labels < 0.0)[0])[0]
    revealed_points_indices = [pos_idx, neg_idx]    
    print "starting off with two points: ", revealed_points_indices    
    
    while(len(revealed_points_indices) < budget):

        svm_curr = train(data[:,array(revealed_points_indices)], labels[revealed_points_indices], C)
        labels_curr = predict(data[:,revealed_points_indices], data, svm_curr)            
        wcurr, bcurr = get_hyperplane_from_svm(svm_curr, data[:,array(revealed_points_indices)])
        
        #prevent the points already chosen to be chosen again
        labels_curr[revealed_points_indices] = max(labels_curr)
        closest_to_boundry = argmin(abs(labels_curr))
        revealed_points_indices.append(closest_to_boundry)        
        print "appending %d to the points with labels, it has a margin of "%closest_to_boundry, labels_curr[closest_to_boundry]
        
        if(plot_progress):
            plot_2d(data, labels)
            highlight_2d(data[:,closest_to_boundry], [labels[closest_to_boundry]])
            plot_hyperplane(wcurr, bcurr , 'y')
            show()
        
        if(log_file != None):
            log_file.write("\n budget %d: ["%t)
            for idx in revealed_points_indices: log_file.write("%d, "%idx)
            log_file.write("]\n")
            
    print "finally we have these points: ",revealed_points_indices
    svm = train(data[:,array(revealed_points_indices)], labels[array(revealed_points_indices)], C)        
    w, b = get_hyperplane_from_svm(svm, data[:,array(revealed_points_indices)])
    
    return w, b, revealed_points_indices
    
