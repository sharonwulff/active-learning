# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 18:54:55 2012

@author: sharon
"""

from shogun.Kernel import LinearKernel
from shogun.Features import RealFeatures, Labels
from shogun.Classifier import LibSVM
from utils.utils import *
from numpy.random import random, randint, permutation
from numpy import sign, array, matrix, ones, zeros, nonzero, dot, ceil
from pylab import *


def max_model_change_active_learning(data, labels, budget, C, information_score = None, error_perc = 0.1, log_file = None, plot_progress = False, sample_random_in_no_change = False):

    #seed(seed_num)
    dim, num_points = data.shape    
    #get the first two point for free, because the shogun SVM cant train unless 
    #it gets representation of both classes in the training data
    pos_idx = permutation(nonzero(labels > 0.0)[0])[0]
    neg_idx = permutation(nonzero(labels < 0.0)[0])[0]
    revealed_points_indices = [pos_idx, neg_idx]    
    print "starting off with two points: ", revealed_points_indices    
    
    
    #see if this helps for memory consumption
    temp_revealed_points_indices = []
    temp_train_data = data
    temp_train_labels = labels
    svm_curr = None
    labels_curr = labels
        
    for t in range(budget):
            
        svm_curr = train_with_svm(data[:,array(revealed_points_indices)], labels[revealed_points_indices], C)
        labels_curr = predict(data[:,revealed_points_indices], data, svm_curr)            
        #wcurr, bcurr = get_hyperplane_from_svm(svm_curr, data[:,array(revealed_points_indices)])
        
        max_change_index = -1
        max_change = -10
        max_allowed_error_on_known = ceil(error_perc*t)
        min_change_to_consider = ceil(0.05*num_points)
        max_score = -1
        
        
        for i in range(num_points):
            if(i in revealed_points_indices): continue
            
            change = 0            
            error_on_known = 0            
            score = 0
            
            temp_revealed_points_indices = [idx for idx in revealed_points_indices]
            temp_revealed_points_indices.append(i)
            temp_train_data = data[:,array(temp_revealed_points_indices)]
            temp_train_labels = [label for label in labels[revealed_points_indices]]
        
            if(information_score == 'actual'):
                 
                 temp_train_labels.append(labels[i])
                 svm = train_with_svm(temp_train_data, array(temp_train_labels), C)
                 predictions = predict(temp_train_data, data, svm)
                 error_on_known = sum(sign(predictions[revealed_points_indices]) != sign(labels[revealed_points_indices]))
                 #w, b = get_hyperplane_from_svm(svm, temp_train_data)
                 change = model_diff(predictions, labels_curr)
                 score = 0.8*change + 0.2*(len(revealed_points_indices) - error_on_known)
                 objective = svm.get_objective()
            else:
                temp_train_labels1 = list(temp_train_labels)
                temp_train_labels1.append(-1.0)
                temp_train_labels2 = list(temp_train_labels)
                temp_train_labels2.append(1.0)
            
                svm1 = train_with_svm(temp_train_data, array(temp_train_labels1), C)
                predictions1 = predict(temp_train_data, data, svm1)
                objective1 = svm1.get_objective()
                #w1, b1 = get_hyperplane_from_svm(svm1, temp_train_data)
                
                svm2 = train_with_svm(temp_train_data, array(temp_train_labels2), C)
                predictions2 = predict(temp_train_data, data, svm2)
                objective2 = svm1.get_objective()
                #w2, b2 = get_hyperplane_from_svm(svm2, temp_train_data)
            
                change = 0.5*(model_diff(predictions1, labels_curr)) + 0.5*(model_diff(predictions2, labels_curr))
                error_on_known = 0.5*(sum(sign(predictions1[revealed_points_indices]) != sign(labels[revealed_points_indices])) + 
                                          sum(sign(predictions2[revealed_points_indices]) != sign(labels[revealed_points_indices])))
                score =  0.8*change + 0.2*(len(revealed_points_indices) - error_on_known)
                objective = 0.5*(objective1 + objective2)
                #max_potential_change = max(model_diff(predictions1, labels_curr), model_diff(predictions2, labels_curr))    
                #hyperplane_avg_change = 0.5*(norm(w1 - wcurr) + norm(w2 - wcurr))
            
                # choose a score to assign to the change
                #score = avg_score         
                #if(information_score == 'max'):
                #    change = max_potential_change
                #elif(information_score == 'hyperplane'):
                #    change = hyperplane_avg_change
            
            #print i, avg_change, actual_change, max_potential_change
            if(score > max_score and error_on_known <= max_allowed_error_on_known and change > error_on_known and change > min_change_to_consider):
            #if(change > max_change and error_on_known <= max_allowed_error_on_known):
                print "found a better change score %f change before %d now %d, with error on known %d"%(score, max_change, change, error_on_known)
                max_score = score                
                max_change = change
                max_change_index = i
                print "objective ",objective
        
        if(max_change <= 0):
            print "there are no points which improve the model change!"
            if(not sample_random_in_no_change):break
            #in case we didnt find any point which changes the score, and the above flag is on, sample in random
            max_change_index = randint(num_points)
            while(max_change_index in revealed_points_indices): max_change_index = randint(num_points)
            
            
        revealed_points_indices.append(max_change_index)        
        print "appending %d to the %d points with labels"%(max_change_index, len(revealed_points_indices))
        
        if(plot_progress):
            plot_2d(data, labels)
            highlight_2d(data[:,revealed_points_indices], labels[revealed_points_indices])
            svm = train(data[:,array(revealed_points_indices)], labels[array(revealed_points_indices)], C)        
            w, b = get_hyperplane_from_svm(svm, data[:,array(revealed_points_indices)])
            plot_hyperplane(w, b, 'y')
            show()
            
        if(log_file != None):
            log_file.write("\n budget %d: ["%t)
            for idx in revealed_points_indices: log_file.write("%d, "%idx)
            log_file.write("]\n")
            
    print "finally we have these points: ",revealed_points_indices
    #svm = train(data[:,array(revealed_points_indices)], labels[array(revealed_points_indices)], C)        
    #w, b = get_hyperplane_from_svm(svm, data[:,array(revealed_points_indices)])
    return revealed_points_indices
