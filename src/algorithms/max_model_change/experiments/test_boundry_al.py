# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""
from utils.utils import plot_2d, plot_error, plot_hyperplane, get_uci_data
from utils.gen_data import gen_2_class_guassians
from boundry_active_learning import *
from numpy.random import random, randint, permutation
from numpy import sign, array, matrix, ones, zeros, nonzero, dot
from pylab import show
from test_active_learning import *
import os

num_pts = 250
num_test = 1000
budget = 100
repetitions = 4

def test_boundry_al_on_synthetic_data(models, action):
    
    for model_idx, (flip, C, dim) in enumerate(models): 
        print "%d computing al criteria for "%model_idx, (flip, C, dim)
            
        if(action == 'compute'):
            
            for repetition_id in range(repetitions):
            
                additionals = {'repetition':repetition_id}
                param_str = parameters_2_filename(C, None, num_pts, dim, flip, additional = additionals)        
            
                data, labels = gen_2_class_guassians(dim, num_pts, num_pts, flip_perc = flip)
                wal, bal, chosen_pts = boundry_active_learning(data, labels, budget, C, plot_progress = False)
                assert(len(chosen_pts) == budget)
                write_results_2_file(param_str, data = data, labels = labels, al_pts = chosen_pts)        
        
        elif(action == 'compare'): 
            
            train_data = []
            train_labels = []
            al_chosen_pts = []            
            test_data = []
            test_labels = []
            
            for repetition_id in range(repetitions):
            
                additionals = {'repetition':repetition_id}
                param_str = parameters_2_filename(C, None, num_pts, dim, flip, additional = additionals)        
                filename = param_str + '.pkl'
                if(not os.path.exists(filename)): continue
                
                print filename
                curr_train_data, curr_labels, curr_al_chosen_pts = results_from_file(filename)
                curr_test_data, curr_test_labels = gen_2_class_guassians(dim, num_test, num_test, noise_level = flip)
                train_data.append(curr_train_data)
                train_labels.append(curr_labels)
                al_chosen_pts.append(curr_al_chosen_pts)       
                test_data.append(curr_test_data)
                test_labels.append(curr_test_labels)
                                
            compare_al_with_sl(train_data, train_labels, test_data, test_labels, C, budget, al_chosen_pts, num_repetitions = len(train_data), gen_plot = True)
               

def test_boundry_al_on_uci(models, action):
    
    frac_train = 0.5
    
    for model_idx, (data, C) in enumerate(models): 
        print "%d computing al criteria for "%model_idx, (data, C)
            
        if(action == 'compute'):
            
            for repetition_id in range(repetitions):
            
                train_data, train_labels, test_data, test_labels = get_uci_data(data, frac_train, perm_idx = repetition_id)
                additionals = {'dataset':data, 'repetition':repetition_id, 'split':frac_train}
                param_str = parameters_2_filename(C, None, None, None, None, additional = additionals)     
                #filename = param_str + '.pkl'
                print param_str
               
                wal, bal, chosen_pts = boundry_active_learning(train_data, train_labels, budget, C, plot_progress = False)
                write_results_2_file(param_str, data = train_data, labels = train_labels, al_pts = chosen_pts)       
        
        elif(action == 'compare'): 
            
            train_data = []
            train_labels = []
            al_chosen_pts = []            
            test_data = []
            test_labels = []
            
            for repetition_id in range(repetitions):
                
                additionals = {'dataset':data, 'repetition':repetition_id, 'split':frac_train}
                param_str = parameters_2_filename(C, None, None, None, None, additional = additionals)        
                filename = param_str + '.pkl'
                if(not os.path.exists(filename)): continue
                
                print filename
                dummy1, dummy2, curr_al_chosen_pts = results_from_file(filename) 
                curr_train_data, curr_train_labels, curr_test_data, curr_test_labels = get_uci_data(data, frac_train, perm_idx = repetition_id)
                
                train_data.append(curr_train_data)
                train_labels.append(curr_train_labels)
                al_chosen_pts.append(curr_al_chosen_pts)       
                test_data.append(curr_test_data)
                test_labels.append(curr_test_labels)
                                
            compare_al_with_sl(train_data, train_labels, test_data, test_labels, C, budget, al_chosen_pts, num_repetitions = len(train_data), gen_plot = True)

        
if __name__ == '__main__':
    
    
    Cs = [0.1,0.01]
    dims = [2]
    flip_percs = [0.1]
    datasets = ['wpbc','ionosphere','pima','wdbc']       
    datasets = ['ionosphere','wdbc']   
    
    models = []
    for data in datasets:
        for C in Cs:
            #for d in dims: 
                #models.append((flip, C, d))    
            models.append((data, C))        
                
    #action = 'compute'    
    action = 'compare'
    #action = 'compute_bayes'
    
    #test_boundry_al_on_synthetic_data(models, action)
    test_boundry_al_on_uci(models, action)
    
    if(action == 'compute_bayes'):
        train_data, labels = gen_2_class_guassians(dim, num_pts, num_pts, flip_perc = flip)
        test_data, test_labels = gen_2_class_guassians(dim, num_test, num_test, flip_perc = flip)
        find_sl_bayes_error_budget(train_data, labels, test_data, test_labels, budget, C, gen_plot = True) 
 