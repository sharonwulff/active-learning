# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""
epsilon = 1e-10
seed_num = 1

from utils.gen_data import gen_2_class_guassians
from utils.utils import plot_2d, plot_error
from max_model_change_active_learning import *
from test_active_learning import *
from utils.expttools import apply_to_combination

from numpy.random import random, randint
from numpy import sign, array, matrix, ones, zeros, nonzero, dot
from pylab import show
import os


def compare_al_with_sl(train_data, labels, test_data, test_labels, C, al_chosen_pts, gen_plot = False,
                       results_dir, num_pts, flip, C, dim, budget, criteria, error_perc, perm):
    
    print "compare_al_with_sl"
    #seed(seed_num)
    dim, num_points = data.shape
    acc_train_sl = []
    acc_train_al = [] 
    acc_remaining_train_sl = []       
    acc_remaining_train_al = []
    acc_test_sl = []
    acc_test_al = []
        
    for budget in range(3,len(al_chosen_pts)):
        
        #print "budget: %d"%budget 
        #choose points in random for sl, to be fare we start with one from each class given to the al
        sl_pts = [al_chosen_pts[0], al_chosen_pts[1]]
        while(len(sl_pts) < budget):
            idx = randint(num_points)
            if(idx not in sl_pts): sl_pts.append(idx)
     
        svm_sl = train(train_data[:, sl_pts], labels[sl_pts], C)
        train_labels_sl = predict(train_data[:, sl_pts], train_data[:, sl_pts], svm_sl)
        wsl, bsl = get_hyperplane_from_svm(svm_sl, train_data[:, sl_pts])
        remaining_train_labels_sl = predict(train_data[:, sl_pts], train_data, svm_sl)                
        test_labels_sl = predict(train_data[:, sl_pts], test_data, svm_sl)        
       
        al_pts = al_chosen_pts[:budget]
        svm_al = train(train_data[:, al_pts], labels[al_pts], C)
        train_labels_al = predict(train_data[:, al_pts], train_data[:, al_pts], svm_al)    
        wal, bal = get_hyperplane_from_svm(svm_al, train_data[:, al_pts])        
        remaining_train_labels_al = predict(train_data[:, al_pts], train_data, svm_al)            
        test_labels_al = predict(train_data[:, al_pts], test_data, svm_al) 
        
        #measure the error on the train
        acc_train_sl.append(100.0*sum(sign(train_labels_sl) != sign(labels[sl_pts]))/float(len(labels[sl_pts])))
        acc_train_al.append(100.0*sum(sign(train_labels_al) != sign(labels[al_pts]))/float(len(labels[al_pts])))         
        #measure the error on the remaining train points                
        acc_remaining_train_sl.append(100.0*sum(sign(remaining_train_labels_sl) != sign(labels))/float(len(labels)))        
        acc_remaining_train_al.append(100.0*sum(sign(remaining_train_labels_al) != sign(labels))/float(len(labels)))
        #measure the error on the test data 
        acc_test_sl.append(100.0*sum(sign(test_labels_sl) != sign(test_labels))/float(len(test_labels)))
        acc_test_al.append(100.0*sum(sign(test_labels_al) != sign(test_labels))/float(len(test_labels)))        
                
        #for i in range(len(labels)):
        #    if(sign(test_labels_al[i]) != sign(labels[i]) or sign(test_labels_al[i]) != sign(labels[i])):
        #        print i,test_labels_al[i], test_labels_sl[i], labels[i]     
        
        #print "test error ",acc_test_al, acc_test_sl 
        #print "train error ",acc_train_al, acc_train_sl 
        if (dim == 2):
            plot_2d(data, labels)
            plot_hyperplan(wsl, bsl, 'g')
            plot_hyperplan(wal, bal, 'y')
            show()
        
    if(gen_plot):
        print "plotting!"
        plot_error(acc_remaining_train_sl, 'sl: remaining train error')
        plot_error(acc_remaining_train_al, 'al: remaining train error')
        plot_error(acc_test_sl, 'sl: test error')
        plot_error(acc_test_al, 'al: test error')
        show()


def compare_al_to_sl_averaged_over_data_splits(results_dir, num_train, num_test, flip, C, dim, max_budget, perms, criteria, error_perc, gen_plot = True):
                                            
    perms_found = []
    for perm in perms:    
        
        print "compare_al_to_sl_averaged_over_data_splits, perm ==", perm
        file_name_parameters = {'flip':flip,'C':C, 'dim':dim, 'criteria':criteria, 'error':error_perc, 'perm':perm}
        param_str = exp_2_results_file_name(file_name_parameters)
        if(results_dir != None): filename = "%s/%s.pkl"%(results_dir, filename)        
        if(not os.path.exists(filename)): 
            print "file not found ", filename
            continue
        perms_found.append(perm)
    
    num_perms = len(perms_found)    
    if(num_perms < 1): return
    
    accuracies_al = zeros((num_perms, max_budget+2)) #the +2 is for because we always start off from 2 
    accuracies_sl = zeros((num_perms, max_budget+2))    
    num_chosen_pts = []    
    
    for idx, perm in enumerate(perms_found):
    
        print "compare_al_to_sl_averaged_over_data_splits, perm %d of %d"%(perm, num_perms)
        file_name_parameters = {'flip':flip,'C':C, 'dim':dim, 'criteria':criteria, 'error':error_perc, 'perm':perm}
        param_str = exp_2_results_file_name(file_name_parameters)
        if(results_dir != None): filename = "%s/%s.pkl"%(results_dir, filename)        
        print filename
        
        seed(perm)
        train_data, train_labels = gen_2_class_guassians(dim, num_train, num_train, flip_perc = flip)
        dummy1, dummy2, al_chosen_pts = results_from_file(filename)
        num_chosen_pts.append(len(al_chosen_pts))
        print "num al points ",len(al_chosen_pts)
        sl_perm = permutation(num_train)
        sl_perm[:2] = al_chosen_pts[:2]
        for i in range(2,len(al_chosen_pts)):
            
            predictions_al = get_predictions(train_data[:,al_chosen_pts[:i]], train_labels[al_chosen_pts[:i]], train_data, C)
            accuracies_al[idx, i] = 100.0*sum(sign(train_labels) == sign(predictions_al))/float(len(predictions_al))
            
            predictions_sl = get_predictions(train_data[:,sl_perm[:i]], train_labels[sl_perm[:i]], train_data, C)
            accuracies_sl[idx, i] = 100.0*sum(sign(train_labels) == sign(predictions_sl))/float(len(predictions_sl))
        
        #fill up the rest of the array with the accuracy of the last iteration
        accuracies_al[idx, len(al_chosen_pts):] = accuracies_al[idx, len(al_chosen_pts)-1]
        accuracies_sl[idx, len(al_chosen_pts):] = accuracies_sl[idx, len(al_chosen_pts)-1]
    
     
    
    print  mean(accuracies_al,0)#, std(accuracies_al,0)
    print  mean(accuracies_sl,0)#, std(accuracies_sl,0)
    
    if(gen_plot):
        
        title = "%s train error"%(exp_2_results_file_name(file_name_parameters))
        plot_with_error(arange(max_budget+2), mean(accuracies_al,0), std(accuracies_al,0), max_budget+10, 100, title, "max model change", color = "r", icon = "o")
        plot_with_error(arange(max_budget+2), mean(accuracies_sl,0), std(accuracies_sl,0), max_budget+10, 100, title, "statistical learning", color = "g", icon = "+")
        show()  
        
        
def max_model_active_learning_wrapper(results_dir, num_pts, flip, C, dim, budget, criteria, error_perc, perm):
    
    seed(perm)
    data, labels = gen_2_class_guassians(dim, num_pts, num_pts, flip_perc = flip)
    file_name_parameters = {'flip':flip,'C':C, 'dim':dim, 'criteria':criteria, 'error':error_perc, 'perm':perm}
    param_str = exp_2_results_file_name(file_name_parameters)
    print param_str
    chosen_pts = max_model_change_active_learning(data, labels, budget, C, information_score = criteria, error_perc = error_perc, plot_progress = False)
    write_results_2_file(results_dir+'/'+param_str, data = None, labels = None, al_pts = chosen_pts)
        
if __name__ == '__main__':
    
    seed(seed_num)    
    num_pts = 50
    num_test = 400
    budget = 60
    
    Cs = [10]
    dims = [10]
    flip_percs = [0.1]
    criterias = ['avg','actual','hyperplane']
    criterias = ['actual'] 
    #error_percs = [0.05,0.2,0.3]
    error_percs = [0.2]
    perms = [0,1,2]
    
    exp_id = 'max_model_change_on_syntethic'
    results_dir = 'results'+'/'+exp_id
    if(not os.path.exists(results_dir)): os.mkdir(results_dir)    
                
    action = 'compute'    
    #action = 'compare'
    #action = 'compute_bayes'
    
    if(action == 'compare'): 
        
        fixed = {'results_dir':results_dir, 'num_test':num_test, 'max_budget':budget, 'criteria':criteria, 'perms':perms, 'num_train':num_train}
        param = {'dim':dims, 'flip':flip_percs, 'C':Cs, 'error_perc':error_percs}        
        
        apply_to_combination(compare_al_to_sl_averaged_over_data_splits, param, fixed=fixed, multiproc=False)
        
    
    elif(action == 'compute'):    
    
        fixed = {'results_dir':results_dir, 'budget':budget, 'num_pts':num_pts, }
        param = {'dim':dims, 'flip':flip_percs, 'C':Cs, 'error_perc':error_percs, 'criteria':criterias,'perm':perms}
        apply_to_combination(max_model_active_learning_wrapper, param, fixed=fixed, multiproc=False)            
    

    
    if(action == 'compare'):
        
        #for f in os.listdir(results_dir):
        for model_idx, (flip, C, dim, criteria) in enumerate(models):   
            
            print "%d results for "%model_idx, (flip, C, dim, criteria)
            additionals = {'criteria':criteria}
            param_str = parameters_2_filename(C, seed_num, num_pts, dim, flip, additional = additionals)

            for f in os.listdir(results_dir):
                if(param_str in f): 
                    data, labels, al_chosen_pts = results_from_file(results_dir+'/'+f)
                    test_data, test_labels = gen_2_class_guassians(dim, num_test, num_test, flip_perc = flip)
                    compare_al_with_sl(data, labels, test_data, test_labels, C, al_chosen_pts, gen_plot = True)
                    break
                
   