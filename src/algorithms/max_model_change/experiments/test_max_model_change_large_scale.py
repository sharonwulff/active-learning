# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""
from gen_data import gen_2_class_guassians
from utils import plot_2d, plot_error, plot_hyperplane
from max_model_change_active_learning import *
from numpy.random import random, randint, permutation
from numpy import sign, array, matrix, ones, zeros, nonzero, dot
from pylab import show
from test_active_learning import *
import os


        
if __name__ == '__main__':
    
    seed(seed_num)    
    num_pts = 250
    num_test = 1000
    budget = 30
    
    Cs = [0.01]
    dims = [50]
    flip_percs = [0.2]
    criterias = ['hyperplane']
    #criterias = ['avg','actual']    
    criterias = ['avg']        
    models = []
    for flip in flip_percs:
        for C in Cs:
            for d in dims: 
                for criteria in criterias: models.append((flip, C, d, criteria))    
    
    action = 'compute'    
    action = 'compare'
    action = 'compute_bayes'
    
    for model_idx, (flip, C, dim, criteria) in enumerate(models): 
                
        print "%d computing al criteria for "%model_idx, (flip, C, dim, criteria)
        additionals = {'criteria':criteria}
        param_str = parameters_2_filename(C, seed_num, num_pts, dim, flip, additional = additionals)        
        
        if(action == 'compute'):
            
            data, labels = gen_2_class_guassians(dim, num_pts, num_pts, noise_level = flip)
            wal, bal, chosen_pts = max_model_change_active_learning(data, labels, budget, C, information_score = criteria,
            plot_progress = False)
            write_results_2_file(param_str, data = data, labels = labels, al_pts = chosen_pts)        
        
        elif(action == 'compare'): 
            
            print "%d results for "%model_idx, (flip, C, dim, criteria)
            filename = param_str + '.pkl'
            if(not os.path.exists(filename)): continue
                
            train_data, labels, al_chosen_pts = results_from_file(filename) 
            test_data, test_labels = gen_2_class_guassians(dim, num_test, num_test, noise_level = flip)
            compare_al_with_sl(train_data, labels, test_data, test_labels, C, al_chosen_pts, gen_plot = True)
           
            
        elif(action == 'compute_bayes'):
            
            train_data, labels = gen_2_class_guassians(dim, num_pts, num_pts, noise_level = flip)
            test_data, test_labels = gen_2_class_guassians(dim, num_test, num_test, noise_level = flip)
            find_sl_bayes_error_budget(train_data, labels, test_data, test_labels, budget, C, gen_plot = True) 
     