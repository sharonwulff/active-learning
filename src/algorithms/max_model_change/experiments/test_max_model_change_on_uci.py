# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""

from test_active_learning import *
from numpy import ceil
from utils.utils import plot_2d, plot_error, get_uci_data
from pylab import show
from numpy.random import permutation
from utils.expttools import apply_to_combination
"""
import os
from utils import plot_2d, plot_error, plot_hyperplane
from numpy.random import random, randint, permutation
from numpy import sign, array, matrix, ones, zeros, nonzero, dot
from pylab import show
"""
    
def compare_al_to_sl_averaged_over_data_splits(max_budget, results_dir, data, C, perms, criteria, error_perc, train_frac, gen_plot = True):
    
    
    perms_found = []
    for perm in perms:    
        
        print "compare_al_to_sl_averaged_over_data_splits, perm ==", perm
        file_name_parameters = {'dataset':data,'C':C, 'perm':perm, 'criteria':criteria, 'error':error_perc}
        filename = exp_2_results_file_name(file_name_parameters)
        if(results_dir != None): filename = "%s/%s.pkl"%(results_dir, filename)        
        if(not os.path.exists(filename)): 
            print "file not found ", filename
            continue
        perms_found.append(perm)
    
    num_perms = len(perms_found)    
    if(num_perms < 1): return
    
    accuracies_al = zeros((num_perms, max_budget+2)) #the +2 is for because we always start off from 2 
    accuracies_sl = zeros((num_perms, max_budget+2))    
    num_chosen_pts = []    
    
    for idx, perm in enumerate(perms_found):
    
        print "compare_al_to_sl_averaged_over_data_splits, perm %d of %d"%(perm, num_perms)
        file_name_parameters = {'dataset':data,'C':C, 'perm':perm, 'criteria':criteria, 'error':error_perc}
        filename = exp_2_results_file_name(file_name_parameters)
        if(results_dir != None): filename = "%s/%s.pkl"%(results_dir, filename)        
        print filename
        
        train_data, train_labels, test_data, test_labels = get_uci_data(data, frac_train, perm_idx = perm)
        dim, num_train = train_data.shape
        dummy1, dummy2, al_chosen_pts = results_from_file(filename)
        num_chosen_pts.append(len(al_chosen_pts))
        print "num al points ",len(al_chosen_pts)
        sl_perm = permutation(num_train)
        sl_perm[:2] = al_chosen_pts[:2]
        for i in range(2,len(al_chosen_pts)):
            
            predictions_al = get_predictions(train_data[:,al_chosen_pts[:i]], train_labels[al_chosen_pts[:i]], train_data, C)
            accuracies_al[idx, i] = 100.0*sum(sign(train_labels) == sign(predictions_al))/float(len(predictions_al))
            
            predictions_sl = get_predictions(train_data[:,sl_perm[:i]], train_labels[sl_perm[:i]], train_data, C)
            accuracies_sl[idx, i] = 100.0*sum(sign(train_labels) == sign(predictions_sl))/float(len(predictions_sl))
        
        #fill up the rest of the array with the accuracy of the last iteration
        accuracies_al[idx, len(al_chosen_pts):] = accuracies_al[idx, len(al_chosen_pts)-1]
        accuracies_sl[idx, len(al_chosen_pts):] = accuracies_sl[idx, len(al_chosen_pts)-1]
    
     
    
    print  mean(accuracies_al,0)#, std(accuracies_al,0)
    print  mean(accuracies_sl,0)#, std(accuracies_sl,0)
    
    if(gen_plot):
        
        title = "%s train error"%(exp_2_results_file_name(file_name_parameters))
        plot_with_error(arange(max_budget+2), mean(accuracies_al,0), std(accuracies_al,0), max_budget+10, 100, title, "max model change", color = "r", icon = "o")
        plot_with_error(arange(max_budget+2), mean(accuracies_sl,0), std(accuracies_sl,0), max_budget+10, 100, title, "statistical learning", color = "g", icon = "+")
        show()           

def max_model_active_learning_wrapper_for_proc(d):
    
    max_model_active_learning_wrapper(**d)

def max_model_active_learning_wrapper(results_dir, dataname, C, frac_train, perm_idx, budget, criteria, error_perc):
    
    print results_dir, dataname, C, frac_train, perm_idx, budget, criteria, error_perc
    train_data, train_labels, test_data, test_labels = get_uci_data(dataname, frac_train, perm_idx = perm_idx)
    dim, num_train = train_data.shape
    file_name_parameters = {'dataset':dataname,'C':C, 'perm':perm_idx, 'criteria':criteria, 'error':error_perc}
    param_str = exp_2_results_file_name(file_name_parameters)
    print param_str
    chosen_pts = max_model_change_active_learning(train_data, train_labels, budget, C, information_score = criteria, error_perc = error_perc, plot_progress = False, 
                                                  sample_random_in_no_change = True)
                                                  
    wrote_to = write_results_2_file(results_dir+'/'+param_str, data = train_data, labels = train_labels, al_pts = chosen_pts)
    print "wrote results to ",wrote_to
    
if __name__ == '__main__':
       
    #criterias = ['avg','actual','hyperplane']    
    criterias = ['avg'] 
    frac_train = 1
    num_perms = 3
    perms = [0,1]
    budget = 60
    error_perc = 0.1
    error_percs = [0.2]
    #error_percs = [0.2,0.3]
    Cs = [10,100]
    #datasets = ['wpbc','ionosphere','pima','wdbc']       
    #datasets = ['pima', 'heart','bupa','wpbc','ionosphere','wdbc','vote']           
    datasets = ['vote']           
    models = []
    for criteria in criterias:
        for C in Cs:
            for data in datasets:  
                models.append((C, data, criteria))   
                
    action = 'compute'    
    action = 'compare'
    #action = 'compute_bayes'
    
    exp_id = 'checking_properties'
    #exp_id = 'actual_model_change_on_uci'
    #exp_id = 'avg_model_change_on_uci'
    results_dir = 'results'+'/'+exp_id
    if(not os.path.exists(results_dir)): os.mkdir(results_dir)    
    
    if(action == 'compare'): 
        
        fixed = {'results_dir':results_dir, 'train_frac':frac_train, 'max_budget':budget, 'criteria':criteria, 'perms':perms}
        param = {'data':datasets, 'C':Cs, 'error_perc':error_percs}
        apply_to_combination(compare_al_to_sl_averaged_over_data_splits, param, fixed=fixed, multiproc=False)
        
    
    elif(action == 'compute'):    
    
        fixed = {'results_dir':results_dir, 'frac_train':frac_train, 'budget':budget, 'criteria':criteria}
        param = {'dataname':datasets, 'C':Cs, 'perm_idx':perms, 'error_perc':error_percs}
        apply_to_combination(max_model_active_learning_wrapper_for_proc, param, fixed=fixed, multiproc=False, seperateproc=True)        
    
    else:
        for model_idx, (C,data, criteria) in enumerate(models): 
            
            for perm_idx in range(num_perms):
                print "%d computing al criteria for "%model_idx, (C, data, perm_idx, criteria)
                        
                train_data, train_labels, test_data, test_labels = get_uci_data(data, frac_train, perm_idx = perm_idx)
                dim, num_train = train_data.shape
                additionals = {'criteria':criteria,'dataset':data, 'perm':perm_idx, 'split':frac_train}
                param_str = parameters_2_filename(C, None, None, dim, None, additional = additionals)        
               
                if(action == 'compute'):
                    
                    chosen_pts = max_model_change_active_learning(train_data, train_labels, budget, C, information_score = criteria,
                                                                  error_perc = error_perc, plot_progress = False)
                    write_results_2_file(results_dir+'/'+param_str, data = train_data, labels = train_labels, al_pts = chosen_pts)        
                
                    
                elif(action == 'compute_bayes'):
                    
                    budget = num_train
                    find_sl_bayes_error_budget(train_data, train_labels, test_data, test_labels, budget, C, gen_plot = True) 
         