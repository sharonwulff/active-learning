# -*- coding: utf-8 -*-
"""
Created on Mon Sep  3 17:34:31 2012

@author: pashosh
"""
from numpy.random import permutation
from numpy import arange, zeros, sqrt, sum, sort, sign, tile, argsort, min
from numpy.linalg import norm

def unittest_nearest_neighbor_wrt_sample_for_large_datasets():
    
    from numpy.random import rand
    from numpy import arange
    dim = 1    
    examples = 12
    data = arange(dim*examples).reshape(dim, examples)
    distance_mat = zeros((examples, examples))
    for i in range(examples):
        for j in range(i):
            distance_mat[i,j] = norm(data[:,i] - data[:,j])
            distance_mat[j,i] = norm(data[:,i] - data[:,j])
            
    sorted_k_arr = [1,3,5]
    labels = [0,1,2,0,1,2,0,1,2,0,1,2]
    samples_arr = [[0,1,2,3,4,5],[6,7,8,9,10,11]]
    
    infered_labels = nearest_neighbor_wrt_sample_for_large_datasets(distance_mat, labels, samples_arr, sorted_k_arr)
    print infered_labels
    print distance_mat
    print labels

def nearest_neighbor_wrt_sample_for_large_datasets(distance_matrix, labels, samples_arr, sorted_k_arr):
    
    num_pts, num_pts = distance_matrix.shape
    non_existing_label = min(labels) -10
    infered_labels = [non_existing_label*zeros((num_pts, len(sorted_k_arr))) for i in range(len(samples_arr))] #store the results for all points, all samples and all ks
    k_max = max(sorted_k_arr)
    num_samples = len(samples_arr)
    
    for idx in range(num_pts):
        #sort the indices of all points
        sorted_indices = argsort(distance_matrix[idx,:])
        k_closest_labels = [[] for i in range(num_samples)]
        num_found = 0
        #first check whether the idx is in one of the samples
        for sample_idx, sample_indices in enumerate(samples_arr):
            if idx in sample_indices:
                for k in range(k_max): k_closest_labels[sample_idx].append(labels[idx])
                num_found += k_max
                assert(majority(k_closest_labels[sample_idx]) == labels[idx])
        
        for closest_n_idx in sorted_indices:
            for sample_idx, sample_indices in enumerate(samples_arr):
                if(idx in sample_indices): continue
                if(closest_n_idx in sample_indices): 
                    k_closest_labels[sample_idx].append(labels[closest_n_idx])
                    num_found += 1
            if(num_found >= k_max*num_samples): 
                print "found %d matching points"%num_found
                break
        #infer the labels for all the samples and the interesting ks
        for sample_idx in range(num_samples):        
            for k_idx, k in enumerate(sorted_k_arr):
                infered_labels[sample_idx][idx,k_idx] = majority(k_closest_labels[sample_idx][:k])
    
    for sample_idx in range(num_samples):
        assert(min(infered_labels[sample_idx]) > non_existing_label)
    return infered_labels

def nearest_neighbor_on_a_budget(points, labels, budget, k):
    
    dim, num_pts = points.shape
    indices = permutation(arange(num_pts))
    sample = indices[:budget]
    
    return nearest_neighbor_wrt_sample(points, labels, sample, k)

def nearest_neighbor_wrt_sample(points, labels, sample_indices, k):
    
    print "nearest_neighbor_wrt_sample sample of size %d out of %d with %d neigbors"%(len(sample_indices), len(labels),k)
    dim, num_pts = points.shape
    sample_points = points[:, sample_indices]
    sample_labels = labels[sample_indices]    
    infered_labels = zeros(num_pts)

    for idx in range(num_pts):        
        point = points[:,idx]
        if(idx in sample_indices):
            infered_labels[idx] = labels[idx]
        else:
            k_closest_indices = k_nearest_points(sample_points, point, k)
            neighbors_labels = [sample_labels[i] for i in k_closest_indices]
            infered_labels[idx] = majority(neighbors_labels)
            
    return infered_labels

def nearest_neighbor_predictions(train_data, train_labels, test_data, k):
    
    print "nearest_neighbor_predictions"
    dim, num_pts_train = train_data.shape
    dim, num_pts_test = test_data.shape
    infered_labels = zeros(num_pts_test)

    for idx in range(num_pts_test):        
        point = test_data[:,idx]
        k_closest_indices = k_nearest_points(train_data, point, k)
        neighbors_labels = [train_labels[i] for i in k_closest_indices]
        infered_labels[idx] = majority(neighbors_labels)
            
    return infered_labels
 
def majority(labels):
    
    labels_vals = list(set(labels))
    maj_label_idx = 0
    maj_label_num = sum(labels == labels_vals[0])
    
    for i in range(len(labels_vals)):
        maj_label_num_i = sum(labels == labels_vals[i])
        if(maj_label_num_i > maj_label_num):
            maj_label_num = maj_label_num_i
            maj_label_idx = i
    return labels_vals[maj_label_idx]

def euclid_dist(point1, point2):
    return sqrt(sum((point1 - point2)**2.0))

def k_nearest_points_old(points, point, k):
    
    dim, num_pts = points.shape    
    k_closest_indices = []
    
    distances = [euclid_dist(points[:,i],point) for i in range(num_pts)]
    sorted_unique_distances = sort(list(set(distances)))
    for dist in sorted_unique_distances[:k]:
        for idx in range(num_pts):
            if dist == euclid_dist(point, points[:,idx]): k_closest_indices.append(idx)

    return k_closest_indices[:k]
    
def k_nearest_points(points, point, k):
    
    dim, num_pts = points.shape
    assert(dim == point.shape[0])
    point.shape = (dim, 1)    
    dup_point = tile(point, num_pts)
    dists = sqrt(((points - dup_point)**2.0).sum(0))
    sorted_indices = argsort(dists)
    return sorted_indices[:k]



if __name__ == '__main__':
    
    unittest_nearest_neighbor_wrt_sample_for_large_datasets()
    exit()
    from numpy.random import rand
    from numpy import array, ones
    dim = 50
    examples = 10000
    data = arange(dim*examples).reshape(dim, examples)
    k = 10
    
    for i in range(examples):
        point = data[:,i]
        indices = k_nearest_points_old(data, point, k)
        indices_new = k_nearest_points(data, point, k)
        assert((indices == indices_new).all())
