import defs
import sys
import os

from data.mnist.parse_mnist import get_mnist_data
from algorithms.plal.plal import pl_algorithm

from algorithms.nearest_neighbor import nearest_neighbor_wrt_sample
from utils.utils import *

from numpy.random import seed, permutation



def gen_random_sample(use_seed, num_examples, num_sample):
    
    seed(use_seed)
    perm = permutation(num_examples)
    return perm[:num_sample]

def compute_nn(exp_id, use_seed, epsilon, outputdir, k, plal_or_random = "plal"):
    
    data, labels = get_mnist_data()
    dim, num_pts = data.shape
    assert(num_pts == 60000)
    
    plal_results_file = '%s/mnist_train_%s_seed_%d_epsilon_%s'%(outputdir, exp_id, use_seed, str(epsilon))
   
    if(not os.path.exists(plal_results_file)):
        print "cant find file ", plal_results_file
        return -1
    
    [plal_query_points, plal_budget, plal_labels] = load_from_file(plal_results_file)
    outputfile = ""
    if(plal_or_random == "plal"):
        outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_k_%d.plal_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon),k)
        nn_predictions_on_plal_queries = nearest_neighbor_wrt_sample(data, labels, plal_query_points, k)        
        write_2_file(outputfile, nn_predictions_on_plal_queries)
        
    elif(plal_or_random == "random"): 
        random_sample = gen_random_sample(use_seed, num_pts, len(plal_query_points))
        outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_k_%d.random_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon),k)
        nn_predictions_on_random_queries = nearest_neighbor_wrt_sample(data, labels, random_sample, k)        
        write_2_file(outputfile, nn_predictions_on_random_queries)
    
    print "wrote %s predictions to %s"%(plal_or_random, outputfile)
    

def compute_queries(exp_id, use_seed, epsilon, outputdir):
    
    data, labels = get_mnist_data()    
    plal_results_file = '%s/mnist_train_%s_seed_%d_epsilon_%s'%(outputdir, exp_id, use_seed, str(epsilon))
    print plal_results_file
    
    plal_labels, plal_budget, plal_query_points = pl_algorithm(data, labels, epsilon, min_splitting = True)
    print "plal: need %d sample points "%len(plal_query_points)
    write_2_file(plal_results_file, [plal_query_points, len(plal_query_points), plal_labels])
    
   
if __name__ == '__main__':
     
    action = sys.argv[1]
    exp_id = sys.argv[2]
    use_seed = int(sys.argv[3])
    epsilon = float(sys.argv[4])
    outputdir = sys.argv[5]
    if(action == "compute_queries"):
        compute_queries(exp_id, seed, epsilon, outputdir)
    elif(action == "compute_nn"):
        k = int(sys.argv[6])
        plal_or_random = sys.argv[7]
        compute_nn(exp_id, use_seed, epsilon, outputdir, k, plal_or_random = plal_or_random)
    else:
        print "dont know action %s"%action
        
    
    
    
    