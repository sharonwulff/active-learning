import defs

from data.mnist.parse_mnist import get_mnist_data, get_mnist_labels
from utils.utils import *
from prepare import *
from numpy import zeros, argmax, mean, std, sign
from pylab import show, savefig

exp_id = fixed_parameters['exp_id']
outputdir = fixed_parameters['outputdir']


    
def collect_results():
    
    epsilons.sort()    

    #we store the best out of all ks
    results_plal = [[] for epsilon in epsilons]
    results_random = [[] for epsilon in epsilons]
    budgets = [[] for epsilon in epsilons]
    
    labels = get_mnist_labels()
    num_pts = len(labels)
    assert(num_pts == 60000)
    
    for eps_idx, epsilon in enumerate(epsilons):
        for use_seed in seeds:
            
            print epsilon, use_seed
            plal_results_file = '%s/mnist_train_%s_seed_%d_epsilon_%s'%(outputdir, exp_id, use_seed, str(epsilon))
   
            if(not os.path.exists(plal_results_file)):
                print "cant find file ", plal_results_file
                continue
    
            [plal_query_points, plal_budget, plal_labels] = load_from_file(plal_results_file)
            budgets[eps_idx].append(100.0*len(plal_query_points)/float(num_pts))
          
            plal_temp_res = zeros(len(ks))
            random_temp_res = zeros(len(ks))
                        
            for k_id, k in enumerate(ks):
                
                plal_predictions_outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_k_%d.plal_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon),k)
                random_predictions_outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_k_%d.random_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon),k)
                
                if(not os.path.exists(plal_predictions_outputfile)):
                    print "cant find file ", plal_predictions_outputfile
                else:
                    nn_predictions_on_plal_queries = load_from_file(plal_predictions_outputfile)
                    plal_accuracy = multi_label_accuracy(nn_predictions_on_plal_queries, labels)
                    plal_temp_res[k_id] = plal_accuracy
                    
                if(not os.path.exists(random_predictions_outputfile)):
                    print "cant find file ", random_predictions_outputfile
                else:
                    nn_predictions_on_randome_queries = load_from_file(random_predictions_outputfile)
                    random_accuracy = multi_label_accuracy(nn_predictions_on_randome_queries, labels)   
                    random_temp_res[k_id] = random_accuracy
        
            print plal_temp_res
            print random_temp_res
            
            results_plal[eps_idx].append(max(plal_temp_res))
            results_random[eps_idx].append(max(random_temp_res))
                        
    print results_plal
    print results_random
    print budgets
                        
    """
    #extract means and stds
    results_plal_avg = {(dataset_id, dim):[[mean(results_plal[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))], 
                                            [std(results_plal[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in results_plal.keys()}
    results_random_avg = {(dataset_id, dim):[[mean(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))], 
                          [std(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in results_random.keys()} 
                            
    budgets_avg = {(dataset_id, dim):[[mean(budgets[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))],
                                     [std(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in budgets.keys()}   
    
    return results_plal_avg, results_random_avg, budgets_avg

    """

def plot_results(dataset_id, dim, epsilons, results_plal, results_random, budgets):
    
    methods_2_colors = {'plal':'g', 'random':'b', 'budget':'r'}
    epsilons.sort()
    
    title = "data:%d dim %d accuracies and budget(percentage) vs. epsilon"%(dataset_id, dim)
    x_lim = epsilons[-1]+0.1
    y_lim = 110
    plot_with_error(epsilons, budgets[0], budgets[1], x_lim, y_lim, title, 'budget', color = methods_2_colors['budget'])
    plot_with_error(epsilons, results_plal[0], results_plal[1], x_lim, y_lim, title, 'plal', color = methods_2_colors['plal'])
    plot_with_error(epsilons, results_random[0], results_random[1], x_lim, y_lim, title, 'random', color = methods_2_colors['random'])
    plotfile = "%s/%s_dataset_%d_dim_%d_nn_accuracies_and_budget.pdf"%(outputdir, exp_id, dataset_id, dim)
    print plotfile
    savefig(plotfile, format='pdf')
    show()                   

def process_results():
    
    results_plal_avg, results_random_avg, budgets_avg = collect_results()
    sorted_datasets = []
    for (dataset_id, dim) in results_plal_avg.keys():
        sorted_datasets.append([(dataset_id, dim), score_a_result(results_plal_avg[(dataset_id, dim)][0], results_random_avg[(dataset_id, dim)][0], budgets_avg[(dataset_id, dim)][0])])
    
    def my_cmp(i1,i2):
        return int(sign(i2[1] - i1[1]))
    sorted_datasets.sort(cmp = my_cmp)
    
    for (dataset_id, dim), score in sorted_datasets:
        
        print score, (dataset_id, dim)
        plot_results(dataset_id, dim, epsilons, results_plal_avg[(dataset_id, dim)], results_random_avg[(dataset_id, dim)], budgets_avg[(dataset_id, dim)])
        
                 
if __name__ == '__main__':
    
    collect_results()