import defs
import sys
import os

from data.mnist.parse_mnist import get_mnist_data
from utils.utils import *
from plal.data_pl_properties import pl_properties
from numpy import mean, std, array
from numpy.random import permutation
import pylab 



def plot_pl_properties():
    
    data, labels = get_mnist_data(half = True)
    print data.shape
    dim, num_pts = data.shape
    #sample some examples and compute pl for them 
    num_sample = 10000
    perm = permutation(num_pts)
    sample_data = data[:,perm[:num_sample]]    
    sample_labels = array(labels)[perm[:num_sample]]
    
    lambda_range, pl_lambda = pl_properties(sample_data, sample_labels, granularity = 100, pl_def = "truncated", truncate_lambda = True) #, max_dist = 4.0, min_dist = 0)
    
    fig = pylab.figure()
    pylab.plot(lambda_range, pl_lambda)
    pylab.ylim(0,110)
    use_title = "MNIST data thi of lambda with %d examples"%num_sample    
    pylab.title(use_title)
    
    plot_file = 'pl_MNIST_%d.pdf'%num_sample
    pylab.savefig(plot_file, format='pdf')
    #pylab.show()
if __name__ == '__main__':
    
    plot_pl_properties()
    
    