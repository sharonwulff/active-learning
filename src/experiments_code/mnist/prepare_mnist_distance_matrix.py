import defs
import os
import sys

from data.mnist.parse_mnist import get_mnist_data, distance_matrix_file
from numpy import zeros, min
from utils.utils import write_2_file, load_from_file
from numpy.linalg import norm
from numpy import ones

def prepare_mnist_distance_matrix():
    
    num_pts = 60000

    run_filename = "submit_compute_distance.sh"
    run_file = open(run_filename,'w')
    os.chmod(run_filename, 0755)        
        
    num_indices_per_job = 60000/20
    num_jobs = 0
    for i in range(20):
        for j in range(i+1):
            curri=(num_indices_per_job*i, num_indices_per_job*(i+1))
            currj=(num_indices_per_job*j, num_indices_per_job*(j+1))
            print curri, currj
            
            run_file.write("bsub -W 1:00 python experiments_code/mnist/prepare_mnist_distance_matrix.py run %d %d %d %d\n"%\
                                   (num_indices_per_job*i, num_indices_per_job*(i+1), num_indices_per_job*j, num_indices_per_job*(j+1)))
            num_jobs += 1
           
            
    print "going to run %d jobs"%num_jobs

def run(i1,i2,j1,j2):
    
    data, labels = get_mnist_data()
    dim, num_pts = data.shape
    assert(num_pts == 60000)
    
    temp_mat = -1*ones((i2-i1, j2-j1))
    
    for i in range(i1, i2):
        for j in range(j1,j2):
            temp_mat[i-i1,j-j1] = norm(data[:,i] - data[:,j])
    
    outputfile = "distance_matrix_%d_%d_%d_%d.part"%(i1,i2,j1,j2)    
    write_2_file(outputfile, temp_mat)
   
if __name__ == '__main__':
    
    action = sys.argv[1]
    
    if(action == "prepare"):
        prepare_mnist_distance_matrix()
    elif(action == "run"):
        i1 = int(sys.argv[2])
        i2 = int(sys.argv[3])
        j1 = int(sys.argv[4])
        j2 = int(sys.argv[5])
        run(i1, i2, j1, j2)
    else:
        print "dont know action %s"%action
    