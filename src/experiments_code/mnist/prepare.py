import defs
import os

base_dir = '..'

seeds = [0,1,2,3,4]
epsilons = [.01,.025,.05,.075,.1,.125,.15,.175,.2,.225,.25,.275,.3,.325,.35,.375,.4]
ks = [1,5,10]
    
fixed_parameters = {'exp_id':'test1',
                    'outputdir':base_dir + '/results/mnist/test1'
                    }
action = "compute_nn"

def prepare_experiment():
    
    exp_id = fixed_parameters['exp_id']
    outputdir = fixed_parameters['outputdir']
    
    run_filename = "submit_mnist_%s.sh"%(exp_id)
    run_file = open(run_filename,'w')
    os.chmod(run_filename, 0755)        
        
    num_jobs = 0
    for seed in seeds:
        for epsilon in epsilons:
            
            if(action == "compute_queries"):
                run_file.write("bsub -W 8:00 python experiments_code/mnist/main.py %s %s %d %f %s\n"%\
                                    (action, exp_id, seed, epsilon, outputdir))
                num_jobs += 1
            elif(action == "compute_nn"):
                
                for k in ks:
                    for choice in ["random", "plal"]:
                        run_file.write("bsub -W 8:00 python experiments_code/mnist/main.py %s %s %d %f %s %d %s\n"%\
                                    (action, exp_id, seed, epsilon, outputdir, k, choice))
                        num_jobs += 1
                   
            
    print "going to run %d jobs"%num_jobs
    
if __name__ == '__main__':
    
    prepare_experiment()
    