# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""

from plal.plal import pl_algorithm
from plal.nearest_neighbor import nearest_neighbor_on_a_budget, nearest_neighbor_wrt_sample
from utils.gen_data import gen_2_class_guassians
from utils.utils import *
from test_plal_real_data import run_plal_on_uci, run_nn_on_uci
from numpy import sign, array, matrix, ones, zeros, nonzero, ceil, mean, std
from pylab import show, savefig
from test_plal_with_nn_prediction import compute_and_store_plal_results_on_uci
import os

epsilons = [0.1,0.15,0.2,0.25,0.275,0.3,0.35]
ks = [10, 15, 25]
datasets = ['abalone', 'yeast', 'chess'] 
repetitions = 10

methods_2_colors = {10:'b',15:'y',25:'r'}

def compute_and_store_predictions_with_nn(plal_results, ks, outputfile, method = 'plal'):
    
    ks=[15]
    print "compute_and_store_predictions_with_nn ", method
    results = {}
    for (epsilon, dataset), plal_model_queries in plal_results.iteritems():
        
        if(epsilon != 0.35 or dataset != 'abalone'): continue
        print epsilon, dataset
        data, labels, dummy1, dummy2 = get_uci_data(dataset, 1.0, perm_idx = 0)
        num_repetitions = len(plal_model_queries)
        for k in ks:
            predictions = []
            for repetition, plal_queries in enumerate(plal_results[(epsilon, dataset)]):
                
                print repetition, plal_queries[:10]
                if(method == 'plal'):
                    curr_labels = nearest_neighbor_wrt_sample(data, labels, plal_queries, k)
                    predictions.append(curr_labels)
                elif(method == 'nn'):
                    budget = len(plal_queries)
                    curr_labels = nearest_neighbor_on_a_budget(data, labels, budget, k)
                    predictions.append(curr_labels)
                else:
                    print "unknow method!"
                    exit()
                    
            print len(predictions)
            results[(epsilon, dataset, k)] = predictions
            write_2_file(outputfile, results)


def compare_plal_and_nn_results(plal_results, plal_predictions, nn_predictions, 
                                output_dir = None, show_budget = True, show_accuracies = True):
    print "plal results ",len(plal_results)
    for k in plal_results.keys(): print k                                
    print "plal predictions", len(plal_predictions)
    for k in plal_predictions.keys(): print k
    print "nn", len(nn_predictions)
    for k in nn_predictions.keys(): print k
    exit()
    #get the parameters of the runs
    datasets = list(set([key[1] for key in plal_predictions.keys()]))
    epsilons = list(set([key[0] for key in plal_predictions.keys()]))    
    ks = list(set([key[2] for key in plal_predictions.keys()]))
    epsilons.sort()    
    ks.sort()
   
    for dataset in datasets:
        
        print dataset
        #if dataset not in ['pima','bupa']: continue
        data, labels, dummy1, dummy2 = get_uci_data(dataset, 1.0, perm_idx = 0)  
        dim, num_examples = data.shape
        data_str = '%s dim: %d examples %d'%(dataset, dim, num_examples)
        
        accuracies_plal = {k:zeros(len(epsilons)) for k in ks}
        std_plal = {k:zeros(len(epsilons)) for k in ks}
        accuracies_nn = {k:zeros(len(epsilons)) for k in ks}
        std_nn = {k:zeros(len(epsilons)) for k in ks}        
        budgets = zeros(len(epsilons))  
        std_budgets = zeros(len(epsilons)) 
        
        #get the accuracies and std
        for k in ks:
            for epsilon_idx, epsilon in enumerate(epsilons):
                print "k, epsilon ",k, epsilon

                if(not (epsilon, dataset, k) in plal_predictions or not (epsilon, dataset, k) in nn_predictions):
                    print "configuration is missing "
                    continue
                
                num_repetitions = len(plal_predictions[(epsilon, dataset, k)])
                curr_plal_acc = []
                curr_nn_acc = []
                
                for repetition, curr_plal_predictions in enumerate(plal_predictions[(epsilon, dataset,k)]):
                    
                    curr_nn_predictions = nn_predictions[(epsilon, dataset,k)][repetition]
                    plal_acc = multi_label_accuracy(curr_plal_predictions, labels)
                    nn_acc = multi_label_accuracy(curr_nn_predictions, labels)
                    curr_plal_acc.append(plal_acc)
                    curr_nn_acc.append(nn_acc)
                    
                print curr_plal_acc, curr_nn_acc
                accuracies_plal[k][epsilon_idx] = mean(curr_plal_acc)
                std_plal[k][epsilon_idx] = std(curr_plal_acc)
                accuracies_nn[k][epsilon_idx] = mean(curr_nn_acc)
                std_nn[k][epsilon_idx] = std(curr_nn_acc)
                #print "yo"
                
        #get the budgets 
        for epsilon_idx, epsilon in enumerate(epsilons):
            budgets[epsilon_idx] = (100.0/float(num_examples))*mean([len(queries) for queries in plal_results[(epsilon, dataset)]])
            std_budgets[epsilon_idx] = (100.0/float(num_examples))*std([len(queries) for queries in plal_results[(epsilon, dataset)]])
        
        #compute and store plots
        if(show_budget):    
            #plot budget vs. epsilon
            title = "budget(percentage) vs. epsilon %s"%data_str  
            x_lim = (0, epsilons[-1]+0.1)
            y_lim = (0,110)
            plot_with_error(epsilons, budgets, std_budgets, x_lim, y_lim, title, None)
            filename = "%s_budget_vs_epsilon.pdf"%dataset            
            if(output_dir): savefig(output_dir + filename)
            show()                   
        
        if(show_accuracies):
            #first plot accuracy vs. epsilon
            title = "accuracy vs. epsilon %s"%data_str  
            x_lim = (0, epsilons[-1]+0.1)
            y_lim = (0,110)
            for k in ks: 
                method = '%d-plal'%k
                plot_with_error(epsilons, accuracies_plal[k], std_plal[k], x_lim, y_lim, title, method, 
                                color = methods_2_colors[k], icon = 'o')                
                method = '%d-nn'%k
                plot_with_error(epsilons, accuracies_nn[k], std_nn[k], x_lim, y_lim, title, method, 
                                color = methods_2_colors[k], icon = '*')
            
            filename = "%s_accuracy_comparison_plal_nn.pdf"%dataset            
            if(output_dir): savefig(output_dir + filename)
            show()
    
     
if __name__ == '__main__':
    
    #assume that we run the code from plal dir
    base_results_dir = 'results'
    #exp_id = 'multiplelabel'
    exp_id = 'debug'    
    results_dir = base_results_dir + '/'+ exp_id +'/'
    #create a directory for the results in case it doesnt exist    
    if(not os.path.exists(results_dir)): os.mkdir(results_dir)    
    
    plal_models = []
    for dataset in datasets:
        for epsilon in epsilons:
            plal_models.append((epsilon, dataset))    
                
     #store the plal results here
    outputfile_plal = results_dir + 'plal_%s.pkl'%exp_id
    #compute_and_store_plal_results_on_uci(plal_models, outputfile_plal)
    plal_results = load_from_file(outputfile_plal)    
    
    #store the nn results here 
    outputfile_plal_predictions = results_dir + 'plal_predictions_%s.pkl'%exp_id    
    outputfile_nn_predictions = results_dir + 'nn_predictions_%s.pkl'%exp_id
    
    #compute plal with nn and nn predictions 
    compute_and_store_predictions_with_nn(plal_results, ks, outputfile_plal_predictions, method = 'plal')
    compute_and_store_predictions_with_nn(plal_results, ks, outputfile_nn_predictions, method = 'nn')
    
    exit()
    plal_predictions = load_from_file(outputfile_plal_predictions)
    nn_predictions = load_from_file(outputfile_nn_predictions)
    
    
    # collect the results of nn and plal 
    compare_plal_and_nn_results(plal_results, plal_predictions, nn_predictions, 
                                output_dir = results_dir,show_budget = True, show_accuracies = True)