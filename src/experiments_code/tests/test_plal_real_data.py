# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""

from plal.plal import pl_algorithm
from plal.nearest_neighbor import nearest_neighbor_on_a_budget
from utils.gen_data import gen_2_class_guassians
from utils.utils import *
from numpy import sign, array, matrix, ones, zeros, nonzero, ceil, mean, std
from pylab import show

epsilons = [0.1,0.15,0.2,0.25,0.3]
ks = [1,3,5,10]
datasets = ['pima','bupa','heart','vote'] 
repetitions = 5

methods_2_colors = {'plal':'r','1nn':'g','3nn':'b','5nn':'y','10nn':'m'}

def run_plal_on_uci(epsilon, dataset, perm_idx = 0):
    
    frac_train = 1.0
    data, labels, dummy1, dummy2 = get_uci_data(dataset, frac_train, perm_idx = perm_idx)  
    labels_plal, plal_budget, query_points = pl_algorithm(data, labels, epsilon)
    accuracy = 100.0*sum(sign(labels_plal) == sign(labels))/float(len(labels))        
    return plal_budget, accuracy, query_points

def run_nn_on_uci(budget, dataset, k = 10):
    
    frac_train = 1.0
    data, labels, dummy1, dummy2 = get_uci_data(dataset, frac_train, perm_idx = 0) 
    labels_nn = nearest_neighbor_on_a_budget(data, labels, budget, k)
    accuracy = 100.0*sum(sign(labels_nn) == sign(labels))/float(len(labels))    
    return accuracy    
    
def run_plal_and_nn_on_uci(models, outputfile = None):
    
    results = {}
    for model_idx, (epsilon, k, dataset) in enumerate(models):
        
        accuracies_plal = []
        accuracies_nn = []
        budgets = []
        
        print "%d comparing the following parameters "%model_idx, (epsilon, k, dataset)
        for repetition in range(repetitions):
            plal_budget, plal_accuracy, plal_query_points = run_plal_on_uci(epsilon, dataset)
            nn_accuracy = run_nn_on_uci(plal_budget, dataset, k = k)
            accuracies_plal.append(plal_accuracy)
            accuracies_nn.append(nn_accuracy)
            budgets.append(plal_budget)
        
        print (mean(accuracies_plal), std(accuracies_plal),
                                          mean(accuracies_nn), std(accuracies_nn),
                                          mean(budgets), std(budgets))
            
        results[(epsilon, k, dataset)] = (mean(accuracies_plal), std(accuracies_plal),
                                          mean(accuracies_nn), std(accuracies_nn),
                                          mean(budgets), std(budgets))
        
        #write partial results in order not to lose it
        if(outputfile != None): write_2_file(outputfile, results)
                                  
    return results

def compare_plal_and_nn_on_uci(res_dict, show_accuracies = False, show_budget = False):
    
    results_per_dataset = {}
    for (epsilon, k, dataset), model_results in res_dict.iteritems():
        if dataset not in results_per_dataset: results_per_dataset[dataset] = {}
        results_per_dataset[dataset][(epsilon, k)] = model_results
    
    for dataset, model_dict in results_per_dataset.iteritems():
        
        dim, num_examples = get_uci_data_shape(dataset)
        data_str = '%s dim: %d examples %d'%(dataset, dim, num_examples)
        epsilons = list(set([key[0] for key in model_dict.keys()]))
        epsilons.sort()
        ks = list(set([key[1] for key in model_dict.keys()]))
        
        plal_accuracies = zeros(len(epsilons))
        plal_stds = zeros(len(epsilons))
        nn_accuracies = {}
        nn_stds = {}
        for k in ks:
            nn_accuracies[k] = zeros(len(epsilons))
            nn_stds[k] = zeros(len(epsilons))
        budgets = zeros(len(epsilons))
        budgets_std = zeros(len(epsilons))
        
        for (epsilon, k), results in model_dict.iteritems():
            
            (plal_acc, plal_std, nn_acc, nn_std, budget, budget_std) = results
            idx = epsilons.index(epsilon)
            plal_accuracies[idx] += plal_acc/float(len(ks))
            plal_stds[idx] += plal_std/float(len(ks))
            nn_accuracies[k][idx] = nn_acc
            nn_stds[k][idx] = nn_std
            budgets[idx] += (100.0*budget)/(float(len(ks))*num_examples)
            budgets_std[idx] += (100.0*budget_std)/(float(len(ks))*num_examples)
        
        if(show_budget):    
            #plot budget vs. epsilon
            title = "budget(percentage) vs. epsilon %s"%data_str  
            x_lim = (0, epsilons[-1]+0.1)
            y_lim = (0,110)
            plot_with_error(epsilons, budgets, budgets_std, x_lim, y_lim, title, 'r', None)
            show()                   
        
        if(show_accuracies):
            #first plot accuracy vs. epsilon
            title = "accuracy vs. epsilon %s"%data_str  
            x_lim = (0, epsilons[-1]+0.1)
            y_lim = (0,110)
            plot_with_error(epsilons, plal_accuracies, plal_stds, x_lim, y_lim, title, methods_2_colors['plal'], 'plal')
            for k, nn_accs in nn_accuracies.iteritems(): 
                method = '%dnn'%k
                plot_with_error(epsilons, nn_accs, nn_stds[k], x_lim, y_lim, title, methods_2_colors[method], method)
        
            show()
        
if __name__ == '__main__':
    
    models = []
    for dataset in datasets:
        for k in ks:
            for epsilon in epsilons:
                models.append((epsilon, k, dataset))    
                
     #store the results here
    outputfile = 'plal_on_uci.pkl'
    
    # use this to compute the results
    #results = run_plal_and_nn_on_uci(models, outputfile = outputfile)
    
    #load the results
    resdir = 'results'
    results = load_from_file(resdir+'/'+outputfile)
    compare_plal_and_nn_on_uci(results, show_accuracies = True, show_budget = True)
    #write_2_file(outputfile, results)