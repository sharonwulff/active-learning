# -*- coding: utf-8 -*-
"""

@author: sharon
"""
from plal.plal import pl_algorithm
from plal.nearest_neighbor import nearest_neighbor_wrt_sample
from utils.utils import *


def compute_and_store_plal_queries(data, labels, epsilon, outputfile):
    
    plal_labels, plal_budget, query_points = pl_algorithm(data, labels, epsilon)
    write_2_file(outputfile, query_points, plal_budget)

def compute_and_store_nn_predictions(data, labels, query_points, k, outputfile):
    
    nn_predictions = nearest_neighbor_wrt_sample(data, labels, query_points, k)
    write_2_file(outputfile, nn_predictions)
    
