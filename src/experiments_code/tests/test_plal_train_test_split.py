# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon & ruth
"""

from plal.plal import pl_algorithm
#from plal.nearest_neighbor import nearest_neighbor_on_a_budget, nearest_neighbor_wrt_sample
#from utils.gen_data import gen_2_class_guassians
from utils.utils import *
#from test_plal_real_data import run_plal_on_uci, run_nn_on_uci
from numpy import sign, array, matrix, ones, zeros, nonzero, ceil, mean, std
from pylab import show, savefig
#from test_plal_with_nn_prediction import compute_and_store_plal_results_on_uci
import os

methods_2_colors = {'standard':'b', 'query':'y', 'budget':'r', 'indices': 'g'}

def plot_results(results_per_dataset, epsilons):
    
    for dataset, results in results_per_dataset.iteritems():
        title = "error_rates vs. epsilon %s"%dataset
        for method, lists_to_plot in results.iteritems():
            error_rates = lists_to_plot[0]
            variances = lists_to_plot[1]
            x_lim = (0, epsilons[-1]+0.1)
            y_lim = (0, 1.1)
            plot_with_error(epsilons, error_rates, variances, x_lim, y_lim, title, method, methods_2_colors[method])
        show()


def compute_error_rate(learned_labels, true_labels):
    #this works only if the labels are in {-1,1}
    assert(len(learned_labels) == len(true_labels))
    error_rate = 1- (sum(abs(learned_labels + true_labels)))/(2.0*len(learned_labels))
    return error_rate


def get_file_name(label_method, dataset, perm_idx, epsilon, repetition, target_dir, action):
    file_name = target_dir + '/' + action + '_' + label_method + '_data_' + dataset + '_perm_' + str(perm_idx) + '_epsilon_' + str(epsilon) + '_rep_' + str(repetition) + '.pkl'
    return file_name


def run_svm(train_data, train_labels, test_data, rate):
    
    train_dim, train_num_points = train_data.shape
    test_dim, test_num_points = test_data.shape
    assert(train_dim == test_dim)
    predicted_labels = ones(test_num_points)

    if abs(sum(train_labels)) == len(train_labels):
        predicted_labels *= train_labels[0]
        return predicted_labels

    else: 
        svm = train_with_svm(train_data, train_labels, rate)
        svm_prediction = predict(train_data, test_data, svm)
        predicted_labels = sign(svm_prediction)
        return predicted_labels


def run_query(label_method, train_data, train_labels, epsilon, outputfile):
      
    if label_method == 'plal':          
        labels_plal, plal_budget, query_points = pl_algorithm(train_data, train_labels, epsilon)
        results = [labels_plal, plal_budget, query_points]        
        write_2_file(outputfile, results) #TODO maybe move this to fct experiment     
    
    if label_method == 'nn':
        print "nn"


def run_predict(input_file, output_file, train_data, train_labels, test_data, test_labels, svm_rate):
    query_results = load_from_file(input_file)
    query_labels = query_results[0]
    query_budget =  query_results[1]
    query_indices =  query_results[2]
    queried_fraction = float(query_budget) / len(train_labels)
    #learn classifiers with both queried and perfect labels andmake prediction on test data
    predicted_labels = run_svm(train_data, train_labels, test_data, svm_rate)
    query_predicted_labels = run_svm(train_data, query_labels, test_data, svm_rate)
    budget_predicted_labels = run_svm(train_data[:,:query_budget], train_labels[:query_budget], test_data, svm_rate)
    indices_predicted_labels = run_svm(train_data[:, query_indices], train_labels[query_indices], test_data, svm_rate)
    labels_to_compare = [test_labels, predicted_labels, query_predicted_labels, budget_predicted_labels, indices_predicted_labels, queried_fraction]      
    #store the prediction
    write_2_file(output_file, labels_to_compare)                    


#def run_compare(label_methods, datasets, epsilons, permutations, repetitions, results_dir, output_file):
#    for label_method in label_methods:
#        comparisons = {}
#        print "compare"
#        for dataset in datasets:
#            for epsilon in epsilons:
#                average_error_rate = 0
#                average_query_error_rate = 0
#                average_budget_error_rate = 0
#                average_indices_error_rate = 0
#                average_queried_fraction = 0
#                for perm_idx in permutations:
#                    for repetition in range(repetitions):
#                        input_file = get_file_name(label_method, dataset, perm_idx, epsilon, repetition, results_dir, 'predict')
#                        predict_results = load_from_file(input_file)
#                        #compute error rates
#                        true_labels = predict_results[0]
#                        predicted_labels = predict_results[1]
#                        query_predicted_labels = predict_results[2]
#                        budget_predicted_labels = predict_results[3]
#                        indices_predicted_labels = predict_results[4]                        
#                        queried_fraction = predict_results[5]
#                        error_rate = compute_error_rate(predicted_labels, true_labels)
#                        query_error_rate = compute_error_rate(query_predicted_labels, true_labels)
#                        budget_error_rate = compute_error_rate(budget_predicted_labels, true_labels)
#                        indices_error_rate = compute_error_rate(indices_predicted_labels, true_labels)
#                        average_error_rate += error_rate / (repetitions * len(permutations))
#                        average_query_error_rate += query_error_rate / (repetitions * len(permutations))
#                        average_budget_error_rate += budget_error_rate / (repetitions * len(permutations))
#                        average_indices_error_rate += indices_error_rate / (repetitions * len(permutations))
#                        average_queried_fraction += queried_fraction / (repetitions * len(permutations))
#                #store results for this eps and data
#                comparisons[(dataset,epsilon)] = (average_error_rate, average_query_error_rate, average_budget_error_rate, average_indices_error_rate, average_queried_fraction)
#                          
#        write_2_file(output_file, comparisons)


def run_compare_and_plot(label_methods, datasets, epsilons, permutations, repetitions, results_dir, output_file):
    for label_method in label_methods:
        comparisons = {}
        results_per_dataset = {}
        print "compare"
        for dataset in datasets:
            if dataset not in results_per_dataset: results_per_dataset[dataset] = {}
            average_error_rates = []
            average_query_error_rates = []
            average_budget_error_rates = []
            average_indices_error_rates = []
            error_rates_variances = []
            query_error_rates_variances = []
            budget_error_rates_variances = []
            indices_error_rates_variances = []
            for epsilon in epsilons:
                error_rate = []
                query_error_rate = []
                budget_error_rate = []
                indices_error_rate = []
                average_queried_fraction = 0
                for perm_idx in permutations:
                    for repetition in range(repetitions):
                        input_file = get_file_name(label_method, dataset, perm_idx, epsilon, repetition, results_dir, 'predict')
                        predict_results = load_from_file(input_file)
                        #compute error rates
                        true_labels = predict_results[0]
                        predicted_labels = predict_results[1]
                        query_predicted_labels = predict_results[2]
                        budget_predicted_labels = predict_results[3]
                        indices_predicted_labels = predict_results[4]                        
                        queried_fraction = predict_results[5]
                        #collect error rates
                        error_rate.append(compute_error_rate(predicted_labels, true_labels))
                        query_error_rate.append(compute_error_rate(query_predicted_labels, true_labels))
                        budget_error_rate.append(compute_error_rate(budget_predicted_labels, true_labels))
                        indices_error_rate.append(compute_error_rate(indices_predicted_labels, true_labels))
                        average_queried_fraction += queried_fraction / (repetitions * len(permutations))
                #collect results for this eps
                average_error_rates.append(mean(error_rate))
                average_query_error_rates.append(mean(query_error_rate))
                average_budget_error_rates.append(mean(budget_error_rate))
                average_indices_error_rates.append(mean(indices_error_rate))
                error_rates_variances.append(std(error_rate))
                query_error_rates_variances.append(std(query_error_rate))
                budget_error_rates_variances.append(std(budget_error_rate))
                indices_error_rates_variances.append(std(indices_error_rate))
                comparisons[(dataset,epsilon)] = (mean(error_rate), mean(query_error_rate), mean(budget_error_rate), mean(indices_error_rate), average_queried_fraction)

            results_per_dataset[dataset]['standard'] = (average_error_rates, error_rates_variances)
            results_per_dataset[dataset]['query'] = (average_query_error_rates, query_error_rates_variances)
            results_per_dataset[dataset]['budget'] = (average_budget_error_rates, budget_error_rates_variances)
            results_per_dataset[dataset]['indices'] = (average_indices_error_rates, indices_error_rates_variances)

        #store results                  
        results = (comparisons, results_per_dataset)
        write_2_file(output_file, results)
        #plot
        plot_results(results_per_dataset, epsilons)
       

def experiment(action):
    
    #assume that we run the code from plal dir
    base_results_dir = 'results'
    exp_id = 'test_train_split'
    results_dir = base_results_dir + '/'+ exp_id
    #create a directory for the results in case it doesnt exist    
    if(not os.path.exists(results_dir)): os.mkdir(results_dir)
    
    epsilons = [0.1,0.15,0.2,0.25,0.275,0.3,0.35]
    epsilons.sort()
#    datasets = ['skin', 'pima', 'bupa'] 
    datasets = ['skin'] 
#    permutations = [0,1,2,3,4,5,6,7,8,9,10,11]
    permutations = [0] 
    repetitions = 3
 
    prediction_methods = ['nn','svm']
    label_methods = ['plal']# maybe add or replace by 'nn'  
    ks = [10, 15, 25]

    split_ratio = 0.6
    svm_rate = 1
    
    #TODO: decide which things we want to try out
    configurations = []
    for dataset in datasets:
       for perm_idx in permutations:
           for epsilon in epsilons:
               for repetition in range(repetitions):
                   configurations.append((epsilon, dataset, perm_idx, repetition))         

    if(action == "compare"):
        for label_method in label_methods:
            output_file = results_dir + '/' + 'compare_' + label_method
            run_compare_and_plot(label_methods, datasets, epsilons, permutations, repetitions, results_dir, output_file)        
    else:
        #run over all configurations, get the train data, call plal, store result
        for (epsilon, dataset, perm_idx, repetition) in configurations:
            print (dataset, perm_idx)  
            train_data, train_labels, test_data, test_labels = get_uci_data(dataset, split_ratio, perm_idx = perm_idx)                
            if(action == "query"):
                for label_method in label_methods:
                    output_file = get_file_name(label_method, dataset, perm_idx, epsilon, repetition, results_dir, 'query')
                    run_query(label_method, train_data, train_labels, epsilon, output_file)
                    #exit()
            elif(action == "predict"):
                for label_method in label_methods:
                    #get filenames (input with plal labels)
                    input_file = get_file_name(label_method, dataset, perm_idx, epsilon, repetition, results_dir, 'query')
                    output_file = get_file_name(label_method, dataset, perm_idx, epsilon, repetition, results_dir, 'predict')
                    run_predict(input_file, output_file, train_data, train_labels, test_data, test_labels, svm_rate)
            else:
                 print "unknown action ", action
                 exit()
        


#---------------------------------------------------------------------------     
if __name__ == '__main__':
    
    action = "compare"
    experiment(action)
        