# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""

from plal.plal import pl_algorithm
from plal.nearest_neighbor import nearest_neighbor_on_a_budget, nearest_neighbor_wrt_sample
from utils.gen_data import gen_2_class_guassians
from utils.utils import *
from test_plal_real_data import run_plal_on_uci, run_nn_on_uci
from numpy import sign, array, matrix, ones, zeros, nonzero, ceil, mean, std
from pylab import show, savefig

epsilons = [0.2]
ks = [15]
#datasets = ['pima','bupa','heart','vote'] 
datasets = ['chess'] 
repetitions = 5

methods_2_colors = {1:'r',5:'b',10:'y',15:'g'}



def compute_and_store_plal_results_on_uci(models, outputfile, perm_idx = 0):
    
    results = {}
    for model_idx, (epsilon, dataset) in enumerate(models):
        
        results[(epsilon, dataset)] = []
        
        for repetition in range(repetitions):
            
            plal_budget, accuracy, query_points = run_plal_on_uci(epsilon, dataset, perm_idx = perm_idx)
            results[(epsilon, dataset)].append(query_points)
        
        write_2_file(outputfile, results)

def compute_and_store_plal_predictions_with_nn(plal_results, ks, outputfile):
    
    results = {}
    for (epsilon, dataset), plal_model_queries in plal_results.iteritems():
        
        data, labels, dummy1, dummy2 = get_uci_data(dataset, 1.0, perm_idx = 0)
        num_repetitions = len(plal_model_queries)
        for k in ks:
            plal_accuracy = []
            for repetition, queries in enumerate(plal_results[(epsilon, dataset)]):
                print repetition
                plal_nn_labels = nearest_neighbor_wrt_sample(data, labels, queries, k)
                curr_acc = 100.0*sum(sign(plal_nn_labels) == sign(labels))/float(len(labels))
                plal_accuracy.append(curr_acc)            
    
            results[(epsilon, dataset, k)] = (mean(plal_accuracy), std(plal_accuracy))
            write_2_file(outputfile_nn, nn_results)

def compute_and_store_nn_results_with_plal_budget_on_uci(plal_results, ks, outputfile_nn):
    
    nn_results = {}
    for (epsilon, dataset), plal_model_results in plal_results.iteritems():
        
        num_repetitions = len(plal_model_results)
        for k in ks:
            nn_accuracy = []
            for repetition in range(repetitions):
                budget = len(plal_model_results[repetition])
                accuracy = run_nn_on_uci(budget, dataset, k = k)
                nn_accuracy.append(accuracy)
                
            nn_results[(epsilon, dataset, k)] = (mean(nn_accuracy), std(nn_accuracy))
            write_2_file(outputfile_nn, nn_results)

def compare_plal_and_nn_results(plal_results, nn_results, show_budget = True, show_accuracies = True, 
                                results_contain_accuracies = False):
    
    datasets = list(set([key[1] for key in plal_results.keys()]))
    epsilons = list(set([key[0] for key in plal_results.keys()]))
    epsilons.sort()
    ks = list(set([key[2] for key in nn_results.keys()]))
    ks.sort()
   
    for dataset in datasets:

        #if dataset not in ['pima','bupa']: continue
        data, labels, dummy1, dummy2 = get_uci_data(dataset, 1.0, perm_idx = 0)  
        dim, num_examples = data.shape
        data_str = '%s dim: %d examples %d'%(dataset, dim, num_examples)
        
        accuracies_plal = {k:zeros(len(epsilons)) for k in ks}
        std_plal = {k:zeros(len(epsilons)) for k in ks}
        accuracies_nn = {k:zeros(len(epsilons)) for k in ks}
        std_nn = {k:zeros(len(epsilons)) for k in ks}        
        budgets = zeros(len(epsilons))  
        std_budgets = zeros(len(epsilons)) 
        
        #get the accuracies and std
        for k in ks:
            for epsilon_idx, epsilon in enumerate(epsilons):
                print "k, epsilon ",k, epsilon
                if((not (epsilon, dataset) in plal_results and not (epsilon, dataset, k) in plal_results) or 
                not (epsilon, dataset, k) in nn_results):
                    print "configuration is missing ", (epsilon, dataset, k)
                
                num_repetitions = len(plal_results[(epsilon, dataset)])
                curr_acc = []
                
                if(results_contain_accuracies):
                    for repetition, plal_acc in enumerate(plal_results[(epsilon, dataset, k)]):
                        curr_acc.append(plal_acc)
                else:
                    for repetition, queries in enumerate(plal_results[(epsilon, dataset)]):
                        print repetition
                        plal_nn_labels = nearest_neighbor_wrt_sample(data, labels, queries, k)
                        plal_acc = 100.0*sum(sign(plal_nn_labels) == sign(labels))/float(len(labels))
                        curr_acc.append(plal_acc)
                    
                accuracies_plal[k][epsilon_idx] = mean(curr_acc)
                std_plal[k][epsilon_idx] = std(curr_acc)
                accuracies_nn[k][epsilon_idx] = nn_results[(epsilon, dataset, k)][0]
                std_nn[k][epsilon_idx] = nn_results[(epsilon, dataset, k)][1]
        
        #get the budgets 
        for epsilon_idx, epsilon in enumerate(epsilons):
            budgets[epsilon_idx] = (100.0/float(num_examples))*mean([len(queries) for queries in plal_results[(epsilon, dataset)]])
            std_budgets[epsilon_idx] = (100.0/float(num_examples))*std([len(queries) for queries in plal_results[(epsilon, dataset)]])
        
        #compute and store plots
        if(show_budget):    
            #plot budget vs. epsilon
            title = "budget(percentage) vs. epsilon %s"%data_str  
            x_lim = (0, epsilons[-1]+0.1)
            y_lim = (0,110)
            plot_with_error(epsilons, budgets, std_budgets, x_lim, y_lim, title, None)
            filename = "%s_budget_vs_epsilon.pdf"%dataset            
            savefig(filename)
            show()                   
        
        if(show_accuracies):
            #first plot accuracy vs. epsilon
            title = "accuracy vs. epsilon %s"%data_str  
            x_lim = (0, epsilons[-1]+0.1)
            y_lim = (0,110)
            for k in ks: 
                method = '%d-plal'%k
                plot_with_error(epsilons, accuracies_plal[k], std_plal[k], x_lim, y_lim, title, method, 
                                color = methods_2_colors[k], icon = 'o')                
                method = '%d-nn'%k
                plot_with_error(epsilons, accuracies_nn[k], std_nn[k], x_lim, y_lim, title, method, 
                                color = methods_2_colors[k], icon = '*')
            
            filename = "%s_accuracy_comparison_plal_nn.pdf"%dataset            
            savefig(filename)
            show()
    
     
if __name__ == '__main__':
    
    plal_models = []
    for dataset in datasets:
        for epsilon in epsilons:
            plal_models.append((epsilon, dataset))    
                
    exp_id = 'with_nn_on_uci'
    exp_id = 'chess'
     #store the plal results here
    outputfile_plal = 'plal_%s.pkl'%exp_id
    #compute_and_store_plal_results_on_uci(plal_models, outputfile_plal)
    plal_results = load_from_file(outputfile_plal)    
    
    #compute plal predictions with nn
    outputfile_plal_predictions = 'plal_predictions_with_nn_%s.pkl'%exp_id
    compute_and_store_plal_predictions_with_nn(plal_results, ks, outputfile_plal_predictions)    
    
    #store the nn results here 
    outputfile_nn = 'nn_with_plal_%s.pkl'%exp_id
    #compute_and_store_nn_results_with_plal_budget_on_uci(plal_results, ks, outputfile_nn)
    nn_results = load_from_file(outputfile_nn)
    
    # collect the results of nn and plal 
    #compare_plal_and_nn_results(plal_results, nn_results)
    #results = run_plal_and_nn_on_uci(models, outputfile = outputfile)