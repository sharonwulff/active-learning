# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""

from plal.plal import pl_algorithm
from test_plal import nearest_neighbor_on_a_budget
from utils.gen_data import gen_2_class_guassians
from utils.utils import plot_2d, plot_error
from numpy import sign, array, matrix, ones, zeros, nonzero, ceil
from pylab import show

num_pts = 300
dims = [2]
epsilons = [0.4]
#flip_percs = [0.05,0.1,0.2]
flip_percs = [0.05]    

def test_nearest_neighbor_on_a_budget(models):
    
    budget = ceil(num_pts*0.2)
    k = 10
    for model_idx, (flip, epsilon, dim) in enumerate(models):
            
        print "%d computing nn for "%model_idx, (flip, epsilon, dim)
        data, labels = gen_2_class_guassians(dim, num_pts, num_pts, flip_perc = flip)
        #plot_2d(data, labels)
        #show()
        labels_nn = nearest_neighbor_on_a_budget(data, labels, budget, k)
        accuracy = 100.0*sum(sign(labels_nn) == sign(labels))/float(len(labels))
        print "nn k %d: accuracy %f with %d of labels out of %d"%(k, accuracy, budget, len(labels))
        


if __name__ == '__main__':
    
    models = []
    for flip in flip_percs:
        for e in epsilons:
            for d in dims: models.append((flip, e, d))    
    
    #test_nearest_neighbor_on_a_budget(models)
    k = 5
    for model_idx, (flip, epsilon, dim) in enumerate(models):
            
        print "%d computing pl al for "%model_idx, (flip, epsilon, dim)
        data, labels = gen_2_class_guassians(dim, num_pts, num_pts, flip_perc = flip)
        #plot_2d(data, labels)
        #show()
        labels_plal, plal_budget, plal_queries = pl_algorithm(data, labels, epsilon)
        #labels_nn = nearest_neighbor_on_a_budget(data, labels, plal_budget, k)
        #for idx in range(len(labels)):
        #    print idx, labels[idx], infered_labels[idx]
        accuracy_plal = 100.0*sum(sign(labels_plal) == sign(labels))/float(len(labels))
        #accuracy_nn = 100.0*sum(sign(labels_nn) == sign(labels))/float(len(labels))
        print "plal accuracy %f with %d of labels out of %d"%(accuracy_plal, plal_budget, len(labels))
        #print "nn k %d: accuracy %f"%(k, accuracy_nn)
        
       