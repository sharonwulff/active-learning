# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon
"""
from algorithms.plal.plal import *
from algorithms.nearest_neighbor import nearest_neighbor_on_a_budget, nearest_neighbor_wrt_sample
from utils.gen_data import gen_plal_data
from utils.utils import *
from numpy import sign, array, matrix, ones, zeros, nonzero, ceil, mean, std
from pylab import show
from numpy.random import seed 

def gen_simple_data(plot = False):
    
    use_seed = 0
    num_examples = 500
    dim = 20
    dense_gaussians = [.25, .25, .25]
    sparse_gaussians =  [.05, .05, .05, .05, .05, .05]
    data, labels = gen_plal_data(num_examples, dim, dense_gaussians, sparse_gaussians, multilabel = True, dense_var = .005, sparse_var = .1, use_seed = use_seed)
    if(plot):
        if(dim == 3):    
            plot_plal_data_3d(data, labels)
        elif(dim ==2):
            plot_2d(data, labels)
        else:
            print "can't plot dim ",dim
    return data, labels
    
if __name__ == '__main__':
    
    data, labels = gen_simple_data()
    dim, num_pts = data.shape
    #print data[:,:10]
    epsilon = 0.4
    k = 5
    plal_labels1, plal_budget, plal_query_points1 = pl_algorithm(data, labels, epsilon, splitting = SPLITTING_RANDOM)    
    plal_labels2, plal_budget, plal_query_points2 = pl_algorithm(data, labels, epsilon, splitting = SPLITTING_MAX)
    nn_random1 = nearest_neighbor_on_a_budget(data, labels, len(plal_query_points1), k)
    nn_random2 = nearest_neighbor_on_a_budget(data, labels, len(plal_query_points2), k)
    nn1 = nearest_neighbor_wrt_sample(data, labels, plal_query_points1, k)
    nn2 = nearest_neighbor_wrt_sample(data, labels, plal_query_points2, k)
    
    print "splitting randomly: budget %f nn-accuracy %f plal-accuracy %f random accuracy %f"%(100.0*len(plal_query_points1)/float(num_pts), multi_label_accuracy(labels, nn1), 
                                                                                               multi_label_accuracy(labels, plal_labels1), multi_label_accuracy(labels, nn_random1))
                                                                                               
    print "splitting max: budget %f nn-accuracy %f plal-accuracy %f random accuracy %f"%(100.0*len(plal_query_points2)/float(num_pts), multi_label_accuracy(labels, nn2), 
                                                                                               multi_label_accuracy(labels, plal_labels2), multi_label_accuracy(labels, nn_random2))
    
    #plal_labels, plal_budget, plal_query_points = pl_algorithm(data, labels, epsilon, splitting = SPLITTING_ALL)
    #print "splitting all: num points %d"%len(plal_query_points)
    