import defs

from data.mnist.parse_mnist import get_mnist_train_labels_sample, get_mnist_test_labels
from utils.utils import *
from prepare import *
from numpy import zeros, argmax, mean, std, sign
from pylab import show, savefig

exp_id = fixed_parameters['exp_id']
outputdir = fixed_parameters['outputdir']
num_train_sample = fixed_parameters['num_train_sample']

def collect_train_results():
    
    #we store the best out of all ks, for each perm. Later we average over the perms 
    results_plal = [[] for epsilon in epsilons]
    results_plal_no_nn = [[] for epsilon in epsilons]
    results_random = [[] for epsilon in epsilons]
    budgets = [[] for epsilon in epsilons]
    
    train_labels_sample = {perm: get_mnist_train_labels_sample(num_train_sample, perm) for perm in perms}
    
    epsilons.sort()  
    
    for perm in perms:
        for eps_idx, epsilon in enumerate(epsilons):
            for use_seed in seeds:
            
                #print perm, epsilon, use_seed
                plal_results_file = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm)
                
                if(not os.path.exists(plal_results_file)):
                    print "cant find file ", plal_results_file
                    continue
        
                [plal_query_points, plal_budget, plal_labels] = load_from_file(plal_results_file)
                budgets[eps_idx].append(100.0*len(plal_query_points)/float(num_train_sample))
              
                plal_temp_res = -1*ones(len(ks))
                random_temp_res = -1*ones(len(ks))
                
                for k_id, k in enumerate(ks):
                    
                    plal_predictions_outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.plal_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k)
                    random_predictions_outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.random_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k)
                    
                    if(os.path.exists(plal_predictions_outputfile)):
                        nn_predictions_on_plal_queries = load_from_file(plal_predictions_outputfile)
                        plal_accuracy = multi_label_accuracy(nn_predictions_on_plal_queries, train_labels_sample[perm])
                        plal_temp_res[k_id] = plal_accuracy
                    #else:
                        #print "cant find file ", plal_predictions_outputfile                        
                        
                    if(os.path.exists(random_predictions_outputfile)):
                        nn_predictions_on_randome_queries = load_from_file(random_predictions_outputfile)
                        random_accuracy = multi_label_accuracy(nn_predictions_on_randome_queries, train_labels_sample[perm])   
                        random_temp_res[k_id] = random_accuracy
                    #else:
                        #print "cant find file ", random_predictions_outputfile
                        
                #print plal_temp_res
                #print random_temp_res
                
                if(max(plal_temp_res) >= 0 and max(random_temp_res) >= 0): 
                    results_plal[eps_idx].append(max(plal_temp_res))
                    results_random[eps_idx].append(max(random_temp_res))
                    results_plal_no_nn[eps_idx].append(multi_label_accuracy(plal_labels, train_labels_sample[perm]))
                else:
                    print "not taking the configuration into account ",perm, epsilon, use_seed
    
    
    results_plal_avg = [mean([results_plal[eps_idx]]) for eps_idx in range(len(epsilons))]
    results_random_avg = [mean([results_random[eps_idx]]) for eps_idx in range(len(epsilons))]
    budgets_avg = [mean([budgets[eps_idx]]) for eps_idx in range(len(epsilons))]
    results_plal_std = [std([results_plal[eps_idx]]) for eps_idx in range(len(epsilons))]
    results_random_std = [std([results_random[eps_idx]]) for eps_idx in range(len(epsilons))]
    budgets_std = [std([budgets[eps_idx]]) for eps_idx in range(len(epsilons))]
    
    results_plal_no_nn_avg = [mean([results_plal_no_nn[eps_idx]]) for eps_idx in range(len(epsilons))]
    results_plal_no_nn_std = [std([results_plal_no_nn[eps_idx]]) for eps_idx in range(len(epsilons))]
    
    print "train results "
    for eps_id, eps in enumerate(epsilons):
        print "epsilon %f: budget: %f +/- %f  plal: %f +/- %f random: %f +/- %f no nn: %f +/- %f "%(eps, budgets_avg[eps_id], budgets_std[eps_id], results_plal_avg[eps_id], results_plal_std[eps_id], 
                                                                                                    results_random_avg[eps_id], results_random_std[eps_id], results_plal_no_nn_avg[eps_id], results_plal_no_nn_std[eps_id])
        
def collect_test_results():
    
    #we store the best out of all ks, for each perm. Later we average over the perms 
    results_plal = [[] for epsilon in epsilons]
    results_random = [[] for epsilon in epsilons]
    budgets = [[] for epsilon in epsilons]
    
    test_labels = get_mnist_test_labels()
  
    epsilons.sort()  
    
    for perm in perms:
        for eps_idx, epsilon in enumerate(epsilons):
            for use_seed in seeds:
                
                #print perm, epsilon, use_seed
                plal_results_file = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm)
                if(not os.path.exists(plal_results_file)):
                    print "cant find file ", plal_results_file
                    continue
        
                [plal_query_points, plal_budget, plal_labels] = load_from_file(plal_results_file)
                budgets[eps_idx].append(100.0*len(plal_query_points)/float(num_train_sample))                
                
                plal_temp_res = -1*ones(len(ks))
                random_temp_res = -1*ones(len(ks))
                            
                for k_id, k in enumerate(ks):
                    
                    plal_predictions_outputfile = '%s/mnist_test_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.plal_test_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k)
                    random_predictions_outputfile = '%s/mnist_test_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.random_test_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k)
                    
                    if(os.path.exists(plal_predictions_outputfile)):
                        
                        nn_predictions_with_plal_queries = load_from_file(plal_predictions_outputfile)
                        plal_accuracy = multi_label_accuracy(nn_predictions_with_plal_queries, test_labels)
                        plal_temp_res[k_id] = plal_accuracy
                    
                    #else:
                        #print "cant find file ", plal_predictions_outputfile                        
                        
                    if(os.path.exists(random_predictions_outputfile)):
                        nn_predictions_with_randome_queries = load_from_file(random_predictions_outputfile)
                        random_accuracy = multi_label_accuracy(nn_predictions_with_randome_queries, test_labels)   
                        random_temp_res[k_id] = random_accuracy
                    #else:
                        #print "cant find file ", random_predictions_outputfile
                        
                print plal_temp_res
                print random_temp_res
                
                if(max(plal_temp_res) >= 0 and max(random_temp_res) >= 0): 
                    results_plal[eps_idx].append(max(plal_temp_res))
                    results_random[eps_idx].append(max(random_temp_res))
                else:
                    print "not taking the configuration into account ",perm, epsilon, use_seed
    

    results_plal_avg = [mean([results_plal[eps_idx]]) for eps_idx in range(len(epsilons))]
    results_random_avg = [mean([results_random[eps_idx]]) for eps_idx in range(len(epsilons))]
    budgets_avg = [mean([budgets[eps_idx]]) for eps_idx in range(len(epsilons))]
    results_plal_std = [std([results_plal[eps_idx]]) for eps_idx in range(len(epsilons))]
    results_random_std = [std([results_random[eps_idx]]) for eps_idx in range(len(epsilons))]
    budgets_std = [std([budgets[eps_idx]]) for eps_idx in range(len(epsilons))]
    
    print "test results "
    for eps_id, eps in enumerate(epsilons):
        print "epsilon %f: budget: %f +/- %f  plal: %f +/- %f random: %f +/- %f "%(eps, budgets_avg[eps_id], budgets_std[eps_id], results_plal_avg[eps_id], results_plal_std[eps_id], results_random_avg[eps_id], results_random_std[eps_id])
        
    

    """

def plot_results(dataset_id, dim, epsilons, results_plal, results_random, budgets):
    
    methods_2_colors = {'plal':'g', 'random':'b', 'budget':'r'}
    epsilons.sort()
    
    title = "data:%d dim %d accuracies and budget(percentage) vs. epsilon"%(dataset_id, dim)
    x_lim = epsilons[-1]+0.1
    y_lim = 110
    plot_with_error(epsilons, budgets[0], budgets[1], x_lim, y_lim, title, 'budget', color = methods_2_colors['budget'])
    plot_with_error(epsilons, results_plal[0], results_plal[1], x_lim, y_lim, title, 'plal', color = methods_2_colors['plal'])
    plot_with_error(epsilons, results_random[0], results_random[1], x_lim, y_lim, title, 'random', color = methods_2_colors['random'])
    plotfile = "%s/%s_dataset_%d_dim_%d_nn_accuracies_and_budget.pdf"%(outputdir, exp_id, dataset_id, dim)
    print plotfile
    savefig(plotfile, format='pdf')
    show()                   

def process_results():
    
    results_plal_avg, results_random_avg, budgets_avg = collect_results()
    sorted_datasets = []
    for (dataset_id, dim) in results_plal_avg.keys():
        sorted_datasets.append([(dataset_id, dim), score_a_result(results_plal_avg[(dataset_id, dim)][0], results_random_avg[(dataset_id, dim)][0], budgets_avg[(dataset_id, dim)][0])])
    
    def my_cmp(i1,i2):
        return int(sign(i2[1] - i1[1]))
    sorted_datasets.sort(cmp = my_cmp)
    
    for (dataset_id, dim), score in sorted_datasets:
        
        print score, (dataset_id, dim)
        plot_results(dataset_id, dim, epsilons, results_plal_avg[(dataset_id, dim)], results_random_avg[(dataset_id, dim)], budgets_avg[(dataset_id, dim)])
"""
                 
if __name__ == '__main__':
    
    collect_test_results()
    #collect_results()
    #collect_train_results()