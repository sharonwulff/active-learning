import defs
import sys
import os

from data.mnist.parse_mnist import get_mnist_train_sample, get_mnist_test_data
from algorithms.plal.plal import pl_algorithm, SPLITTING_MAX

from algorithms.nearest_neighbor import nearest_neighbor_wrt_sample, nearest_neighbor_predictions
from utils.utils import *

from numpy.random import seed, permutation



def gen_random_sample(use_seed, num_examples, num_sample):
    
    seed(use_seed)
    perm = permutation(num_examples)
    return perm[:num_sample]


def compute_nn_on_train(exp_id, use_seed, epsilon, outputdir, perm, num_train_sample, k, plal_or_random = "plal"):
    
    data, labels = get_mnist_train_sample(num_train_sample, perm) 
    plal_results_file = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm)
    
    if(not os.path.exists(plal_results_file)):
        print "cant find file ", plal_results_file
        return -1
    
    [plal_query_points, plal_budget, plal_labels] = load_from_file(plal_results_file)
    outputfile = ""
    if(plal_or_random == "plal"):
        outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.plal_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k)
        nn_predictions_on_plal_queries = nearest_neighbor_wrt_sample(data, labels, plal_query_points, k)        
        write_2_file(outputfile, nn_predictions_on_plal_queries)
        
    elif(plal_or_random == "random"): 
        random_sample = gen_random_sample(use_seed, num_train_sample, len(plal_query_points))
        outputfile = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.random_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k)
        nn_predictions_on_random_queries = nearest_neighbor_wrt_sample(data, labels, random_sample, k)        
        write_2_file(outputfile, nn_predictions_on_random_queries)
    
    print "wrote %s predictions to %s"%(plal_or_random, outputfile)

def compute_nn_on_test(exp_id, use_seed, epsilon, outputdir, perm, num_train_sample, k, plal_or_random = "plal"):
    
    nn_predictions_file = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.%s_queries_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k, plal_or_random)
    if(not os.path.exists(nn_predictions_file)):
        print "Can't find nn predictions file ", nn_predictions_file
        return -1
    
    train_data, train_labels = get_mnist_train_sample(num_train_sample, perm) 
    test_data, test_labels = get_mnist_test_data()
    
    predictions_on_train = load_from_file(nn_predictions_file)
    predictions_on_test = nearest_neighbor_predictions(train_data, predictions_on_train, test_data, k)
    
    outputfile = '%s/mnist_test_%s_seed_%d_epsilon_%s_examples_%d_perm_%d_k_%d.%s_test_predictions'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm, k, plal_or_random)
    write_2_file(outputfile, predictions_on_test)
    print "wrote %s test predictions to %s"%(plal_or_random, outputfile)

def compute_queries(exp_id, use_seed, epsilon, outputdir, perm, num_train_sample):
    
    data, labels = get_mnist_train_sample(num_train_sample, perm) 
    plal_results_file = '%s/mnist_train_%s_seed_%d_epsilon_%s_examples_%d_perm_%d'%(outputdir, exp_id, use_seed, str(epsilon), num_train_sample, perm)
    
    
    plal_labels, plal_budget, plal_query_points = pl_algorithm(data, labels, epsilon, splitting = SPLITTING_MAX)
    print "plal: need %d sample points "%len(plal_query_points)
    write_2_file(plal_results_file, [plal_query_points, len(plal_query_points), plal_labels])
    print plal_results_file
   
if __name__ == '__main__':
     
    action = sys.argv[1]
    exp_id = sys.argv[2]
    use_seed = int(sys.argv[3])
    epsilon = float(sys.argv[4])
    outputdir = sys.argv[5]
    perm = int(sys.argv[6])
    num_train_sample = int(sys.argv[7])
    
    if(action == "compute_queries"):
         
        compute_queries(exp_id, use_seed, epsilon, outputdir, perm, num_train_sample)
        
    elif(action == "compute_nn_on_train"):
        
        k = int(sys.argv[8])
        plal_or_random = sys.argv[9]        
        compute_nn_on_train(exp_id, use_seed, epsilon, outputdir, perm, num_train_sample, k, plal_or_random = plal_or_random)

    elif(action == "compute_nn_on_test"):
        
        k = int(sys.argv[8])
        plal_or_random = sys.argv[9]        
        compute_nn_on_test(exp_id, use_seed, epsilon, outputdir, perm, num_train_sample, k, plal_or_random = plal_or_random)
    else:
        print "dont know action %s"%action
        
    
    
    
    