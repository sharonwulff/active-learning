import defs
import os

base_dir = '..'

perms = [0,1,2,3,4] #the permutations of the input data
seeds = [0,1,2] #repetitions of the algorithm and the sample of random points
epsilons = [.01,.025,.05,.075,.1,.125,.15,.175,.2,.225,.25,.275,.3,.325,.35,.375,.4]
#ks = [1,5,10]
ks = [1]
    
fixed_parameters = {'exp_id':'splitmax',
                    'outputdir':base_dir + '/results/mnist2/splitmax',
                    'num_train_sample':10000
                    }
                    
action = "compute_nn_on_test"

def prepare_experiment():
    
    exp_id = fixed_parameters['exp_id']
    outputdir = fixed_parameters['outputdir']
    num_train_sample = fixed_parameters['num_train_sample']
    
    run_filename = "submit_mnist2_%s.sh"%(exp_id)
    run_file = open(run_filename,'w')
    os.chmod(run_filename, 0755)        
        
    num_jobs = 0
    for use_seed in seeds:
        for epsilon in epsilons:
            for perm in perms:
                if(action == "compute_queries"):
        
                    run_file.write("bsub -W 8:00 python experiments_code/mnist2/main.py %s %s %d %f %s %d %d\n"%\
                                        (action, exp_id, use_seed, epsilon, outputdir, perm, num_train_sample))
                    num_jobs += 1
                elif(action == "compute_nn_on_test" or action == "compute_nn_on_train"):
                
                    for k in ks:
                        for choice in ["random", "plal"]:
                        #for choice in ["random"]:
                            run_file.write("bsub -W 8:00 python experiments_code/mnist2/main.py %s %s %d %f %s %d %d %d %s\n"%\
                                        (action, exp_id, use_seed, epsilon, outputdir, perm, num_train_sample,
                                         k, choice))
                            num_jobs += 1
               
            
    print "going to run %d jobs"%num_jobs
    
if __name__ == '__main__':
    
    prepare_experiment()
    