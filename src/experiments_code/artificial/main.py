import defs
import sys
import os

from algorithms.plal.plal import pl_algorithm
from utils.gen_data import gen_plal_data
from algorithms.nearest_neighbor import nearest_neighbor_wrt_sample
from utils.utils import *
from prepare import datasets 
from numpy.random import seed, permutation



def gen_random_sample(use_seed, num_examples, num_sample):
    
    seed(use_seed)
    perm = permutation(num_examples)
    return perm[:num_sample]

def run(exp_id, dataset_id, dim, num_examples, use_seed, epsilon, outputdir, k, normalize_data):
    
    data_params = datasets[dataset_id]
    data, labels = gen_plal_data(num_examples, dim, data_params['dense_gaussians'], data_params['sparse_gaussians'], 
                                 multilabel = data_params['multilabel'], dense_var = data_params['dense_var'], 
                                 sparse_var =  data_params['sparse_var'], use_seed = use_seed, normalize_data = normalize_data)
    
    plal_results_file = '%s/%s_seed_%d_dataset_%d_dim_%d_examples_%d_epsilon_%s'%(outputdir, exp_id, use_seed, dataset_id, dim, num_examples, str(epsilon))
    if(os.path.exists(plal_results_file)):
        [plal_query_points, plal_budget, plal_labels] = load_from_file(plal_results_file)
    else:
        plal_labels, plal_budget, plal_query_points = pl_algorithm(data, labels, epsilon, min_splitting = True)
        write_2_file(plal_results_file, [plal_query_points, len(plal_query_points), plal_labels])
    
    #print labels[100:110]
    
    nn_predictions_on_plal_queries = nearest_neighbor_wrt_sample(data, labels, plal_query_points, k)
    #print nn_predictions_on_plal_queries[100:110]
    
    random_sample = gen_random_sample(use_seed, num_examples, len(plal_query_points))
    nn_predictions_on_random_queries = nearest_neighbor_wrt_sample(data, labels, random_sample, k)
    #print nn_predictions_on_random_queries[100:110]
    
    outputfile = '%s/%s_seed_%d_dataset_%d_dim_%d_examples_%d_epsilon_%s_k_%d.predictions'%(outputdir, exp_id, use_seed, dataset_id, dim, num_examples, str(epsilon), k)
    write_2_file(outputfile, [nn_predictions_on_plal_queries, nn_predictions_on_random_queries, len(plal_query_points)])
    
    
if __name__ == '__main__':
    
    assert(len(sys.argv) == 10)
    exp_id = sys.argv[1]
    dataset_id = int(sys.argv[2])
    dim = int(sys.argv[3])
    num_examples = int(sys.argv[4])
    use_seed = int(sys.argv[5])
    epsilon = float(sys.argv[6])
    outputdir = sys.argv[7]
    k = int(sys.argv[8])
    normalize_data = int(sys.argv[9])
    
    run(exp_id, dataset_id, dim, num_examples, use_seed, epsilon, outputdir,k, normalize_data)    
    
    
    
    