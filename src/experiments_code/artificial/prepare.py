import defs
import os

base_dir = '..'

datasets_ids = [0,1,2]
dims = [5,10,15,25] 

seeds = [0,1,2,3,4]
epsilons = [.01,.05,.1,.15,.2,.25,.3,.35,.4]
#epsilons = [.05]
ks = [1,3,5,10]
#ks = [1]

fixed_parameters = {'exp_id':'test1',
                    'outputdir':base_dir + '/results/artificial/test1',
                    'num_examples':2500, 
                    'normalize':False
                    }              
                    
dataset0 = {'dense_gaussians':[.21,.21,.21,.21], 'sparse_gaussians':[.04,.04,.04,.04], 
            'multilabel':True, 'dense_var': .1, 'sparse_var': 1}            
dataset1 = {'dense_gaussians':[.21,.21,.21,.21], 'sparse_gaussians':[.04,.04,.04,.04], 
            'multilabel':True, 'dense_var': .5, 'sparse_var': .5}
dataset2 = {'dense_gaussians':[.21,.21,.21,.21], 'sparse_gaussians':[.04,.04,.04,.04], 
            'multilabel':True, 'dense_var': .001, 'sparse_var': .1} #has to be a good one! It is indeed good in terms of saving labels


datasets = [dataset0, dataset1, dataset2]

dataset6 = {'dense_gaussians':[.2,.2,.2,.2], 'sparse_gaussians':[.05,.05,.05,.05], 
            'multilabel':True, 'dense_var': .1, 'sparse_var': 1}            
dataset7 = {'dense_gaussians':[.15,.15,.15,.15,.15], 'sparse_gaussians':[.05,.05,.05,.05,.05], 
            'multilabel':True, 'dense_var': .005, 'sparse_var': .5}#we are doing pretty bad on this one. As soon as the sparse gaussians have substaintial weight and they are very close, we waist all the sample on them
dataset14 = {'dense_gaussians':[.24,.24,.24, .24], 'sparse_gaussians':[0.02, .02, .02], 
            'multilabel':True, 'dense_var': .001, 'sparse_var': .1} #has to be a good one! It is indeed good in terms of saving labels

def prepare_experiment():
    
    exp_id = fixed_parameters['exp_id']
    outputdir = fixed_parameters['outputdir']
    num_examples = fixed_parameters['num_examples']
    normalize = fixed_parameters['normalize']
    
    run_filename = "submit_artificial_%s.sh"%(exp_id)
    run_file = open(run_filename,'w')
    os.chmod(run_filename, 0755)        
        
    num_jobs = 0
    for dataset_id in datasets_ids:
        for dim in dims:
            for seed in seeds:
                for epsilon in epsilons:       
                    for k in ks:
                        run_file.write("bsub -W 1:00 python experiments_code/artificial/main.py %s %d %d %d %d %f %s %d %d\n"%\
                                        (exp_id, dataset_id, dim, num_examples, seed, epsilon, outputdir,k, normalize))
                        num_jobs += 1
                        print "%d worker with 1 jobs"%(num_jobs)
                
    print "going to run %d jobs"%num_jobs

def no_brutus():
    
    from main import run
    exp_id = fixed_parameters['exp_id']
    outputdir = fixed_parameters['outputdir']
    num_examples = fixed_parameters['num_examples']
    normalize = fixed_parameters['normalize']
    
    cnt = 1
    for dataset_id in datasets_ids:
        for dim in dims:
            for seed in seeds:
                for epsilon in epsilons:       
                    for k in ks:
                        
                        run(exp_id, dataset_id, dim, num_examples, seed, epsilon, outputdir, k, normalize)
                        print "finished %d jobs"%(cnt)
                        cnt +=1 

if __name__ == '__main__':
    
    #no_brutus()
    #exit()
    prepare_experiment()
    