import defs
import os

base_dir = '..'

#keep: [6,7,14], [5,15,25], [0,1,3]

datasets_ids = [0,1,2,3,4,5,6,7,14]
datasets_ids = [3,4,6,7,10,11,14]
datasets_ids = [6,7,14]
#dims = [2,5,8,10] 0,1,2,
#dims = [5, 10] #3,4
#dims = [5,10,15,25] 
dims = [5] 

seeds = [0,1,3]
epsilons = [.01,.05,.1,.15,.2,.25,.3]
#epsilons = [.05]
ks = [1,3,5,10]
#ks = [1]

"""
fixed_parameters = {'exp_id':'test1',
                    'outputdir':base_dir + '/results/cooked/test1',
                    'num_examples':2000
                    }
fixed_parameters = {'exp_id':'test2minsplit',
                    'outputdir':base_dir + '/results/cooked/test2',
                    'num_examples':2000, 
                    }
"""                 
#in test2-minsplit we test how well we do with min splitting 
#in test3-normalize we normalize the data, mostly for comparison reasons
"""
fixed_parameters = {'exp_id':'test3normalize',
                    'outputdir':base_dir + '/results/cooked/test3',
                    'num_examples':2000, 
                    'normalize':True
                    }
"""         
fixed_parameters = {'exp_id':'test4notnormalized',
                    'outputdir':base_dir + '/results/cooked/test4',
                    'num_examples':2000, 
                    'normalize':False
                    }              

                    
dataset0 = {'dense_gaussians':[.29,.28,.28], 'sparse_gaussians':[.05,.05,.05], 
            'multilabel':True, 'dense_var': .0001, 'sparse_var': .005}

dataset1 = {'dense_gaussians':[.4,.4], 'sparse_gaussians':[.05,.05,.05,.05], 
            'multilabel':False, 'dense_var': .0001, 'sparse_var': .005}

dataset2 = {'dense_gaussians':[.29,.28,.28], 'sparse_gaussians':[.05,.05,.05], 
            'multilabel':True, 'dense_var': .005, 'sparse_var': .1}
dataset3 = {'dense_gaussians':[.15,.15,.15,.15,.15], 'sparse_gaussians':[.05,.05,.05,.05,.05], 
            'multilabel':True, 'dense_var': .005, 'sparse_var': .1}
dataset4 = {'dense_gaussians':[.15,.15,.15,.15,.15], 'sparse_gaussians':[.05,.05,.05,.05,.05], 
            'multilabel':True, 'dense_var': .01, 'sparse_var': .1}
dataset5 = {'dense_gaussians':[.15,.15,.15,.2,.2], 'sparse_gaussians':[.05,.05,.05], 
            'multilabel':True, 'dense_var': .05, 'sparse_var': .5}            
dataset6 = {'dense_gaussians':[.2,.2,.2,.2], 'sparse_gaussians':[.05,.05,.05,.05], 
            'multilabel':True, 'dense_var': .1, 'sparse_var': 1}            
dataset7 = {'dense_gaussians':[.15,.15,.15,.15,.15], 'sparse_gaussians':[.05,.05,.05,.05,.05], 
            'multilabel':True, 'dense_var': .005, 'sparse_var': .5}#we are doing pretty bad on this one. As soon as the sparse gaussians have substaintial weight and they are very close, we waist all the sample on them
dataset8 = {'dense_gaussians':[.2,.2,.2, .2], 'sparse_gaussians':[.05,.05,.05, .05], 
            'multilabel':True, 'dense_var': .001, 'sparse_var': .05}
dataset9 = {'dense_gaussians':[.25,.25,.25], 'sparse_gaussians':[.05,.05,.05,.05,.05], 
            'multilabel':True, 'dense_var': .001, 'sparse_var': .05}# small difference to random
dataset10 = {'dense_gaussians':[.225,.225,.225,.225], 'sparse_gaussians':[.05,.05], 
            'multilabel':True, 'dense_var': .005, 'sparse_var': .5}#between 4 and 7
dataset11 = {'dense_gaussians':[.225,.225,.225,.225], 'sparse_gaussians':[.025,.025,.025,.025], 
            'multilabel':True, 'dense_var': .01, 'sparse_var': .1}#should be better than 4
dataset12 = {'dense_gaussians':[.24,.24,.24, .24], 'sparse_gaussians':[.01,.01,.01, .01], 
            'multilabel':True, 'dense_var': .01, 'sparse_var': .5}
dataset13 = {'dense_gaussians':[.15,.15,.15,.15,.15], 'sparse_gaussians':[0], 
            'multilabel':True, 'dense_var': .01, 'sparse_var': .1} #like 4 but without noise

dataset14 = {'dense_gaussians':[.24,.24,.24, .24], 'sparse_gaussians':[0.02, .02, .02], 
            'multilabel':True, 'dense_var': .001, 'sparse_var': .1} #has to be a good one! It is indeed good in terms of saving labels


dataset15 = {'dense_gaussians':[.22,.22,.22,.22], 'sparse_gaussians':[.03,.03,.03,.03], 
            'multilabel':True, 'dense_var': .1, 'sparse_var': 1}            
dataset16 = {'dense_gaussians':[.22,.22,.22,.22], 'sparse_gaussians':[.03,.03,.03,.03], 
            'multilabel':True, 'dense_var': .01, 'sparse_var': .1}
dataset17 = {'dense_gaussians':[.22,.22,.22,.22], 'sparse_gaussians':[.03,.03,.03,.03], 
            'multilabel':True, 'dense_var': .001, 'sparse_var': .01} #has to be a good one! It is indeed good in terms of saving labels


datasets = [dataset0, dataset1, dataset2, dataset3, dataset4, dataset5, dataset6, dataset7, dataset8, dataset9,  dataset10,  dataset11, dataset12, 
            dataset13, dataset14, dataset15, dataset16, dataset17]



def prepare_experiment():
    
    exp_id = fixed_parameters['exp_id']
    outputdir = fixed_parameters['outputdir']
    num_examples = fixed_parameters['num_examples']
    normalize = fixed_parameters['normalize']
    
    run_filename = "submit_cooked_%s.sh"%(exp_id)
    run_file = open(run_filename,'w')
    os.chmod(run_filename, 0755)        
        
    num_jobs = 0
    for dataset_id in datasets_ids:
        for dim in dims:
            for seed in seeds:
                for epsilon in epsilons:       
                    for k in ks:
                        run_file.write("bsub -W 1:00 python experiments_code/cooked/main.py %s %d %d %d %d %f %s %d %d\n"%\
                                        (exp_id, dataset_id, dim, num_examples, seed, epsilon, outputdir,k, normalize))
                        num_jobs += 1
                        print "%d worker with 1 jobs"%(num_jobs)
                
    print "going to run %d jobs"%num_jobs

def no_brutus():
    
    from main import run
    exp_id = fixed_parameters['exp_id']
    outputdir = fixed_parameters['outputdir']
    num_examples = fixed_parameters['num_examples']
    normalize = fixed_parameters['normalize']
    
    cnt = 1
    for dataset_id in datasets_ids:
        for dim in dims:
            for seed in seeds:
                for epsilon in epsilons:       
                    for k in ks:
                        
                        run(exp_id, dataset_id, dim, num_examples, seed, epsilon, outputdir, k, normalize)
                        print "finished %d jobs"%(cnt)
                        cnt +=1 

if __name__ == '__main__':
    
    no_brutus()
    #exit()
    #prepare_experiment()
    