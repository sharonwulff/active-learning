import defs

from matplotlib import rc
from utils.gen_data import gen_plal_data
from algorithms.plal.data_pl_properties import pl_properties
from utils.utils import *
from prepare import *
from numpy import zeros, argmax, mean, std, sign
from numpy.linalg import norm
import pylab 


exp_id = fixed_parameters['exp_id']
outputdir = fixed_parameters['outputdir']
num_examples = fixed_parameters['num_examples']


datasetmap = {6:'A',7:'B',14:'C'}

def collect_results(dataset_combinations):
    
    num_examples = fixed_parameters['num_examples']
    epsilons.sort()    
    
    #we store the best out of all ks
    results_plal = {(dataset_id, dim):[[] for epsilon in epsilons] for (dataset_id, dim) in dataset_combinations}
    results_random = {(dataset_id, dim):[[] for epsilon in epsilons] for (dataset_id, dim) in dataset_combinations}

    budgets = {(dataset_id, dim):[[] for epsilon in epsilons] for (dataset_id, dim) in dataset_combinations}
    
    best_k_plal = zeros(len(ks))
    best_k_random = zeros(len(ks))
    
    found_jobs = 0
    for (dataset_id, dim) in dataset_combinations:
        for use_seed in seeds:
            
            print dataset_id, dim, use_seed
            #get the matching dataset
            data_params = datasets[dataset_id]
            data, labels = gen_plal_data(num_examples, dim, data_params['dense_gaussians'], data_params['sparse_gaussians'], 
                             multilabel = data_params['multilabel'], dense_var = data_params['dense_var'], 
                             sparse_var =  data_params['sparse_var'], use_seed = use_seed, normalize_data = fixed_parameters['normalize'])                
            
            for eps_id, epsilon in enumerate(epsilons):       
                
                plal_temp_res = zeros(len(ks))
                nn_temp_res = zeros(len(ks))
                
                for k_id, k in enumerate(ks):
                    
                    outputfile = '%s/%s_seed_%d_dataset_%d_dim_%d_examples_%d_epsilon_%s_k_%d.predictions'%(outputdir, exp_id, use_seed, dataset_id, dim, num_examples, str(epsilon), k)
                    if(not os.path.exists(outputfile)):
                        print "could not find results of %s"%outputfile
                        continue
                    
                    #print "results of %s"%outputfile
                    found_jobs += 1                        
                    [nn_predictions_on_plal_queries, nn_predictions_on_random_queries, plal_query_points] = load_from_file(outputfile)
                    
                    #get accuracy
                    if(data_params['multilabel']):
                        #print "multi-label!"
                        plal_accuracy = multi_label_accuracy(nn_predictions_on_plal_queries, labels)
                        nn_accuracy = multi_label_accuracy(nn_predictions_on_random_queries, labels)
                    else:
                        plal_accuracy = 100.0*sum(sign(nn_predictions_on_plal_queries) == sign(labels))/float(len(labels))
                        nn_accuracy = 100.0*sum(sign(nn_predictions_on_random_queries) == sign(labels))/float(len(labels))
                    
                    plal_temp_res[k_id] = 100.0-plal_accuracy
                    nn_temp_res[k_id] = 100.0-nn_accuracy
                    
                    #store the budget
                    budgets[(dataset_id, dim)][eps_id].append(100.0*plal_query_points/float(num_examples))
                    
                #choose the best k and append the result
                results_plal[(dataset_id, dim)][eps_id].append(min(plal_temp_res))
                best_k_plal[argmax(plal_temp_res)] += 1
                results_random[(dataset_id, dim)][eps_id].append(min(nn_temp_res))
                best_k_random[argmax(nn_temp_res)] += 1
                
                    
                                          
    print "found %d results"%found_jobs
    
    #extract means and stds
    results_plal_avg = {(dataset_id, dim):[[mean(results_plal[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))], 
                                            [std(results_plal[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in results_plal.keys()}
    results_random_avg = {(dataset_id, dim):[[mean(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))], 
                          [std(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in results_random.keys()} 
                            
    budgets_avg = {(dataset_id, dim):[[mean(budgets[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))],
                                     [std(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in budgets.keys()}   
    
    return results_plal_avg, results_random_avg, budgets_avg



def plot_results(dataset_id, dim, epsilons, results_plal, results_random, budgets):

    outputdir = fixed_parameters['outputdir']
    outputdir = '../doc/prob_lip/colt2013/include/'
    
    rc('text', usetex=True)
    rc('font', family='Helvetica')
    fontsize_small = 14
    fontsize_title = 16
    
    lineformats = {'plal':'b-', 'random':'g-', 'budget':'r.:'}
    lines_names = {'plal':r'\% NN-PLAL-error', 'random':r'\% NN-random-error', 'budget':r'\% queries'}

    epsilons.sort()
    
    pylab.xlim(0,epsilons[-1]+0.01)
    pylab.ylim(0,90)
    
    #pylab.title('Synthetic data of size=($%d$,$%d$) $K=%d$ $L=%d$'%(size_factor*K, size_factor*L, K, L))
    pylab.title(r'Synthetic dataset %s: \hspace{3mm} dimension=$%d$'%(datasetmap[dataset_id], dim),fontsize=fontsize_title)            
    pylab.xlabel(r'$\epsilon$ \hspace{1mm} values',fontsize=fontsize_title)
    #pylab.ylabel(r'Percentage',fontsize=fontsize_small)    

    l1 = pylab.errorbar(epsilons, budgets[0], fmt=lineformats['budget'], yerr = budgets[1],  capsize = 5)
    l2 = pylab.errorbar(epsilons, results_plal[0], fmt=lineformats['plal'], yerr = results_plal[1],  capsize = 5)
    l3 = pylab.errorbar(epsilons, results_random[0], fmt=lineformats['random'], yerr = results_random[1],  capsize = 5)    
    
    lines = {'plal':l2, 'random':l3, 'budget':l1}
    #title = "data:%d dim %d accuracies and budget(percentage) vs. epsilon"%(dataset_id, dim)
    plotted_methods_order = ['plal', 'random', 'budget']
    plotted_lines = []
    plotted_methods = []
    for method in plotted_methods_order:
        plotted_methods.append(lines_names[method])
        plotted_lines.append(lines[method])
        
    pylab.legend(plotted_lines, plotted_methods, 'upper right')  
    pylab.grid(True, color='gray')
    
    figfile = "%s/%s_dataset_%d_dim_%d_nn_accuracies_and_budget.pdf"%(outputdir, exp_id, dataset_id, dim)
    print figfile
    pylab.savefig(figfile)
    pylab.show()           

def plot_pl(lambda_range, datasets, dim, means, stds):
    
    outputdir = fixed_parameters['outputdir']
    outputdir = base_dir + '/results/cooked/keep'
    outputdir = '.'
    
    rc('text', usetex=True)
    rc('font', family='Helvetica')
    fontsize_small = 14
    fontsize_title = 16
    
    pylab.ylim(0,110)
    pylab.title(r'Empirical $\phi(\lambda)$ comparison \hspace{3mm} dimension=$%d$'%(dim),fontsize=fontsize_title)            
    pylab.xlabel(r'$\lambda$ values',fontsize=fontsize_title)
    pylab.ylabel(r'Empirical $\phi(\lambda)$',fontsize=fontsize_small)    
    
    plotted_lines = []
    plotted_methods = []
    for dataset_id in datasets:
        label = r'dataset %s'%datasetmap[dataset_id]
        l = pylab.errorbar(lambda_range, means[dataset_id], yerr=stds[dataset_id], capsize=5)
        plotted_lines.append(l)
        plotted_methods.append(label)
        
    pylab.legend(plotted_lines, plotted_methods, 'lower right')  
    pylab.grid(True, color='gray')
    #plot_file = '%s/pl_comparison_%d.pdf'%(outputdir,dim)
    #pylab.savefig(plot_file, format='pdf')
    #print "saved the comparison to file:"
    #print plot_file


def plot_polynomial_lambda(lambda_range, n, max_dist):
    
    pylab.plot(lambda_range, (100.0/(pow(max_dist,n)))*array([pow(i,n) for i in lambda_range]))
    
    
def prepare_pl_plots():
    
    datasets_ids = [6,7,14]
    dim = 25
    #average max dist for dim 25 is ~17    
    
    seeds = [i for i in range(3)]
    num_examples = 500
    use_normalization = True
    max_dist = 10
    granularity = 200
    
    means = {}
    stds = {}    
    lambda_range = None
    
    for dataset_id in datasets_ids:
        print dataset_id, dim
        data_params = datasets[dataset_id]
        
        pl_arr = []
        for use_seed in seeds:
            data, labels = gen_plal_data(num_examples, dim, data_params['dense_gaussians'], data_params['sparse_gaussians'], 
                                         multilabel = data_params['multilabel'], dense_var = data_params['dense_var'], 
                                         sparse_var =  data_params['sparse_var'], use_seed = use_seed, normalize_data = use_normalization)
            
            print data.shape
            lambda_range, pl_lambda = pl_properties(data, labels, granularity = granularity, 
                                                    pl_def = "truncated", normalize_dist = False, max_dist = max_dist, min_dist = 0)
                        
            pl_arr.append(pl_lambda)
        
        pl_lambda_means = []
        pl_lambda_stds = []
        
        for i in range(len(lambda_range)):
            pl_lambda_means.append(mean([pl_arr[use_seed][i] for use_seed in seeds]))
            pl_lambda_stds.append(std([pl_arr[use_seed][i] for use_seed in seeds]))
            
        means[dataset_id] = pl_lambda_means
        stds[dataset_id] = pl_lambda_stds
    
    plot_pl(lambda_range, datasets_ids, dim, means, stds)
    for n in range(20,30):
        plot_polynomial_lambda(lambda_range, n, max_dist)
    pylab.show()

    exit()
    
    headers = ["lambda", "A-mean","A-std","B-mean","B-std","C-mean","C-std"]
    usedir = base_dir + '/results/cooked/keep'
    csv_file = usedir + "/pl_comparison_dim_%d.csv"%(dim)
    write_2_csv(csv_file, headers, lambda_range, means[6], stds[6], means[7], stds[7], means[14], stds[14])
    print "wrote ",csv_file
    
def write_2_csv(outputfilename, headers, xaxis, m1,s1,m2,s2,m3,s3):
    
    import csv
    outputfile = csv.writer(open(outputfilename, 'w'), delimiter=',', quoting=csv.QUOTE_NONE)   
    # Write the headers
    outputfile.writerow(headers)
    for i in range(len(xaxis)):
        curr_row = [xaxis[i], m1[i], s1[i], m2[i], s2[i], m3[i], s3[i]]
        outputfile.writerow(curr_row)

    print "wrote to ",outputfilename
    
def prepare_accuracy_plot():
    
    #list down the ones you want to plot, make sure that the right experiment parameters are set in prepare
    datasets_ids = [6,7,14]
    #dims = [5] #5,6,7
    dims = [5,10,15,25] #5,6,7
    
    headers = ["epsilon", "plal-mean","plal-std","random-mean","random-std","queries-mean","queries-std"]
    
    # each combination of a dataset_id (parameters and dim) is considered a dataset of each own. 
    dataset_combinations = []
    for dataset_id in datasets_ids:
        for dim in dims: dataset_combinations.append((dataset_id, dim))
    
    results_plal_avg, results_random_avg, budgets_avg = collect_results(dataset_combinations)
    
    for (dataset_id, dim) in dataset_combinations:
        print (dataset_id, dim)
        
        #plot_results(dataset_id, dim, epsilons, results_plal_avg[(dataset_id, dim)], results_random_avg[(dataset_id, dim)], budgets_avg[(dataset_id, dim)])
        
        
        usedir = base_dir + '/results/cooked/keep'
        csv_file = usedir + "/data_%s_dim_%d.csv"%(datasetmap[dataset_id], dim)
        write_2_csv(csv_file, headers, epsilons, results_plal_avg[(dataset_id, dim)][0], results_plal_avg[(dataset_id, dim)][1], 
                                                                        results_random_avg[(dataset_id, dim)][0], results_random_avg[(dataset_id, dim)][1], 
                                                                        budgets_avg[(dataset_id, dim)][0], budgets_avg[(dataset_id, dim)][1])
                                                                        


if __name__ == '__main__':
    
   #prepare_accuracy_plot()
   prepare_pl_plots()
    