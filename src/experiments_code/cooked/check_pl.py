import defs
import sys
import os

from utils.gen_data import gen_plal_data
from utils.utils import *
from utils.mldata import normalize
from prepare import * 
from algorithms.plal.data_pl_properties import pl_properties, neighbors_distance
from numpy import mean, std
import pylab 

def check_neighbors_distance(configurations):
    
    num_examples = fixed_parameters['num_examples']
    num_neighbors = ks
    
    for dataset_id, dim in configurations:
        print dataset_id, dim
        data_params = datasets[dataset_id]
        
        avg_neighbors_dist = {k:[] for k in ks}        
        for use_seed in seeds:
            data, labels = gen_plal_data(num_examples, dim, data_params['dense_gaussians'], data_params['sparse_gaussians'], 
                                         multilabel = data_params['multilabel'], dense_var = data_params['dense_var'], 
                                         sparse_var =  data_params['sparse_var'], use_seed = use_seed)
                                         
            curr_neighbors_distance = neighbors_distance(data, labels, num_neighbors)
            print use_seed, curr_neighbors_distance
            for k in ks: avg_neighbors_dist[k].append(curr_neighbors_distance[k])
        
        print "average:"
        for k in ks: print k, mean(avg_neighbors_dist[k])
            

def plot_pl_properties(configurations):
    
    seeds = [0,1,2,3,4,5,6,7,8]
    num_examples = 500
    outputdir = fixed_parameters['outputdir']
    use_normalization = True
    
    fig = pylab.figure()    
    lambda_range = None
    
    datastr = ""    
    
    for dataset_id, dim in configurations:
        print dataset_id, dim
        data_params = datasets[dataset_id]
        pl_lambda_arr = []
        for use_seed in seeds:
            data, labels = gen_plal_data(num_examples, dim, data_params['dense_gaussians'], data_params['sparse_gaussians'], 
                                         multilabel = data_params['multilabel'], dense_var = data_params['dense_var'], 
                                         sparse_var =  data_params['sparse_var'], use_seed = use_seed, normalize_data = use_normalization)
            
            lambda_range, pl_lambda = pl_properties(data, labels, granularity = 500, pl_def = "truncated", normalize_dist = False, max_dist = dim, min_dist = 0)
                        
            pl_lambda_arr.append(pl_lambda)
        
        datastr += "_data%d_dim%d"%(dataset_id, dim)
        pl_lambda_means = []
        pl_lambda_stds = []
        
        for i in range(len(lambda_range)):
            pl_lambda_means.append(mean([pl_lambda_arr[use_seed][i] for use_seed in seeds]))
            pl_lambda_stds.append(std([pl_lambda_arr[use_seed][i] for use_seed in seeds]))
        pylab.errorbar(lambda_range, pl_lambda_means, yerr=pl_lambda_stds, capsize=3, label = "dataset%d-dim%d"%(dataset_id, dim))
    
    pylab.ylim(0,110)
    use_title = "Thi of lambda"    
    pylab.title(use_title)
    pylab.legend()            
    plot_file = '%s/pl_comparison%s.pdf'%(outputdir,datastr)
        
    pylab.legend()    
    pylab.savefig(plot_file, format='pdf')
    print "saved the comparison to file:"
    print plot_file
    pylab.show()
if __name__ == '__main__':
    
    #configurations = [(4,10),(7,10),(9,10)]    
    configurations = [(10,5),(11,5),(7,5),(4,5)]
    #as of 3 its in the middle regime, on 7,10 we are doing really well, as of 5 we are starting to save less labels
    configurations1 = [(14,15),(7,15),(6,15)]
    configurations2 = [(14,10),(7,10),(6,10)]
    configurations3 = [(14,5),(7,5),(6,5)]
    
    plot_pl_properties(configurations2)
    #check_neighbors_distance(configurations)
    