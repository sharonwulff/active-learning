import defs

from utils.gen_data import gen_plal_data
from utils.utils import *
from prepare import *
from numpy import zeros, argmax, mean, std, sign
from pylab import show, savefig

exp_id = fixed_parameters['exp_id']
outputdir = fixed_parameters['outputdir']
num_examples = fixed_parameters['num_examples']

def score_a_result(plal_means, random_means, budgets):
    
    #the score is the added difference between plal and random over the epsilons, weighted by the budget
    score = sum([(100.0-budgets[i])*(plal_means[i] - random_means[i]) for i in range(len(plal_means))])
    #This one only cares about the savings of labels
    #score = sum([(100.0-budgets[i]) for i in range(len(plal_means))])
    return score
    
def collect_results():
    
    epsilons.sort()    

    # each combination of a dataset_id (parameters and dim) is considered a dataset of each own. 
    dataset_combinations = []
    for dataset_id in datasets_ids:
        for dim in dims: dataset_combinations.append((dataset_id, dim))
            
    #we store the best out of all ks
    results_plal = {(dataset_id, dim):[[] for epsilon in epsilons] for (dataset_id, dim) in dataset_combinations}
    results_random = {(dataset_id, dim):[[] for epsilon in epsilons] for (dataset_id, dim) in dataset_combinations}

    budgets = {(dataset_id, dim):[[] for epsilon in epsilons] for (dataset_id, dim) in dataset_combinations}
    
    best_k_plal = zeros(len(ks))
    best_k_random = zeros(len(ks))
    
    found_jobs = 0
    for (dataset_id, dim) in dataset_combinations:
        
        if(dim == 2 and dataset_id in [3,4]): continue
        for use_seed in seeds:
            
            print dataset_id, dim, use_seed
            #get the matching dataset
            data_params = datasets[dataset_id]
            data, labels = gen_plal_data(num_examples, dim, data_params['dense_gaussians'], data_params['sparse_gaussians'], 
                             multilabel = data_params['multilabel'], dense_var = data_params['dense_var'], 
                             sparse_var =  data_params['sparse_var'], use_seed = use_seed)                
            
            for eps_id, epsilon in enumerate(epsilons):       
                
                plal_temp_res = zeros(len(ks))
                nn_temp_res = zeros(len(ks))
                
                for k_id, k in enumerate(ks):
                    
                    outputfile = '%s/%s_seed_%d_dataset_%d_dim_%d_examples_%d_epsilon_%s_k_%d.predictions'%(outputdir, exp_id, use_seed, dataset_id, dim, num_examples, str(epsilon), k)
                    if(not os.path.exists(outputfile)):
                        print "could not find results of %s"%outputfile
                        continue
                    
                    #print "results of %s"%outputfile
                    found_jobs += 1                        
                    [nn_predictions_on_plal_queries, nn_predictions_on_random_queries, plal_query_points] = load_from_file(outputfile)
                    
                    #get accuracy
                    if(data_params['multilabel']):
                        #print "multi-label!"
                        plal_accuracy = multi_label_accuracy(nn_predictions_on_plal_queries, labels)
                        nn_accuracy = multi_label_accuracy(nn_predictions_on_random_queries, labels)
                    else:
                        plal_accuracy = 100.0*sum(sign(nn_predictions_on_plal_queries) == sign(labels))/float(len(labels))
                        nn_accuracy = 100.0*sum(sign(nn_predictions_on_random_queries) == sign(labels))/float(len(labels))
                    
                    plal_temp_res[k_id] = plal_accuracy
                    nn_temp_res[k_id] = nn_accuracy
                    
                    #store the budget
                    budgets[(dataset_id, dim)][eps_id].append(100.0*plal_query_points/float(num_examples))
                    #if(k_id == 0): budgets[(dataset_id, dim)][eps_id].append(100.0*plal_query_points/float(num_examples))
                    #assert(100.0*plal_query_points/float(num_examples) == budgets[(dataset_id, dim)][eps_id][-1])

                #choose the best k and append the result
                results_plal[(dataset_id, dim)][eps_id].append(max(plal_temp_res))
                best_k_plal[argmax(plal_temp_res)] += 1
                results_random[(dataset_id, dim)][eps_id].append(max(nn_temp_res))
                best_k_random[argmax(nn_temp_res)] += 1
                
                    
                                          
    print "found %d results"%found_jobs
    
    #extract means and stds
    results_plal_avg = {(dataset_id, dim):[[mean(results_plal[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))], 
                                            [std(results_plal[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in results_plal.keys()}
    results_random_avg = {(dataset_id, dim):[[mean(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))], 
                          [std(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in results_random.keys()} 
                            
    budgets_avg = {(dataset_id, dim):[[mean(budgets[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))],
                                     [std(results_random[(dataset_id, dim)][eps_id]) for eps_id in range(len(epsilons))]] for (dataset_id, dim) in budgets.keys()}   
    
    return results_plal_avg, results_random_avg, budgets_avg



def plot_results(dataset_id, dim, epsilons, results_plal, results_random, budgets):
    
    methods_2_colors = {'plal':'g', 'random':'b', 'budget':'r'}
    epsilons.sort()
    
    title = "data:%d dim %d accuracies and budget(percentage) vs. epsilon"%(dataset_id, dim)
    x_lim = epsilons[-1]+0.1
    y_lim = 110
    plot_with_error(epsilons, budgets[0], budgets[1], x_lim, y_lim, title, 'budget', color = methods_2_colors['budget'])
    plot_with_error(epsilons, results_plal[0], results_plal[1], x_lim, y_lim, title, 'plal', color = methods_2_colors['plal'])
    plot_with_error(epsilons, results_random[0], results_random[1], x_lim, y_lim, title, 'random', color = methods_2_colors['random'])
    plotfile = "%s/%s_dataset_%d_dim_%d_nn_accuracies_and_budget.pdf"%(outputdir, exp_id, dataset_id, dim)
    print plotfile
    savefig(plotfile, format='pdf')
    show()                   

def process_results():
    
    results_plal_avg, results_random_avg, budgets_avg = collect_results()
    sorted_datasets = []
    for (dataset_id, dim) in results_plal_avg.keys():
        sorted_datasets.append([(dataset_id, dim), score_a_result(results_plal_avg[(dataset_id, dim)][0], results_random_avg[(dataset_id, dim)][0], budgets_avg[(dataset_id, dim)][0])])
    
    def my_cmp(i1,i2):
        return int(sign(i2[1] - i1[1]))
    sorted_datasets.sort(cmp = my_cmp)
    
    for (dataset_id, dim), score in sorted_datasets:
        
        print score, (dataset_id, dim)
        plot_results(dataset_id, dim, epsilons, results_plal_avg[(dataset_id, dim)], results_random_avg[(dataset_id, dim)], budgets_avg[(dataset_id, dim)])
        
                 
if __name__ == '__main__':
    
    process_results()