
"""
@author: sharon
"""

from plal.plal import pl_algorithm
from plal.nearest_neighbor import nearest_neighbor_on_a_budget, nearest_neighbor_wrt_sample
from shockfish_utils.utils import write_2_file, load_from_file
from shockfish_utils.svm_utils import train_and_predict_with_svm
from shockfish_utils.plot_utils import plot_with_error
from numpy import sign, array, matrix, ones, zeros, nonzero, ceil, mean, std
from numpy.random import permutation
from pylab import show, savefig

import os 

from plal_on_shockfish import get_shockfish_data, shockfish_sensors_ids


#kernels = [('Linear','None'), ('Poly',2), ('Gaussian',1)]
kernels = [('Linear','None')]
Cs = [100,1000,10]
results_dir = 'plal/results'
methods_2_colors = {3:'r',5:'b',7:'m',10:'y',15:'g'}

""" helper functions """

def compute_plal_queries_on_shockfish(plal_models, repetitions, outputfile, test_mode = 'train_on_all_test_on_one'):
    
    print "compute_plal_queries_on_shockfish"
    results = {}
     
    if test_mode == 'train_on_all_test_on_one':
        
        for (epsilon, sensor_id) in plal_models:
            print "testing on ",epsilon, sensor_id
            #get the data            
            train_on = list(shockfish_sensors_ids)
            train_on.remove(sensor_id)
            test_on = [sensor_id]
            train_data, train_labels, test_data, test_labels = \
            get_shockfish_data(split_type = 'different_sensors', train_on = train_on, test_on = test_on)
            
            results[(epsilon, sensor_id)] = []
            
            for repetition in range(repetitions):
                print "repetition %d out of %d"%(repetition, repetitions)
                plal_labels, plal_budget, query_points = pl_algorithm(train_data, train_labels, epsilon)
                results[(epsilon, sensor_id)].append(query_points)
            
            
            write_2_file(outputfile, results)
            
    elif test_mode == 'label_each_sensor':
        
        sensors_data, sensors_labels = get_shockfish_data(split_type = None)
        
        for (epsilon, sensor_id) in plal_models:
            
            print "finding plal label for ",epsilon, sensor_id           
                
            results[(epsilon, sensor_id)] = []
            for repetition in range(repetitions):
                print "repetition %d out of %d"%(repetition, repetitions)
                plal_labels, plal_budget, query_points = pl_algorithm(sensors_data[sensor_id], sensors_labels[sensor_id], epsilon)
                results[(epsilon, sensor_id)].append((plal_labels, plal_budget, query_points))
                
            write_2_file(outputfile, results)
            
            
        
def extract_budget_from_plal_results(plal_results):
    
    budgets = {}
    for key, results in plal_results.iteritems():
        budgets[key] = []
        for repetition_result in results:
            budgets[key].append(len(repetition_result))
    
    return budgets

def test_shockfish_with_plal_queries(test_models, plal_results, outputfile):
    
    results = {}
    for (epsilon, sensor_id), model_plal_results in plal_results.iteritems():
            
        print (epsilon, sensor_id)
                    
        train_on = list(shockfish_sensors_ids)
        train_on.remove(sensor_id)
        test_on = [sensor_id]
        train_data, train_labels, test_data, test_labels = \
        get_shockfish_data(split_type = 'different_sensors', train_on = train_on, test_on = test_on)
        
        for (kernel, C) in test_models:
            
            results[(epsilon, sensor_id, kernel, C)] = []
            for plal_queries in model_plal_results:
                
                print "train with budget %d with "%len(plal_queries), (kernel, C)
                plal_train_data = train_data[:,plal_queries]
                plal_train_labels = train_labels[plal_queries]
                
                if(sum(plal_train_labels > 0) < 1 or sum(plal_train_labels < 0) < 1):
                    print "repetition has only one class labels!"
                    continue
                
                predictions = train_and_predict_with_svm(plal_train_data, plal_train_labels, test_data, C, kernel_type = kernel[0], param = kernel[1])
                results[(epsilon, sensor_id, kernel, C)].append(predictions)
            
            write_2_file(outputfile, results)
            
def test_shockfish_with_budget(budgets, test_models, outputfile):
    
    print "test_shockfish_with_budget"
    results = {}
    for (epsilon, sensor_id), model_budgets in budgets.iteritems():
            
        print (epsilon, sensor_id)
                    
        train_on = list(shockfish_sensors_ids)
        train_on.remove(sensor_id)
        test_on = [sensor_id]
        train_data, train_labels, test_data, test_labels = \
        get_shockfish_data(split_type = 'different_sensors', train_on = train_on, test_on = test_on)
        
        for (kernel, C) in test_models:
            
            results[(epsilon, sensor_id, kernel, C)] = []
            for budget in model_budgets:
                
                print "train with budget %d with "%budget, (kernel, C)
                dim, num_train = train_data.shape
                queries = permutation(num_train)[:budget]
                budget_train_data = train_data[:,queries]
                budget_train_labels = train_labels[queries]
                
                if(sum(budget_train_labels > 0) < 1 or sum(budget_train_labels < 0) < 1):
                    print "repetition has only one class labels!"
                    continue
                
                predictions = train_and_predict_with_svm(budget_train_data, budget_train_labels, test_data, C, kernel_type = kernel[0], param = kernel[1])
                results[(epsilon, sensor_id, kernel, C)].append(predictions)
            
            write_2_file(outputfile, results)

def _extract_accuracies(budgets, results, averaged_over_sensors = True):
        
    epsilons = [key[0] for key in results.keys()]
    averaged_results = {eps:{} for eps in epsilons}
    
    #print "_extract_accuracies",results.keys()
    for (epsilon, sensor_id, (kernel_type, kernel_param), C), predictions in results.iteritems():    
        
        print (epsilon, sensor_id, (kernel_type, kernel_param), C)
        configuration = ((kernel_type, kernel_param), C)
        if (configuration not in averaged_results[epsilon]): averaged_results[epsilon][configuration] = []
        
        train_on = list(shockfish_sensors_ids)
        train_on.remove(sensor_id)
        test_on = [sensor_id]
        train_data, train_labels, test_data, test_labels = \
        get_shockfish_data(split_type = 'different_sensors', train_on = train_on, test_on = test_on)
        
        for pred_idx, preds in enumerate(predictions):
            acc = 100.0*sum(sign(preds) == sign(test_labels))/float(len(preds))
            #try to see how we are doing with many labels
            if(budgets[(epsilon, sensor_id)][pred_idx] > 2000):
                averaged_results[epsilon][configuration].append(acc)
            #print acc, budgets[(epsilon, sensor_id)][pred_idx]
    
    return averaged_results
    
def experiment1():
    
    epsilons = [0.1, 0.15, 0.3]
    repetitions = 10
    print "experiment 1: train on all sensors but 1 using plal queries, test on 1 sensor"
    
    exp_id = 'trainOnAllTestOnOneTake2'
    experiment_mode = 'train_on_all_test_on_one'
     #store the plal results here
    outputfile_plal = '%s/plal_%s.pkl'%(results_dir, exp_id)
    svm_predictions_with_plal = '%s/plal_svm_predictions_%s.pkl'%(results_dir, exp_id)
    svm_predictions_with_budget = '%s/budget_svm_predictions_%s.pkl'%(results_dir, exp_id)
    
    plal_models = []
    for epsilon in epsilons:
        for sensor_id in shockfish_sensors_ids:
        #for sensor_id in [13]:
            plal_models.append((epsilon, sensor_id))    
    
    test_models = []
    for kernel in kernels:
        for C in Cs:
            test_models.append((kernel, C))
            
    #compute the queries
    if(not os.path.exists(outputfile_plal)):
        print "cant find file ",outputfile_plal
        compute_plal_queries_on_shockfish(plal_models, repetitions, outputfile_plal, test_mode = experiment_mode)
    
    plal_results = load_from_file(outputfile_plal)
    #remove the results which have an unreasonable amount of labels i.e. too small or too large
    min_labels = 20
    max_labels = 3000    
    temp_plal_results = {}    
    for key, results_arr in plal_results.iteritems():
        temp_results_arr = []
        for res in results_arr:
            if (len(res) >= min_labels and len(res) <= max_labels):
                temp_results_arr.append(res)
        if(len(temp_results_arr) > 0):
            temp_plal_results[key] = temp_results_arr
            if(len(temp_results_arr) < len(results_arr)): print "key %s keeping %d results out of %d, the rest have unsuitable number of labels requested"%(str(key),len(temp_results_arr),len(results_arr))
        else: print "key %s is discarded for unsuitable number of labels requested"%str(key)
    plal_results = temp_plal_results
    
    budgets = extract_budget_from_plal_results(plal_results)    
   
    #run svm on the test data and write the results
    if(not os.path.exists(svm_predictions_with_plal)): 
        print "cant find file ",svm_predictions_with_plal
        test_shockfish_with_plal_queries(test_models, plal_results, svm_predictions_with_plal)
        
    plal_svm_predictions = load_from_file(svm_predictions_with_plal)
    
    #run svm on the test data with random budget queries and write the results
    if(not os.path.exists(svm_predictions_with_budget)): 
        print "cant find file ",svm_predictions_with_budget
        test_shockfish_with_budget(budgets, test_models, svm_predictions_with_budget)
    
    budget_svm_predictions = load_from_file(svm_predictions_with_budget)
    
    #process some of the results, compare the test data of plal vs. random 
    averaged_plal_svm = _extract_accuracies(budgets, plal_svm_predictions)
    averaged_budget_svm = _extract_accuracies(budgets, budget_svm_predictions)
    
    #choose the best configuration for each 
    for eps in averaged_plal_svm.keys():
        confs = averaged_plal_svm[eps].keys()
        confs.sort()
        for conf in confs:
            print "epsilon:%f conf %s"%(eps, str(conf))
            print mean(averaged_plal_svm[eps][conf]), averaged_plal_svm[eps][conf]
            print mean(averaged_budget_svm[eps][conf]), averaged_budget_svm[eps][conf]


def experiment2(show_budget = True, show_accuracies = True):
    
    print "experiment 2: for each sensor find the plal queries and predictions in a transductive settings"
    
    epsilons = [0.05, 0.1, 0.15, 0.2, 0.3]
    #ks = [3, 5, 7, 10, 15]
    ks = [3, 5, 7, 10, 15]    
    repetitions = 10
    exp_id = 'shockfishTransductive'
    experiment_mode = 'label_each_sensor'
     #store the plal results here
    outputfile_plal = '%s/plal_%s.pkl'%(results_dir, exp_id)
    nn_accuracies_with_plal_file = '%s/nn_accuracies_plal_queries_%s.pkl'%(results_dir, exp_id)
    nn_accuracies_with_budget_file = '%s/nn_accuracies_budget_%s.pkl'%(results_dir, exp_id)
    
    test_models = ks
    
    plal_models = []
    for epsilon in epsilons:
        for sensor_id in shockfish_sensors_ids:
            plal_models.append((epsilon, sensor_id))       
    
    sensors_data, sensors_labels = get_shockfish_data(split_type = None)    
    
    #compute the queries
    if(not os.path.exists(outputfile_plal)):
        print "cant find file ",outputfile_plal
        compute_plal_queries_on_shockfish(plal_models, repetitions, outputfile_plal, test_mode = experiment_mode)
    
    plal_results = load_from_file(outputfile_plal)
    
    if(not os.path.exists(nn_accuracies_with_plal_file)):
        plal_accuracies = {}
        nn_on_plal_accuracies = {}
        nn_on_budget_accuracies = {}
        
        for (epsilon, sensor_id) in plal_models:
            
            print (epsilon, sensor_id)
            
            if(not (epsilon, sensor_id) in plal_results): 
                print "not in plal_results!"
                continue
    
            for (plal_labels, plal_budget, query_points) in plal_results[(epsilon, sensor_id)]:
                
                plal_acc = 100.0*sum(sign(plal_labels) == sign(sensors_labels[sensor_id]))/float(len(plal_labels))            
                #print "plal_acc ",plal_acc
                
                assert(plal_budget == len(query_points))                
                random_sample = permutation(len(sensors_labels[sensor_id]))[:plal_budget]
                
                for k in ks:
                    
                    nn_plal_predictions = nearest_neighbor_wrt_sample(sensors_data[sensor_id], sensors_labels[sensor_id], query_points, k)
                    nn_plal_acc = 100.0*sum(sign(nn_plal_predictions) == sign(sensors_labels[sensor_id]))/float(len(nn_plal_predictions))
                    nn_budget_predictions = nearest_neighbor_wrt_sample(sensors_data[sensor_id], sensors_labels[sensor_id], random_sample, k)
                    nn_budget_acc = 100.0*sum(sign(nn_budget_predictions) == sign(sensors_labels[sensor_id]))/float(len(nn_budget_predictions))
                    
                    #print "nn_plal_acc, nn_budget_acc ",nn_plal_acc, nn_budget_acc
                    
                    if((epsilon, k) not in plal_accuracies):
                        plal_accuracies[(epsilon, k)] = []
                        nn_on_plal_accuracies[(epsilon, k)] = []
                        nn_on_budget_accuracies[(epsilon, k)] = []
                    
                    plal_accuracies[(epsilon, k)].append(plal_acc)
                    nn_on_plal_accuracies[(epsilon, k)].append(nn_plal_acc)
                    nn_on_budget_accuracies[(epsilon, k)].append(nn_budget_acc)
        
        
            write_2_file(nn_accuracies_with_plal_file, nn_on_plal_accuracies)  
            write_2_file(nn_accuracies_with_budget_file, nn_on_budget_accuracies)
    
    nn_on_plal_accuracies = load_from_file(nn_accuracies_with_plal_file)
    nn_on_budget_accuracies = load_from_file(nn_accuracies_with_budget_file)
    
    #get the plal accuracy from the plal results file
    plal_accuracies = {}
    plal_budgets = {}
    
    for (epsilon, sensor_id) in plal_results.keys():
        for (plal_labels, plal_budget, query_points) in plal_results[(epsilon, sensor_id)]:
            
            if(epsilon not in plal_budgets): plal_budgets[epsilon] = []
            plal_budgets[epsilon].append(100.0*(plal_budget)/float(len(plal_labels)))
            
            plal_acc = 100.0*sum(sign(plal_labels) == sign(sensors_labels[sensor_id]))/float(len(plal_labels))            
            for k in ks:
                if((epsilon, k) not in plal_accuracies): plal_accuracies[(epsilon, k)] = []
                plal_accuracies[(epsilon, k)].append(plal_acc)
    
    epsilons_res = list(set([key[0] for key in plal_results.keys()]))
    epsilons_res.sort()
    
    means_nn_on_plal = {k:zeros(len(epsilons)) for k in ks}
    std_nn_on_plal = {k:zeros(len(epsilons)) for k in ks}
    
    means_nn_on_budget = {k:zeros(len(epsilons)) for k in ks}
    std_nn_on_budget = {k:zeros(len(epsilons)) for k in ks}
    
    means_plal = {k:zeros(len(epsilons)) for k in ks}
    std_plal = {k:zeros(len(epsilons)) for k in ks}
    
    means_budgets = zeros(len(epsilons))
    std_budgets = zeros(len(epsilons))
    
    for (epsilon, k), accuracies in nn_on_plal_accuracies.iteritems():
        
        epsilon_idx = epsilons_res.index(epsilon)
        
        means_nn_on_plal[k][epsilon_idx] = mean(accuracies)
        std_nn_on_plal[k][epsilon_idx] = std(accuracies)
        
        means_nn_on_budget[k][epsilon_idx] = mean(nn_on_budget_accuracies[(epsilon, k)])
        std_nn_on_budget[k][epsilon_idx] = std(nn_on_budget_accuracies[(epsilon, k)])
        
        means_plal[k][epsilon_idx] = mean(plal_accuracies[(epsilon, k)])
        std_plal[k][epsilon_idx] = std(plal_accuracies[(epsilon, k)])
        
        means_budgets[epsilon_idx] = mean(plal_budgets[epsilon])
        std_budgets[epsilon_idx] = std(plal_budgets[epsilon])
        
    #compute and store plots
    if(show_budget):    
        #plot budget vs. epsilon
        title = "budget(percentage) vs. epsilon averaged over 9 sensors"
        x_lim = (0, epsilons_res[-1]+0.1)
        y_lim = (0,110)
        #plot_with_error(epsilons_res, means_budgets, std_budgets, x_lim, y_lim, title, None)
        plot_with_error(epsilons_res, means_budgets, None, x_lim, y_lim, title, None)        
        filename = "/shockfish_budget_vs_epsilon.pdf"            
        savefig(results_dir + filename)
        show()                   
        
    if(show_accuracies):
        #first plot accuracy vs. epsilon
        title = "shockfish accuracy vs. epsilon averaged over 9 sensors %d repetitions"%repetitions
        x_lim = (0, epsilons[-1]+0.1)
        y_lim = (0,110)
        for k in ks: 
            if k == 15: continue
            method = '%d-plal-nn'%k
            #plot_with_error(epsilons, means_nn_on_plal[k], std_nn_on_plal[k], x_lim, y_lim, title, method, 
            plot_with_error(epsilons, means_nn_on_plal[k], None, x_lim, y_lim, title, method, 
                            color = methods_2_colors[k], icon = 'o')                
            method = '%d-nn'%k
            plot_with_error(epsilons, means_nn_on_budget[k], None, x_lim, y_lim, title, method, 
                            color = methods_2_colors[k], icon = '*')            
#            method = '%d-plal'%k
#            plot_with_error(epsilons, means_plal[k], None, x_lim, y_lim, title, method, 
#                            color = methods_2_colors[k], icon = '+')               
        
        filename = "/shockfish_accuracy_comparison_plal_nn.pdf"            
        savefig(results_dir + filename)
        show()    
    
    
if __name__ == '__main__':
                
    experiment2()