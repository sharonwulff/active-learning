# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 15:45:16 2012

@author: sharon & ruth
"""


from shockfish_utils.handle_data import *
from shockfish_utils.features import MagneticReadingFeatures
from shockfish_utils.utils import write_2_file, load_from_file
from numpy import vstack
import os.path, time

base_results_dir = 'results'
base_data_dir = '/home/wulffs/projects/shockfish_new/data/hummerich'
labeled_data_file_part1 = base_data_dir + '/26_aug/labeled_data_hummerich_0318_to_0404.csv'
labeled_data_file_part2 = base_data_dir + '/26_aug/labeled_data_hummerich_0419_to_0511.csv'
labeled_data_together = base_data_dir + '/26_aug/labeled_data_hummerich_0318_to_0404_and_0419_to_0511.csv'
hummerich_labeled_file = labeled_data_together

#shockfish_permfile = base_data_dir + '/26_aug/perms.pkl'

shockfish_sensors_ids = [6,9,10,11,12,13,14,15,16]
shockfish_data_temp_file = 'shockfish_data_temp.pkl'

def get_shockfish_data(split_type = None, train_on = None, test_on = None):
    
    #check if the temp file exists
    if(not os.path.exists(shockfish_data_temp_file) or (time.time() - os.path.getctime(shockfish_data_temp_file) > 86400)):
        
        #get the hummerich data
        standarized_hummerich_data = get_standarized_labeled_data(hummerich_labeled_file, aligned_data = False)
        features_info = MagneticReadingFeatures(x_axis = True, y_axis = True, z_axis = True, radius = True)
        hummerich_features = extract_features(standarized_hummerich_data, features_info)
        (hummerich_features, min_val, max_val) = scale_features(hummerich_features)
        # dilute the hummerich measurements to avoid a blow up of the training data 
        hummerich_labels = {}
        for sensor_id, data in standarized_hummerich_data.iteritems(): hummerich_labels[sensor_id] = data[:,processed_data_label_pos]
        #dilute the data to get rid of redundant measurements    
        threshold = 0.00001
        (sensors_data, sensors_labels) = dilute_labeled_sensor_data(hummerich_features, hummerich_labels, threshold)
            
        write_2_file(shockfish_data_temp_file, (sensors_data, sensors_labels))
    
    
    (sensors_data, sensors_labels) = load_from_file(shockfish_data_temp_file)
    if(split_type == None): 
        for sensor_id, sensor_data in sensors_data.iteritems():
            sensors_data[sensor_id] = sensor_data.T
            sensors_labels[sensor_id] = array(sensors_labels[sensor_id])
        return sensors_data, sensors_labels
    
    if(split_type == 'different_sensors'):
        
        assert(len(train_on) > 0 and len(test_on) > 0)
        train_data = None
        train_labels = []
        test_data = None
        test_labels = []
        
        for sensor_id, sensor_data in sensors_data.iteritems():
            
            if(sensor_id in train_on):
                #print "adding sensor %d to the train set"%sensor_id                
                if(train_data == None): train_data = sensor_data
                else: train_data = vstack((train_data, sensor_data))
                train_labels.extend(sensors_labels[sensor_id])
            if(sensor_id in test_on):
                #print "adding sensor %d to the test set"%sensor_id                
                if(test_data == None): test_data = sensor_data
                else: test_data = vstack((test_data, sensor_data))
                test_labels.extend(sensors_labels[sensor_id])
                
        return train_data.T, array(train_labels), test_data.T, array(test_labels)

#---------------------------------------------------------------------------     
if __name__ == '__main__':
    
    train_data, train_labels, test_data, test_labels = get_shockfish_data(split_type = 'different_sensors', train_on = [6,9,10], test_on = [11,12,13,14,15,16])
   