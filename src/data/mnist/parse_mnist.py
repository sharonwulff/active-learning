# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 15:25:09 2013

@author: wulffs
"""
import os
import sys
import struct
from defs import home
from numpy import empty, zeros, array, floor, arange, loadtxt
import pickle
from numpy.linalg import norm
from numpy.random import permutation, randint

endian_char = '>'
if(sys.byteorder != 'little'): endian_char = '<'

data_path = '%s/projects/active_learning/src/data/mnist'%home
if(not os.path.exists(data_path)):
    print "Can't find the data path ", data_path
    exit()
    
#original files
orig_train_file = '%s/train-images-idx3-ubyte'%data_path
orig_train_labels_file = '%s/train-labels-idx1-ubyte'%data_path
orig_test_file = '%s/t10k-images-idx3-ubyte'%data_path
orig_test_labels_file = '%s/t10k-labels-idx1-ubyte'%data_path


#after parsing once, store the rest here
train_data_file = '%s/mnist_train_data.pkl'%data_path
train_labels_file = '%s/mnist_train_labels.pkl'%data_path
test_data_file = '%s/mnist_test_data.pkl'%data_path
test_labels_file = '%s/mnist_test_labels.pkl'%data_path

permfile_template = '%s/mnist_perms_train'%data_path

#files storing partial data and things we would not want to recompute
train_data_file_small = '%s/mnist_train_data_small.pkl'%data_path
train_data_file_half = '%s/mnist_train_data_half.pkl'%data_path
distance_matrix_file = '%s/mnist_distance_matrix.pkl'%data_path    


def get_stream(filename):
    
    f = open(filename,'rb')
    #read the file
    stream = ""
    byte = f.read(1)
    while(byte):
        stream += byte
        byte = f.read(1)
    return stream

def get_mnist_train_sample(num_examples, perm_id):
    
    train_data, train_labels = get_mnist_train_data()
    permfile = permfile_template+"_%d_examples.txt"%num_examples
    if(not os.path.exists(permfile)):
        print "cant file perm file %s. Creating a new one", permfile
        prepare_perm_file(max(20, perm_id), train_labels, num_examples, permfile)
    perms = loadtxt(permfile, dtype=int, delimiter=' ')
    print len(perms[perm_id])
    
    return train_data[:,perms[perm_id]], array(train_labels)[perms[perm_id]]    

def get_mnist_train_labels_sample(num_examples, perm_id):
    
    train_labels = get_mnist_train_labels()
    permfile = permfile_template+"_%d_examples.txt"%num_examples
    if(not os.path.exists(permfile)):
        print "cant file perm file %s. Creating a new one", permfile
        prepare_perm_file(max(20, perm_id), train_labels, num_examples, permfile)
    perms = loadtxt(permfile, dtype=int, delimiter=' ')
    
    return array(train_labels)[perms[perm_id]]    

def _parse_mnist_data(datafile, num_examples):
    
    num_pixels_row = 28
    num_pixels_col = 28
    dim = num_pixels_row * num_pixels_col
    
    #read the data, parse it and store it in a normal format
    data = zeros((dim, num_examples))    
    train_stream = get_stream(datafile)
    #sanity check for the offset
    assert(int(struct.unpack('>i', train_stream[4:8])[0]) == num_examples)
    stream_offset = 16
    for idx in range(num_examples):
        offset = 0
        for i in range(dim):
            data[offset, idx] = int(struct.unpack('>B', train_stream[stream_offset + idx*dim + offset])[0])
            offset += 1  
    
    return data

def _parse_mnist_labels(labelsfile, num_examples):
    
    #Now read and parse the labels
    labels = []
    labels_stream = get_stream(labelsfile)
    #sanity check for the offset
    assert(int(struct.unpack('>i', labels_stream[4:8])[0]) == num_examples)
    stream_offset = 8
    for idx in range(num_examples):
        labels.append(int(struct.unpack('>B', labels_stream[stream_offset + idx])[0]))
    
    return labels
    
def get_mnist_train_data(small = False, half = False):
    
    if(small and os.path.exists(train_data_file_small)):
        pkl_file = open(train_data_file_small, 'rb')
        [train_data, train_labels] = pickle.load(pkl_file)
        return train_data, array(train_labels)
    
    if(half and os.path.exists(train_data_file_half)):
        pkl_file = open(train_data_file_half, 'rb')
        [train_data, train_labels] = pickle.load(pkl_file)
        return train_data, array(train_labels)
     
    if(os.path.exists(train_data_file)):
        pkl_file = open(train_data_file, 'rb')
        [train_data, train_labels] = pickle.load(pkl_file)
        return train_data, array(train_labels)
    
    train_data = _parse_mnist_data(orig_train_file, 60000)
    train_labels = get_mnist_train_labels()
    
    #store it to a file
    outputfile = open('%s'%train_data_file, 'wb')
    pickle.dump([train_data, train_labels], outputfile)
    
    return train_data, array(train_labels)

#one might need access to the labels only, much faster to avoid loading the data
def get_mnist_train_labels():
    
    if(os.path.exists(train_labels_file)):
        pkl_file = open(train_labels_file, 'rb')
        train_labels = pickle.load(pkl_file)
        return array(train_labels)
    
    num_train_examples = 60000
    
    train_labels = _parse_mnist_labels(orig_train_labels_file, num_train_examples)
    print len(train_labels)
    outputfile = open('%s'%train_labels_file, 'wb')
    pickle.dump(train_labels, outputfile)
    return array(train_labels)


def get_mnist_test_data():
    
    if(os.path.exists(test_data_file)):
        pkl_file = open(test_data_file, 'rb')
        [test_data, test_labels] = pickle.load(pkl_file)
        return test_data, array(test_labels)
    
    test_data = _parse_mnist_data(orig_test_file, 10000)
    test_labels = get_mnist_test_labels()
    
    #store it to a file
    outputfile = open('%s'%test_data_file, 'wb')
    pickle.dump([test_data, test_labels], outputfile)
    
    return test_data, array(test_labels)

#one might need access to the labels only, much faster to avoid loading the data
def get_mnist_test_labels():
    
    if(os.path.exists(test_labels_file)):
        pkl_file = open(train_labels_file, 'rb')
        test_labels = pickle.load(pkl_file)
        return array(test_labels)
    
    num_test_examples = 10000
    
    test_labels = _parse_mnist_labels(orig_test_labels_file, num_test_examples)
    print len(test_labels)
    outputfile = open('%s'%test_labels_file, 'wb')
    pickle.dump(test_labels, outputfile)
    
    return array(test_labels)


def get_mnist_distance_matrix():
    
    if(os.path.exists(distance_matrix_file)):
        pkl_file = open(train_data_file, 'rb')
        distance_mat = pickle.load(pkl_file)
        return distance_mat
    
    data, labels = get_mnist_data()
    dim, num_pts = data.shape
    #compute the distance matrix, this can take time..     
    distance_matrix = zeros((num_pts, num_pts))
    for i in range(num_pts):
        if(num_pts > 5000): print "row %d"%i
        for j in range(i):
            n = norm(data[:,i] - data[:,j])
            distance_matrix[i,j] = n
            distance_matrix[j,i] = n    


def check_data_point(data_vec, label):
    
    from pylab import imshow, show
    data_mat = data_vec.reshape(28,28)    
    print label
    imshow(data_mat)
    show()
    
def test_mnist_parse():
    
    train_data, train_labels = get_data()
    for i in range(10):
        check_data_point(train_data[:,i], train_labels[i])

def count_mnist_class_labels():
    
    train_labels = get_mnist_train_labels()
    unique_labels = list(set(train_labels))
    num_labels = [sum(train_labels == i) for i in unique_labels]
    print unique_labels, num_labels

def prepare_perm_file(num_perms, labels, num_pts_sample, permfilename):
    
    num_unique_labels = len(set(labels))
    permfile = open(permfilename,'w')
    num_examples_per_label = int(floor(num_pts_sample/float(num_unique_labels)))
    missing_examples = num_pts_sample - num_examples_per_label*num_unique_labels    
    
    for i in range(num_perms):
        cnt = 0        
        lucky_index = randint(num_unique_labels)
        for label in range(num_unique_labels):
            label_indices = arange(len(labels))[array(labels) == label]
            perm = permutation(len(label_indices))
            if(label != lucky_index): permed_indices = label_indices[perm][:num_examples_per_label]
            else: permed_indices = label_indices[perm][:num_examples_per_label+missing_examples]
            for j in permed_indices:
                if(cnt < num_pts_sample - 1):
                    permfile.write("%d "%j)
                else:
                    permfile.write("%d\n"%j)
                cnt += 1
        print "wrote %d indices"%cnt
    permfile.close()
    print "wrote perms ", permfilename
   
if __name__ == '__main__':
    
    num_sample = 10000
    data, labels = get_mnist_train_sample(num_sample, 0)
    print data.shape
    print len(labels)
    
    #prepare_perm_file(3, array([0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2]), 7, "deleteme")
    
    """
    num_perms = 20
    num_pts = 10000
    num_pts_sample = 10000
    filename = "mnist_train_perms_%d_examples.txt"%num_pts_sample
    prepare_perm_file(num_perms, num_pts, num_pts_sample, filename)
    """
    #start off
    #get_mnist_distance_matrix()
    #get_mnist_data()