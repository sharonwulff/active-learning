
"""Some scripts to help run machine learning experiments.

   Based on Hans Peter Langtangen's scripts for multipleloop
"""

import re, operator
import expttools
import copy_reg
import types

multiprocessing_present=True
try:
    import multiprocessing
    from multiprocessing import Process
except ImportError, detail:
    print "Error importing multiprocessing. Local computing limited to one CPU."
    print "Please install python2.6 or the backport of the multiprocessing package"
    print detail
    multiprocessing_present=False
#multiprocessing_present=False


def apply_to_combination(function_handle, param, fixed=dict(),
                         verbose=False, multiproc=True, seperateproc=False):
    """Apply the function defined by function handle to the
    tensor product of the parameters defined by dictionary param.
    
    param is a dictionary of lists, each key defining the
    keyword argument to be passed to the function. The function
    is applied to all combinations of all the settings.

    The dictionary defined by fixed is concatenated to the end
    of the keywords.
    """
    assert(param.keys() != fixed.keys())
    all_values = [(name, input2values(param[name])) for name in param]
    (params,names,varied) = combine(all_values)

    function_inputs = []
    for cur_param in params:
        kwargs = fixed.copy()
        kwargs.update(dict((names[ix],cur_param[ix]) for ix in xrange(len(names))))
        if verbose:
            print 'apply_to_combination'
            print function_handle
            print kwargs
            print '===================='
        function_inputs.append(kwargs)
        
    if multiprocessing_present and multiproc:
        if multiproc:
            cpus = multiprocessing.Pool()
            for fins in function_inputs:
                fins['function_handle'] = function_handle
            function_outputs = cpus.map(expttools._wrap_kwargs, function_inputs)
            
    elif multiprocessing_present and seperateproc:
            for kwargs in function_inputs:
                p = Process(target=function_handle, args=(kwargs,))
                p.start()
                p.join()
            return
            
    else:
        function_outputs = []
        for kwargs in function_inputs:
            cur_out = function_handle(**kwargs)
            function_outputs.append(cur_out)

    return (function_outputs, function_inputs)

def _wrap_kwargs(kwargs):
    """Small helper function to enable Pool"""
    function_handle = kwargs['function_handle']
    del kwargs['function_handle']
    return function_handle(**kwargs)

def _pickle_method(method):
    """A workaround for inability to pickle instance methods"""
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    """A workaround for inability to pickle instance methods"""
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)

copy_reg.pickle(types.MethodType, expttools._pickle_method, expttools._unpickle_method)

def input2values(s):
    """
    Translate a string s with multiple loop syntax into
    a list of single values (for the corresponding parameter).

    Multiple loop syntax:
    '-1 & -3.4 & 20 & 70 & [0:10,1.3] & [0:10] & 11.76'

    That is, & is delimiter between different values, [0:10,1.3]
    generates a loop from 0 up to and including 10 with steps 1.3,
    [0:10] generates the integers 1,2,...,10.

    Interactive session::

    >>> input2values('-1 & -3.4 & 20 & 70 & [0:10,1.3] & [0:10] & 11.76')
    [-1, -3.3999999999999999, 20, 70, 0, 1.3, 2.6000000000000001,
     3.9000000000000004, 5.2000000000000002, 6.5, 7.7999999999999998,
     9.0999999999999996, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11.76]

    >>> p = {'w': '[0.7:1.3,0.1]', 'b': '1 & 0.3 & 0', 'func': 'y & siny'}
    >>> print input2values(p['w'])
    [0.69999999999999996, 0.79999999999999993, 0.89999999999999991,
     0.99999999999999989, 1.0999999999999999, 1.2, 1.3]
    >>> print input2values(p['b'])
    [1, 0.29999999999999999, 0]
    >>> print input2values(p['func'])
    ['y', 'siny']
    >>> prm_values = [(name, input2values(p[name])) for name in p]
    >>> prm_values
    [('b', [1, 0.29999999999999999, 0]),
     ('func', ['y', 'siny']),
     ('w', [0.69999999999999996, 0.79999999999999993, 0.89999999999999991,
            0.99999999999999989, 1.0999999999999999, 1.2, 1.3])]

    """
    if not isinstance(s, basestring):
        return s
    
    items = s.split('&')

    values = []
    for i in items:
        i = i.strip()  # white space has no meaning
        # is i a loop?
        m = re.search(r'\[(.+):([^,]+),?(.*)\]',i)
        if m:
            # the group are numbers, take eval to get right type
            start = eval(m.group(1))
            stop  = eval(m.group(2))
            try:
                incr = m.group(3).strip()
                incr_op = operator.add
                if incr[0] == '*':
                    incr_op = operator.mul
                    incr = incr[1:]
                elif incr[0] == '+' or incr[0] == '-':
                    incr = incr[1:]
                incr = eval(incr)
            except:
                incr = 1
            r = start
            while (r <= stop and start <= stop) or \
                  (r >= stop and start >= stop):
                values.append(r)
                r = incr_op(r, incr)
        else:
            # just an ordinary item, convert i to right type:
            values.append(str2obj(i))
    # return list only if there are more than one item:
    if len(values) == 1:
        return values[0]
    else:
        return values



def _outer(a, b):
    """
    Return the outer product/combination of two lists.
    a is a multi- or one-dimensional list,
    b is a one-dimensional list, tuple, NumPy array or scalar (new parameter)
    Return:  outer combination 'all'.

    The function is to be called repeatedly::
    
        all = _outer(all, p)
    """
    all = []
    if not isinstance(a, list):
        raise TypeError, 'a must be a list'
    if isinstance(b, (float,int,complex,basestring)):  b = [b]  # scalar?

    if len(a) == 0:
        # first call:
        for j in b:
            all.append([j])
    else:
        for j in b:
            for i in a:
                if not isinstance(i, list):
                    raise TypeError, 'a must be list of list'
                # note: i refers to a list; i.append(j) changes
                # the underlying list (in a), which is not what
                # we want, we need a copy, extend the copy, and
                # add to all
                k = i + [j]  # extend previous prms with new one
                all.append(k)
    return all

def combine(prm_values):
    """
    @param prm_values: nested list of 
        (parameter_name, list_of_parameter_values)
        or dictionary
        prm_values[parameter_name] = list_of_parameter_values

    @type prm_values: list or dict

    @return: (all,names,varied) where

      - all contains all combinations (experiments)
        all[i] is the list of individual parameter values in
        experiment no i

      - names contains a list of all parameter names

      - varied holds a list of parameter names that are varied
        (i.e. where there is more than one value of the parameter)


    Code example::

    >>> dx = array([1.0/2**k for k in range(2,5)])
    >>> dt = 3*dx;  dt = dt[:-1]
    >>> p = {'dx': dx, 'dt': dt}
    >>> p
    {'dt': [ 0.75 , 0.375,], 'dx': [ 0.25  , 0.125 , 0.0625,]}
    >>> all, names, varied = combine(p)
    >>> all
    [[0.75, 0.25], [0.375, 0.25], [0.75, 0.125], [0.375, 0.125],
     [0.75, 0.0625], [0.375, 0.0625]]

    """
    if isinstance(prm_values, dict):
        # turn dict into list [(name,values),(name,values),...]:
        prm_values = [(name, prm_values[name]) \
                      for name in prm_values]
    all = []
    varied = []
    for name, values in prm_values:
        all = _outer(all, values)
        if isinstance(values, list) and len(values) > 1:
            varied.append(name)
    names = [name for name, values in prm_values]
    return all, names, varied

def dump(all, names, varied):
    e = 1
    for experiment in all:
        print 'Experiment %4d:' % e,
        for name, value in zip(names, experiment):
            print '%s:' % name, value,
        print # newline
        e += 1  # experiment counter

    for experiment in all:
        cmd = ' '.join(['-'+name+' '+repr(value) for \
                        name, value in zip(names, experiment)])
        print cmd

def options(all, names, prefix='--'):
    """
    Return a list of command-line options.

    @param all: all[i] holds a list of parameter values in experiment no i
    @param names: names[i] holds name of parameter no. i
    @return: cmd[i] holds -name value pairs of all parameters in
             experiment no. i
    """
    cmd = []
    for experiment in all:
        cmd.append(' '.join([prefix + name + ' ' + repr(str2obj(value)) \
                   for name, value in zip(names, experiment)]))
    return cmd

def varied_parameters(parameters, varied, names):
    """
    @param names: names of parameters.
    @param parameters: values of parameters.
    @param varied: subset of names (the parameters that are varied elsewhere).
    @return: a list of the items in parameters whose names are listed
    in varied.

    An example may help to show the idea. Assume we have three parametes
    named 'a', 'b', and 'c'. Their values are 1, 5, and 3, i.e.,
    'a' is 1, 'b' is 5, and 'c' is 3. In a loop elsewhere we assume
    that 'a' and 'c' are varied while 'b' is fixed. This function
    returns a list of the parameter values that correspond to varied
    parameters, i.e., [1,3] in this case, corresponding to the names
    'a' and 'c'::

    >>> parameters = [1,5,3]
    >>> names = ['a','b','c']
    >>> varied = ['a','c']
    >>> varied_parameteres(parameters, varied, names)
    [1,3]
    """
    indices_varied = [names.index(i) for i in varied]
    varied_parameters = [parameters[i] for i in indices_varied]
    return varied_parameters

def remove(condition, all, names):
    """
    Remove experiments that fulfill a boolean condition.
    Example:
    all = remove('w < 1.0 and p = 1.2) or (q in (1,2,3) and f < 0.1', all, names)
    (names of the parametes must be used)
    """
    import copy
    for ex in copy.deepcopy(all):  # iterate over a copy of all!
        c = condition
        for n in names:  # replace names by actual values
            #print 'replace "%s" by "%s"' % (n, repr(ex[names.index(n)]))
            c = c.replace(n, repr(ex[names.index(n)]))
            # note the use of repr: strings must be quoted
            #print 'remove ',remove
        if eval(c):  # if condition
            all.remove(ex)
    return all  # modified list
    
def str2obj(s, globals=globals(), locals=locals()):
    """
    Turn string s into the corresponding object.
    eval(s) normally does this, but if s is just a string ready
    from file, GUI or the command-line, eval will not work when
    s really represents a string:
    >>> eval('some string')
    Traceback (most recent call last):
    SyntaxError: unexpected EOF while parsing
    It tries to parse 'some string' as Python code.

    In this function we try to eval(s), and if it works, we
    return that object. If it does not work, s probably has
    meaning as a string, and we return just s.

    Examples::
    
    >>> from misc import str2obj
    >>> s = str2obj('0.3')
    >>> print s, type(s)
    0.3 <type 'float'>
    >>> s = str2obj('3')
    >>> print s, type(s)
    3 <type 'int'>
    >>> s = str2obj('(1,8)')
    >>> print s, type(s)
    (1, 8) <type 'tuple'>
    >>> s = str2obj('some string')
    >>> s
    'some string'

    If the name of a user defined function, class or instance is
    sent to str2obj, one must also provide locals() and globals()
    dictionaries as extra arguments. Otherwise, str2obj will not
    know how to "eval" the string and produce the right object
    (user defined types are not known inside str2obj).
    Here is an example::
    
    >>> def myf(x):
    ...     return 1+x
    ... 
    >>> class A:
    ...     pass
    ... 
    >>> a = A()
    >>> 
    >>> s = str2obj('myf')
    >>> print s, type(s)   # now s is simply the string 'myf'
    myf <type 'str'>
    >>> # provide locals and globals such that we get the function myf:
    >>> s = str2obj('myf', locals(), globals())
    >>> print s, type(s)
    <function myf at 0xb70ffe2c> <type 'function'>
    >>> s = str2obj('a', locals(), globals())
    >>> print s, type(s)
    <__main__.A instance at 0xb70f6fcc> <type 'instance'>

    Caveat: if the string argument is the name of a valid Python
    class (type), that class will be returned. For example,
    >>> str2obj('list')  # returns class list
    <type 'list'>

    You can normally safely apply eval on the output of this function.
    """
    try:
        s = eval(s, globals, locals)
        return s
    except:
        return s
    


def _test1():
    s1 = ' -3.4 & [0:4,1.2] & [1:4,*1.5] & [0.5:6E-2,  *0.5]'
    #s2 = "method1 &  abc  & 'adjusted method1' "
    s2 = 0.22
    s3 = 's3'
    l1 = input2values(s1)
    l2 = input2values(s2)
    l3 = input2values(s3)
    p = [('prm1', l3), ('prm2', l2), ('prm3', l1)]
    all, names, varied = combine(p)
    dump(all, names, varied)
    p = {'w': [0.7, 1.3, 0.1], 'b': [1, 0], 'func': ['y', 'siny']}
    all, names, varied = combine(p)
    print '\n\n\n'
    dump(all, names, varied)
    print options(all, names, prefix='-')

def _test2():
    p = {'w': '[0.7:1.3,0.1]', 'b': '1 & 0.3 & 0', 'func': 'y & siny'}
    print input2values(p['w'])
    print input2values(p['b'])
    print input2values(p['func'])
    prm_values = [(name, input2values(p[name])) \
                  for name in p]
    print 'prm_values:', prm_values
    all, names, varied = combine(prm_values)
    print 'all:', all

    # rule out b=0 when w>1
    all_restricted = [];
    bi = names.index('b'); wi = names.index('w')
    for e in all:
        if e[bi] == 0 and e[wi] > 1:
            pass # rule out
        else:
            all_restricted.append(e)  # del would be dangerous!
    # b->damping, w->omega:
    names2 = names[:]
    names2[names.index('b')] = 'damping'
    names2[names.index('w')] = 'omega'
    print options(all, names, prefix='--')
    conditions = (('b',operator.eq,0), ('w',operator.gt,1))
    def rule_out(all, conditions):
        all_restricted = []
        for e in all:
            for name, op, r in conditions:
                pass


def _inner_func(a=1.0, b=2.0, x=3.0):
    """For use with _test3()"""
    return (0.5*a*a*x + b*x)

def _test3():
    """Test apply_to_combination"""
    params = {'a': [-1.0, -0.2, 0.2, 1.0],
              'b': [0.3, 3.2],
              }
    fixed_param = {'x': 1.3}
    results = apply_to_combination(expttools._inner_func, params, fixed=fixed_param)
    print results
    

if __name__ == '__main__':
    #_test1()
    #_test2()
    _test3()
