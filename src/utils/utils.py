# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 16:21:00 2012

@author: sharon
"""

import pylab
from mpl_toolkits.mplot3d import Axes3D

from numpy import array, matrix, zeros, nonzero, arange, sign, ones
from numpy.random import random, randint
from numpy.linalg import norm 
from mldata import Dataset, normalize

import pickle 

need_shogun = False
if(need_shogun):
    from shogun.Kernel import LinearKernel
    from shogun.Features import RealFeatures, Labels
    from shogun.Classifier import LibSVM

datadir = '../data'
epsilon = 1e-10


    
def plot_plal_data_3d(points, labels = None, labels_names = None):
    
    colors_2_classes = {0:'r',1:'g',2:'b',3:'cyan',4:'magenta'}
    markers_2_classes = {0:'o',1:'^',2:'*',3:'-',4:'&'}    
    
    dim, num_pts = points.shape
    assert(dim == 3 or dim == 2)
    
    fig = pylab.figure()
    ax = fig.add_subplot(111, projection='3d')


    if(labels == None):
        xs = points[0,:]
        ys = points[1,:]
        zs = points[2,:]
        ax.scatter(xs, ys, zs, c='r', marker='^')
    else:
        classes = list(set(labels))
        labels = array(labels)
        for c_idx, c in enumerate(classes):
            indices = arange(num_pts)[labels == c]
            xs = points[0,indices]
            ys = points[1,indices]
            zs = points[2,indices]
            real_idx = c_idx%len(colors_2_classes)
            ax.scatter(xs, ys, zs, c=colors_2_classes[real_idx], marker=markers_2_classes[real_idx])
            
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    pylab.show()    
    
def plot_2d(train_ex, train_lab):
    """Plot 2D data"""
    pos_lab = train_lab > 0.0
    neg_lab = train_lab < 0.0
    pylab.plot(train_ex[0,pos_lab], train_ex[1,pos_lab], 'r+')
    pylab.plot(train_ex[0,neg_lab], train_ex[1,neg_lab], 'bx')
    
    pylab.show()

def plot_2d(train_ex, train_lab):
    """Plot 2D data"""
    pos_lab = train_lab > 0.0
    neg_lab = train_lab < 0.0
    pylab.plot(train_ex[0,pos_lab], train_ex[1,pos_lab], 'r+')
    pylab.plot(train_ex[0,neg_lab], train_ex[1,neg_lab], 'bx')
    
    pylab.show()
    
def highlight_2d(train_ex, train_lab):
    """Plot 2D data, highlight in yellow, for active learning for example"""
    if(len(train_lab) == 1):
         if(train_lab > 0): pylab.plot(train_ex[0], train_ex[1], 'g+')
         else: pylab.plot(train_ex[0], train_ex[1], 'gx')
         return
    pos_lab = train_lab > 0.0
    neg_lab = train_lab < 0.0
    pylab.plot(train_ex[0,pos_lab], train_ex[1,pos_lab], 'g+')
    pylab.plot(train_ex[0,neg_lab], train_ex[1,neg_lab], 'gx')

def plot_with_error(x,y,err,x_lim, y_lim, title, legend, color = "r", icon = "o"):
    
    pylab.errorbar(x, y, yerr=err, fmt='%s%s'%(color,icon), label = legend)
    pylab.title(title)
    pylab.xlim(0,x_lim)
    pylab.ylim(0,y_lim)
    if(legend != None): pylab.legend()

def predict_with_hyperplane(w, b, data):
    
    dim, num_points = data.shape
    pred = array(matrix(w).T*matrix(data) + b)
    pos_idx = nonzero(pred>0.0)[1]
    neg_idx = nonzero(pred<0.0)[1]
    labels = zeros(num_points)
    labels[pos_idx] = 1.0
    labels[neg_idx] = -1.0  

def multi_label_accuracy(labels, predictions):
    
    return 100.0*(sum(array(labels) == array(predictions)))/float(len(predictions))


def get_hyperplane_from_svm(svm, data):

    w = sum(svm.get_alphas()*data[:,svm.get_support_vectors()],1)
    b = svm.get_bias()
    
    return w,b 

def train_with_svm(train_data, labels, C, kernel_type = "linear"):
    
    kernel = LinearKernel()
    if(kernel_type != "linear"):
        print "unknown kernel type ", kernel_type
        exit()
    feats_train = RealFeatures(train_data)
    kernel.init(feats_train, feats_train)
    train_labels = Labels(labels)
    svm = LibSVM(C, kernel, train_labels)
    svm.train()
    
    return svm

def predict(train_data, test_data, svm): 
    
    feats_train = RealFeatures(train_data)
    feats_test = RealFeatures(test_data)
    kernel = svm.get_kernel()
    kernel.init(feats_train, feats_test)
    try:
        labels = svm.classify().get_labels()
        return labels 
    except AttributeError: 
        labels = svm.apply().get_labels()
        return labels 

def model_diff(predictions1, predictions2):
    
    num_diff_predictions = len(predictions1) - sum(sign(predictions1 + epsilon) == sign(predictions2 + epsilon))
    return num_diff_predictions



def compare_hyperplanes(svm1, svm2):
    
    w1,b1 = get_hyperplane_from_svm(svm1)
    w2,b2 = get_hyperplane_from_svm(svm2)
    
    return norm(vstack((w1,b1)) - vstack((w2,b2)))


def plot_hyperplane(w,b, color):
     
     w = w/norm(w)
     x = 1-2*random(10) 
     y = -(w[0]/w[1]*x - b)
     pylab.plot(x,y,color) 

def plot_error(error, legend):
    
    x = arange(len(error))
    pylab.plot(x, error,label=legend)
    pylab.ylim(0,100)
    pylab.legend()

def get_uci_data_shape(dataname):

    dataset = Dataset(dataname, datadir)
    return dataset.examples.shape

def get_uci_data(dataname, frac_train, perm_idx = 0, normalize_data = True):
    
    dataset = Dataset(dataname, datadir)
    split_type = 'test'  
    (idx_train, idx_pred, id_str) = dataset.get_perm(perm_idx, split_type, frac_train)

    #extract data
    train_data=dataset.examples[:,idx_train]
    test_data=dataset.examples[:,idx_pred]    
    #dataset.labels = dataset.labels.astype(float)
    train_labels=dataset.labels[idx_train]
    test_labels = dataset.labels[idx_pred]
    
    #normalize the data
    if(normalize_data): normalize(train_data, test_examples=test_data, subtract_mean=True, divide_std=True)
    
    print dataname, train_data.shape, test_data.shape
    return train_data, train_labels, test_data, test_labels

def write_2_file(filename, data):
    
    outputfile = open('%s'%filename, 'wb')
    pickle.dump(data, outputfile)
    
def load_from_file(filename):
    
    pkl_file = open(filename, 'rb')
    d = pickle.load(pkl_file)
    return d

if __name__ == '__main__':
    
    from numpy.random import rand
    num_pts = 150
    points = rand(3,num_pts)
    labels = []
    labels.extend(list(ones(50)))
    labels.extend(list(2*ones(50)))
    labels.extend(list(3*ones(50)))
    plot_plal_data_3d(points, labels = labels, labels_names = None)