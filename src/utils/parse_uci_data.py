from numpy.random import permutation,random
from numpy import ones,zeros,arange,sign,sum
from numpy.lib.shape_base import vstack
import os
import csv
from pylab import *

datadir = 'data/'
num_perms=50

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
        
def convert_uci_data_file(datafilename, outputfilename, dataname, convert_func, permfilename = None):
    
    datafile = open(datafilename,'r')
    lines = datafile.readlines()
    data = convert_func(lines)
    
    (num_points, dim) = data.shape
    outputfile = csv.writer(open(outputfilename, 'w'), delimiter=',', quoting=csv.QUOTE_NONE)
    
    # Write the data 
    for i in range(num_points):
        curr_row = data[i,:]
        outputfile.writerow(curr_row)
    
    if(permfilename == None): return
    
    permfile = open(permfilename,'w')
    for i in range(num_perms):
        perm = permutation(num_points)
        for j in range(num_points-1):
            permfile.write("%d "%perm[j])
        permfile.write("%d\n"%perm[num_points-1])
    permfile.close()
    
 
def _convert_chess(chess_input):
    
    data = []
    string_2_label = {}
    string_2_attributes = {}
    
    for line in chess_input:
        curr_data = []        
        strings = line.strip().split(',')
        
        #get the label
        attr_str = strings[-1]
        if(attr_str not in string_2_label): string_2_label[attr_str] = len(string_2_label)-1
        label = string_2_label[attr_str]
        curr_data.append(label)
                
        for idx,attr_str in enumerate(strings):
            
            if(idx == len(strings)-1): continue
                
            #parse the attributes
            if(is_number(attr_str)): attr = float(attr_str)
            else:
                if(attr_str not in string_2_attributes): string_2_attributes[attr_str] = float(len(string_2_attributes))
                attr = string_2_attributes[attr_str]
            curr_data.append(attr)         
        
        data.append(array(curr_data))
    
    return array(data)

def _convert_abalone(abalone_input):
    
    data = []
    string_2_label = {}
    string_2_attributes = {}
    
    for line in abalone_input:
        curr_data = []        
        strings = line.strip().split(',')
        
        #get the label
        attr_str = strings[-1]
        if(attr_str not in string_2_label): string_2_label[attr_str] = len(string_2_label)
        label = string_2_label[attr_str]
        curr_data.append(label)
                
        for idx,attr_str in enumerate(strings):
            
            if(idx == len(strings)-1): continue
                
            #parse the attributes
            if(is_number(attr_str)): attr = float(attr_str)
            else:
                if(attr_str not in string_2_attributes): string_2_attributes[attr_str] = float(len(string_2_attributes))
                attr = string_2_attributes[attr_str]
            curr_data.append(attr)         
        
        data.append(array(curr_data))
    
    return array(data)
    
def _convert_yeast(yeast_input):
    
    data = []
    string_2_label = {}
    string_2_attributes = {}
    
    for line in yeast_input:
        curr_data = []        
        strings = line.strip().split(' ')
        strings_temp = []
        for s in strings: 
            if len(s) > 0 : strings_temp.append(s)
        strings = strings_temp
        assert(len(strings) == 10)
        
        #get the label
        attr_str = strings[-1]
        if(attr_str not in string_2_label): string_2_label[attr_str] = len(string_2_label)
        label = string_2_label[attr_str]
        curr_data.append(label)
                
        for idx,attr_str in enumerate(strings):
            #ignore the label and the name in the beginning
            if(idx == len(strings)-1 or idx == 0): continue
                
            #parse the attributes
            if(is_number(attr_str)): attr = float(attr_str)
            else:
                print "what the hell is this attribute!", attr_str
            curr_data.append(attr)         
        
        data.append(array(curr_data))
    
    return array(data)

def _convert_skin(skin_input):
    
    string_2_label = {'2':1,'1':-1}    
    data = []
    
    for line_idx, line in enumerate(skin_input):
        line = line.split(',')
        if(len(line) < 4): continue
        curr_data = []
        #get the label, originally its 1 and 2, transform it into -1,1
        label = string_2_label[line[-1].strip()]
        curr_data.append(label)
        for attr_idx in range(len(line)-1):
            curr_data.append(float(line[attr_idx]))
        data.append(array(curr_data))
   
    return array(data)

if __name__ == '__main__':    
    
    #dataname = 'chess'
    #dataname = 'yeast'  
    dataname = 'skin' 
    datafilename = datadir+'original/%s/%s.data'%(dataname, dataname)
    outputfilename = datadir+'/%s.csv'%dataname
    permfilename = datadir+'/%s_perm.txt'%dataname
    
    convert_uci_data_file(datafilename, outputfilename, dataname, _convert_skin, permfilename = permfilename)
    
    """
    datafile = open(datafilename,'r')
    lines = datafile.readlines()
    #out = open(datafilename+'.conv','w')
    out = csv.writer(open(datafilename+'.conv.csv', 'w'), delimiter=',', quoting=csv.QUOTE_NONE)
    for l in lines:
        stack = l.strip().split(' ')
        row = []
        for s in stack: 
            #if len(s) > 0: row += "%s,"%str(s)
            if len(s) > 0: row.append("%s"%str(s))
        #row = row[:len(row)-1] + "\n"
        
        out.writerow(row)
    #out.close()
    exit()
    """
    """
    from utils import get_uci_data_shape
    datasets = ['pima','vote','heart','bupa']
    for dataname in datasets:
        (dim, num_points) = get_uci_data_shape(dataname)
        print dim, num_points, dataname
        permfilename = '%s_perm.txt'%dataname
        permfile = open(permfilename,'w')
        for i in range(num_perms):
            perm = permutation(num_points)
            for j in range(num_points-1):
                permfile.write("%d "%perm[j])
            permfile.write("%d\n"%perm[num_points-1])
        permfile.close()
    
    exit()
    """