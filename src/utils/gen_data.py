# -*- coding: utf-8 -*-
"""
Created on Sun Aug 26 13:43:12 2012

@author: sharon 
"""
from numpy.random import multivariate_normal, permutation, seed, rand
from numpy import ones, diag, vstack, hstack, array, ceil, mean, std
from pylab import show
from numpy.random import seed, rand


def normalize(train_examples, test_examples = None, subtract_mean=False, divide_std=False):
    """
    Scale examples to ... (be on a ball? just const? 0 mean, std 1?)
    """
    if subtract_mean:
        # mean = 0.0
        m = mean(train_examples, axis=1)
        for i in xrange(train_examples.shape[1]):
            train_examples[:,i]-=m
        if(test_examples != None):
            for i in xrange(test_examples.shape[1]):
                test_examples[:,i]-=m
        
    if divide_std:
        # std = 1.0
        s = std(train_examples, axis=1)
        for i in xrange(train_examples.shape[1]):
            train_examples[:,i]/=(s+1e-10)
        if(test_examples != None):
            for i in xrange(test_examples.shape[1]):
                test_examples[:,i]/=(s+1e-10)

def gen_2_class_guassians(dim, n1, n2, noise_level = 0, flip_perc = 0):
    
    #TODO: remove the seed!
    seed(1)
    mean1 = ones(dim)
    mean2 = -1*ones(dim)    
    cov = diag(0.5*ones(dim))
    if(noise_level > 0):
        for i in range(dim):
            for j in range(dim):
                cov[i,j] += noise_level
                
    s1 = multivariate_normal(mean1, cov, n1)
    s2 = multivariate_normal(mean2, cov, n2)        
    labels = ones(n1+n2)    
    labels[n1:] *= -1
    data = vstack((s1,s2)).T
    
    if(flip_perc > 0):
        p = permutation(n1+n2)
        num_flipped = ceil(flip_perc*(n1+n2))
        labels[p[:num_flipped]] *= -1
        
    return data, labels


def gen_plal_data(num_examples, dim, dense_gaussians, sparse_gaussians, multilabel = False, cov = False, dense_var = .0001, sparse_var = .005, 
                  use_seed = None, normalize_data = False):
    
    """
    dense_gaussians - list of ratios in [0,1] corresponding to the number of points that should 
    be generated from dense gaussians.
    sparse_gaussians - the same as the dense only from sparse gausssians 
    labels - if given, should have the length = len(dense_gaussians) + len(sparse_gaussians) and indicate for 
    each gaussian, which label it should recieve. The default is to have two classes, half of the dense and half of
    the sparse are assigned to one class. 
    """
    if use_seed != None: seed(use_seed)
  
    if(len(dense_gaussians) > pow(2,dim)):
        print "can't generate such a dataset!"
        return None
    #assert((sum(dense_gaussians) + sum(sparse_gaussians)) == 1)
    
    if(cov):
        dense_cov = dense_var*ones((dim, dim))
        sparse_cov = sparse_var*ones((dim, dim))
    else:
        #generate covariance matrices    
        dense_cov = diag(dense_var*ones(dim))
        sparse_cov = diag(sparse_var*ones(dim))    
        
        
    data = None
    labels = None
    label_cnt = 0
    
    def sampe_corner(dim):
        corner = array([int(round(rand(1))) for i in range(dim)])
        return corner
    
    corner_distance = 0.1
    corners_used = []    
    #obtain the dense gaussians
    for pidx, porportion in enumerate(dense_gaussians):
        
        num_dense_examples = int(ceil(num_examples * porportion))                
        
        corner = None
        while (corner == None):
            corner = sampe_corner(dim)
            for c in corners_used: 
                if (sum(corner == c) == dim):
                    corner = None
                    break
        corners_used.append(corner)
        mean = abs(corner - corner_distance) #this way its between 0 and 1
        curr_data =  multivariate_normal(mean, dense_cov, num_dense_examples)
        curr_labels = ones(num_dense_examples)
        #use binary labels
        if not multilabel:
            if(pidx%2 == 1): curr_labels *= -1
        else: 
            curr_labels *= label_cnt
            label_cnt += 1
        
        if(data == None):
            data = curr_data
            labels = list(curr_labels)
        else:
            data = vstack((data, curr_data))
            labels.extend(curr_labels)
    
    #now the sparse gaussians
    for pidx, porportion in enumerate(sparse_gaussians):
        
        num_sparse_examples = int(ceil(num_examples * porportion))                
        mean = rand(dim)
        curr_data =  multivariate_normal(mean, sparse_cov, num_sparse_examples)
        curr_labels = ones(num_sparse_examples)
        #use binary labels
        if not multilabel:
            if(pidx%2 == 1): curr_labels *= -1
        else: 
            curr_labels *= label_cnt
            label_cnt += 1
        
        if(data == None):
            data = curr_data
            labels = list(curr_labels)
        else:
            data = vstack((data, curr_data))
            labels.extend(curr_labels)
    
    data = data.T
    if(normalize_data):
        normalize(data, subtract_mean=True, divide_std=True)
    
    return data, array(labels)


if __name__ == '__main__':
    
    from utils import *

    num_examples = 1000
    dim = 2
    dense_gaussians = [.2,.2,.2,.2]
    sparse_gaussians = [.05,.05,.05,.05]
    data, labels = gen_plal_data(num_examples, dim, dense_gaussians, sparse_gaussians, multilabel = False, dense_var = .1, sparse_var = 1)
    #normalize(data, test_examples = None, subtract_mean=True, divide_std=True)    
    if(dim == 3):    
        plot_plal_data_3d(data, labels)
    elif(dim ==2):
        plot_2d(data, labels)
    else:
        print "can't plot dim ",dim
   
#    seed(1)
#    a= rand()
#    print a
#    exit()
    #data, labels = gen_2_class_guassians(2,100,100, flip_perc = 0.1)
#    data, labels = gen_2_class_guassians(2,100,100, noise_level = 0.6)    
    #plot_2d(data, labels)
    #show() 
    