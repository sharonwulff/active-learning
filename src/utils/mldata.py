#!/usr/bin/env python

"""Classes to encapsulate the idea of a dataset in machine learning,
   including file access. Currently this focuses on reading and writing
   transparently to different file formats.

   A dataset is modeled as an (example,label) tuple, each of which is an array.
   The base class doesn't know how to split, so just returns one array.

   The three classes currently implemented use three
   different ways of iterating through files:
   - CSV uses the python module csv's iterator
   - ARFF always reads the whole file, and does a slice
   - FASTA uses a hand crafted while loop that behaves like a generator

   The class DatasetFileARFF is in mldata-arff.py.
"""


#############################################################################################
#                                                                                           #
#    This program is free software; you can redistribute it and/or modify                   #
#    it under the terms of the GNU General Public License as published by                   #
#    the Free Software Foundation; either version 3 of the License, or                      #
#    (at your option) any later version.                                                    #
#                                                                                           #
#    This program is distributed in the hope that it will be useful,                        #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of                         #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                           #
#    GNU General Public License for more details.                                           #
#                                                                                           #
#    You should have received a copy of the GNU General Public License                      #
#    along with this program; if not, see http://www.gnu.org/licenses                       #
#    or write to the Free Software Foundation, Inc., 51 Franklin Street,                    #
#    Fifth Floor, Boston, MA 02110-1301  USA                                                #
#                                                                                           #
#############################################################################################

import sys
from numpy.linalg import norm
from numpy import array, concatenate, mean, std 
import numpy
import csv

try:
    import arff
    have_arff = True
except ImportError:
    have_arff = False



class DatasetFileBase(file):
    """A Base class defining barebones and common behaviour
    """
    
    def __init__(self,filename,extype):
        """Just the normal file __init__,
        followed by the specific class corresponding to the file extension.
        
        """
        self.extype = extype
        self.filename = filename

    
    def readlines(self,idx=None):
        """Read the lines defined by idx (a numpy array).
        Default is read all lines.

        """
        if idx is None:
            data = self.readlines()
        else:
            data = self.readlines()[idx]
            #itertools.islice(open('tempx.txt'), 11, 12).next()
            #file("filename").readlines()[11]
            #linecache.getline(  filename, lineno[, module_globals])
        return data

    def writelines(self,data,idx=None):
        """Write the lines defined by idx (a numpy array).
        Default is write all lines.

        data is assumed to be a numpy array.

        """
        if idx is None:
            self.writelines(data)
        else:
            self.writelines(data[idx])



class DatasetFileCSV(DatasetFileBase):
    """Comma Seperated Values file.

    Labels are in the first column.

    """
    def __init__(self,filename,extype):
        DatasetFileBase.__init__(self,filename,extype)

    def readlines(self,idx=None):
        """Read from file and split data into examples and labels"""
        reader = csv.reader(open(self.filename,'r'), delimiter=',', quoting=csv.QUOTE_NONE)
        labels = []
        examples = []
        for ix,line in enumerate(reader):
            if idx is None or ix in idx:
                labels.append(float(line[0]))
                if self.extype == 'vec':
                    examples.append(array(map(float,line[1:])))
                elif self.extype == 'seq':
                    examples.append(line[1:][0])
                elif self.extype == 'mseq':
                    examples.append(array(line[1:]))

        if self.extype == 'vec':
            examples = array(examples).T
            print '%d features, %d examples' % examples.shape
        elif self.extype == 'seq':
            print 'sequence length = %d, %d examples' % (len(examples[0]),len(examples))
        elif self.extype == 'mseq':
            printstr = 'sequence lengths = '
            for seq in examples[0]:
                printstr += '%d, ' % len(seq)
            printstr += '%d examples' % len(examples)
            print printstr
        
        return (examples,array(labels))


    def writelines(self,examples,labels,idx=None):
        """Merge the examples and labels and write to file"""
        if idx==None:
            idx = range(len(labels))
        if self.extype == 'seq':
            data = zip(labels[idx],list(array(examples)[idx]))
        if self.extype == 'mseq':
            data = []
            for ix,curlab in enumerate(labels):
                data.append([curlab]+list(examples[ix]))
        elif self.extype == 'vec':
            data = []
            for ix,curlab in enumerate(labels):
                data.append(concatenate((array([curlab]),examples[:,ix].T)))
                
        fp = open(self.filename,'w')
        writer = csv.writer(fp,delimiter=',',quoting=csv.QUOTE_NONE)
        for ix in idx:
            writer.writerow(data[ix])
        fp.close()




class DatasetFileFASTA(DatasetFileBase):
    """Fasta format file, labels are in the comment after keyword 'label'.
    label=1
    label=-1

    """
    def __init__(self,filename,extype):
        if extype != 'seq':
            print 'Can only write fasta file for sequences!'
            raise IOError
        DatasetFileBase.__init__(self,filename,extype)
        self.fp = None
                
    def readlines(self,idx=None):
        """Read from file and split data into examples and labels"""
        self.fp = open(self.filename,'r')
        line = self.fp.readline()

        examples = []
        labels = []
        ix = 0
        while True:
            if not line : break
            (ex,lab,line) = self.readline(line)
            if idx is None or ix in idx:
                examples.append(ex)
                labels.append(lab)
            ix += 1

        print 'sequence length = %d, %d examples' % (len(examples[0]),len(examples))
        return (examples,array(labels))

    def writelines(self,examples,labels,idx=None,linelen=60):
        """Write the examples and labels and write to file"""
        if idx==None:
            idx = range(len(labels))

        fp = open(self.filename,'w')
        for ix in idx:
            fp.write('> %d label=%d\n'%(ix,round(labels[ix])))
            for lineidx in xrange(0, len(examples[ix]), linelen):
                fp.write(examples[ix][lineidx:lineidx+linelen] + '\n')
        fp.close()
            

    def readline(self,line):
        """Reads a fasta entry and returns the label and the sequence"""
        if line[0] == '' : return

        assert(line[0] == '>')
        # Use list comprehension to get the integer that comes after label=
        a = line.split()
        label = float([b.split('=')[1] for b in a if b.split('=')[0]=='label'][0])

        lines = []
        line = self.fp.readline()
        while True:
            if not line : break
            if line[0] == ">": break
            #Remove trailing whitespace, and any internal spaces
            lines.append(line.rstrip().replace(" ",""))
            line = self.fp.readline()

        return (''.join(lines),label,line)
            
                                                        
def init_datasetfile(filename,extype):
    """A factory that returns the appropriate class based on the file extension.

    recognised file extensions
    - .csv  : Comma Separated Values
    - .arff : Attribute-Relation File Format (weka)
    - .fa   : Fasta file format (seq only)
    - .fasta: same as above.

    Since the file type does not determine what type of data is actually being used,
    the user has to supply the example type.
    
    extype can be ('vec','seq','mseq')
    vec - array of floats
    seq - single sequence
    mseq - multiple sequences

    """
    allowedtypes = ('vec','seq','mseq')
    assert(extype in allowedtypes)
    # map the file extensions to the relevant classes
    _format2dataset = {'csv'   : DatasetFileCSV,
                       'fa'    : DatasetFileFASTA,
                       'fasta' : DatasetFileFASTA,
                       }
    if have_arff:
        from esvm.mldata_arff import DatasetFileARFF
        _format2dataset['arff'] = DatasetFileARFF

    extension = detect_extension(filename)
    return _format2dataset[extension](filename,extype)


def detect_extension(filename):
    """Get the file extension"""
    if filename.count('.') > 1:
        print 'WARNING: %s has more than one . using last one' % filename
    detect_ext = filename.split('.')[-1]
    if have_arff:
        known_ext = ('csv','arff','fasta','fa')
    else:
        known_ext = ('csv','fasta','fa')

    if detect_ext not in known_ext:
        print 'WARNING: %s is an unknown file extension, defaulting to csv' % detect_ext
        detect_ext = 'csv'

    return detect_ext
                                        


def convert(infile,outfile,extype):
    """Copy data from infile to outfile, possibly converting the file format."""
    fp1 = init_datasetfile(infile,extype)
    (examples,labels) = fp1.readlines()
    fp2 = init_datasetfile(outfile,extype)
    fp2.writelines(examples,labels)





class Dataset(object):
    """Encapsulate the data as well as permutations.
    This is to ensure consistent splitting of training and test sets.
    """
    dataname = ''
    datadir = ''
    
    filename = ''
    examples = None
    labels = None
    num_examples = 0
    num_features = 0

    perm_filename = ''
    perms = numpy.array([]) # all the permutations
    perm_idx = 0

    def __init__(self, name, datadir, data_file=None, perm_file=None):
        """Load the data into memory"""
        self.dataname = name
        self.datadir = datadir
        
        if data_file:
            self.filename = data_file
        else:
            self.filename = '%s/%s.csv' % (datadir, name)
        data = init_datasetfile(self.filename,'vec')
        (self.examples, self.labels) = data.readlines()
        (self.num_features, self.num_examples) = self.examples.shape
        
        if perm_file:
            self.perm_filename = perm_file
        else:
            self.perm_filename = '%s/%s_perm.txt' % (datadir, name)
        print "yoyo ",self.perm_filename
        self.perms = numpy.loadtxt(self.perm_filename, dtype=int, delimiter=' ')
        
    def get_perm(self, split_idx, split_type, frac_train, fold=None, num_cv=None):
        """Returns the indices of the training and test examples."""
        assert(split_type=='val' or split_type=='test')
        self.perm_idx = split_idx
        perm = self.perms[split_idx]
        split_train = int(frac_train*self.num_examples)
        if split_type == 'val':
            perm_train = perm[:split_train]
            idx_pred = perm_train[fold::num_cv]
            idx_train = []
            for idx in perm_train:
                if not (idx in idx_pred):
                    idx_train.append(idx)
            idx_train = numpy.array(idx_train)

        elif split_type == 'test':
            idx_train = perm[:split_train]
            idx_pred = perm[split_train:]

        id_str = self._param2string(split_idx, split_type, fold, num_cv, frac_train)
        return (idx_train, idx_pred, id_str)


    def _param2string(self,split_idx, split_type, fold, num_cv, frac_train):
        """Generate part of file name the split"""
        if split_type == 'test':
            id_str = '%d_r%f' % (self.perm_idx, float(frac_train))
        elif split_type == 'val':
            id_str = '%d_cv%d:%d' % (self.perm_idx, fold, num_cv)
        return id_str

def normalize(train_examples, test_examples = None, subtract_mean=False, divide_std=False, rescale=False, norm_one=False):
    """
    Scale examples to ... (be on a ball? just const? 0 mean, std 1?)
    """

    if subtract_mean:
        # mean = 0.0
        m = mean(train_examples, axis=1)
        for i in xrange(train_examples.shape[1]):
            train_examples[:,i]-=m
        if(test_examples != None):
            for i in xrange(test_examples.shape[1]):
                test_examples[:,i]-=m
        
    if divide_std:
        # std = 1.0
        s = std(train_examples, axis=1)
        for i in xrange(train_examples.shape[1]):
            train_examples[:,i]/=(s+1e-10)
        if(test_examples != None):
            for i in xrange(test_examples.shape[1]):
                test_examples[:,i]/=(s+1e-10)
    if rescale:
        # scale to have on average 1 on linear kernel diagonal
        scale=sqrt(mean(diag(mat(train_examples).T*mat(train_examples))))
        train_examples/=scale
        if(test_examples != None):
            test_examples/=scale
    
    if norm_one:
        # ball/circle
        for i in xrange(train_examples.shape[1]):
            train_examples[:,i]/=norm(train_examples[:,i])
        if(test_examples != None):
            for i in xrange(test_examples.shape[1]):
                test_examples[:,i]/=norm(test_examples[:,i])
                
                
def normalize_kernel(K):
    """Center and normalize the trace of the kernel matrix"""
    one_mat = numpy.matrix(numpy.ones(K.shape))
    one_vec = numpy.matrix(numpy.ones((K.shape[0],1)))

    # centering
    row_sum = numpy.matrix(numpy.mean(K,axis=0)).T
    R = K - row_sum * one_vec.T - one_vec * row_sum.T +\
        numpy.mean(row_sum.A)*one_mat

    # variance normalization
    inv_sqrt_diag = numpy.divide(one_vec,numpy.matrix(numpy.sqrt(numpy.diag(K))).T)
    KN = numpy.multiply(numpy.kron(one_vec.T,inv_sqrt_diag),R)
    KN = numpy.multiply(numpy.kron(one_vec,inv_sqrt_diag.T),R)
    return KN


def calc_normalizing_weight(K):
    """Compute weights for unit trace normalization"""
    return numpy.trace(K)
